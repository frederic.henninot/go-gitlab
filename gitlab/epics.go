package gitlab

import (
	"context"
	"fmt"
	"time"
)

type EpicsService service

type Epic struct {
	Id                           *uint64     `json:"id,omitempty"`
	IId                          *uint64     `json:"iid,omitempty"`
	GroupId                      *uint64     `json:"group_id,omitempty"`
	Title                        *string     `json:"title,omitempty"`
	Description                  *string     `json:"description,omitempty"`
	State                        *string     `json:"state,omitempty"`
	WebUrl                       *string     `json:"web_url,omitempty"`
	Reference                    *string     `json:"reference,omitempty"`
	References                   *References `json:"references,omitempty"`
	Author                       *Author     `json:"author,omitempty"`
	StartDate                    *time.Time  `json:"start_date,omitempty"`
	StartDateIsFixed             *bool       `json:"start_date_is_fixed,omitempty"`
	StartDateFixed               *time.Time  `json:"start_date_fixed,omitempty"`
	StartDateFromInheritedSource *time.Time  `json:"start_date_from_inherited_source,omitempty"`
	DueDate                      *time.Time  `json:"due_date,omitempty"`
	DueDateIsFixed               *bool       `json:"due_date_is_fixed,omitempty"`
	DueDateFixed                 *time.Time  `json:"due_date_fixed,omitempty"`
	DueDateFromInheritedSource   *time.Time  `json:"due_date_from_inherited_source,omitempty"`
	CreatedAt                    *time.Time  `json:"created_at,omitempty"`
	UpdatedAt                    *time.Time  `json:"updated_at,omitempty"`
	ClosedAt                     *time.Time  `json:"closed_at,omitempty"`
	Labels                       *[]string   `json:"labels,omitempty"`
	Upvotes                      *uint16     `json:"upvotes,omitempty"`
	Downvotes                    *uint16     `json:"downvotes,omitempty"`
}

type EpicData struct {
	Title            *string    `json:"title,omitempty"`
	Description      *string    `json:"description,omitempty"`
	StartDateIsFixed *bool      `json:"start_date_is_fixed,omitempty"`
	StartDateFixed   *time.Time `json:"start_date_fixed,omitempty"`
	DueDateIsFixed   *bool      `json:"due_date_is_fixed,omitempty"`
	DueDateFixed     *time.Time `json:"due_date_fixed,omitempty"`
	Labels           *string    `json:"labels,omitempty"`
	ParentId         *uint64    `json:"parent_id,omitempty"`
	StateEvent       *string    `json:"state_event,omitempty"`
}

type EpicOption struct {
	AuthorId                *uint64    `json:"author_id,omitempty"`
	Labels                  *string    `json:"labels,omitempty"`
	WithLabelsDetails       *bool      `json:"with_labels_details,omitempty"`
	OrderBy                 *string    `json:"order_by,omitempty"`
	Sort                    *string    `json:"sort,omitempty"`
	Search                  *string    `json:"search,omitempty"`
	State                   *string    `json:"state,omitempty"`
	CreatedAfter            *time.Time `json:"created_after,omitempty"`
	CreatedBefore           *time.Time `json:"created_before,omitempty"`
	UpdatedAfter            *time.Time `json:"updated_after,omitempty"`
	UpdatedBefore           *time.Time `json:"updated_before,omitempty"`
	IncludeAncestorGroups   *bool      `json:"include_ancestor_groups,omitempty"`
	IncludeDescendantGroups *bool      `json:"include_descendant_groups,omitempty"`
	PaginationOptions
}

type EpicTodo struct {
	BaseTodo
	Groupe *Groupe `json:"group,omitempty"`
	Target *Epic   `json:"target,omitempty"`
}

// List epics for a group
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epics.html#list-epics-for-a-group
func (s *EpicsService) List(ctx context.Context, id string, opts *EpicOption) ([]*Epic, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("groups/%v/epics", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var epics []*Epic
	resp, err := s.client.Request(ctx, req, &epics)
	if err != nil {
		return nil, resp, err
	}

	return epics, resp, nil
}

// Single epic
//
// id: group ID or path
// epic_iid: The internal ID of the epic.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epics.html#single-epic
func (s *EpicsService) Single(ctx context.Context, id string, epic_iid uint64) (*Epic, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("groups/%v/epics/%v", id, epic_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	epic := new(Epic)
	resp, err := s.client.Request(ctx, req, &epic)
	if err != nil {
		return nil, resp, err
	}

	return epic, resp, nil
}

// New epic
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epics.html#new-epic
func (s *EpicsService) Create(ctx context.Context, id string, data *EpicData) (*Epic, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("groups/%v/epics", id), data)
	if err != nil {
		return nil, nil, err
	}

	epic := new(Epic)
	resp, err := s.client.Request(ctx, req, &epic)
	if err != nil {
		return nil, resp, err
	}

	return epic, resp, nil
}

// Update epic
//
// id: group ID or path
// epic_iid: The internal ID of the epic.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epics.html#update-epic
func (s *EpicsService) Update(ctx context.Context, id string, epic_iid uint64, data *EpicData) (*Epic, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("groups/%v/epics/%v", id, epic_iid), data)
	if err != nil {
		return nil, nil, err
	}

	epic := new(Epic)
	resp, err := s.client.Request(ctx, req, &epic)
	if err != nil {
		return nil, resp, err
	}

	return epic, resp, nil
}

// Delete epic
//
// id: group ID or path
// epic_iid: The internal ID of the epic.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epics.html#delete-epic
func (s *EpicsService) Delete(ctx context.Context, id string, epic_iid uint64) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("groups/%v/epics/%v", id, epic_iid), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// Create a todo
//
// id: group ID or path
// epic_iid: The internal ID of the epic.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epics.html#create-a-todo
func (s *EpicsService) Todo(ctx context.Context, id string, epic_iid uint64) (*EpicTodo, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("groups/%v/epics/%v/todo", id, epic_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	todo := new(EpicTodo)
	resp, err := s.client.Request(ctx, req, &todo)
	if err != nil {
		return nil, resp, err
	}

	return todo, resp, nil
}
