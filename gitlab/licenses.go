package gitlab

import (
	"context"
	"fmt"
)

type LicensesService service

type LicenseTemplate struct {
	Key         *string   `json:"key,omitempty"`
	Name        *string   `json:"name,omitempty"`
	Nickname    *string   `json:"nickname,omitempty"`
	Featured    *bool     `json:"featured,omitempty"`
	HtmlUrl     *string   `json:"html_url,omitempty"`
	SourceUrl   *string   `json:"source_url,omitempty"`
	Description *string   `json:"description,omitempty"`
	Conditions  *[]string `json:"conditions,omitempty"`
	Permissions *[]string `json:"permissions,omitempty"`
	Limitations *[]string `json:"limitations,omitempty"`
	Content     *string   `json:"content,omitempty"`
}

type LicenseTemplateOption struct {
	Popular  *bool   `json:"popular,omitempty"`
	Project  *string `json:"project,omitempty"`
	Fullname *string `json:"fullname,omitempty"`
}

// List license templates
//
// Giltab API docs: https://docs.gitlab.com/ee/api/templates/licenses.html#list-license-templates
func (s LicensesService) List(ctx context.Context, opts *LicenseTemplateOption) ([]*LicenseTemplate, *Response, error) {
	u, err := addUrlOptions("templates/licenses", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var availableTemplates []*LicenseTemplate
	resp, err := s.client.Request(ctx, req, &availableTemplates)
	if err != nil {
		return nil, resp, err
	}

	return availableTemplates, resp, nil
}

// Single license template
//
// Giltab API docs: https://docs.gitlab.com/ee/api/templates/licenses.html#single-license-template
func (s LicensesService) Get(ctx context.Context, key string, opts *LicenseTemplateOption) (*LicenseTemplate, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("templates/licenses/%v", key), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	licenseTemplate := new(LicenseTemplate)
	resp, err := s.client.Request(ctx, req, licenseTemplate)
	if err != nil {
		return nil, resp, err
	}

	return licenseTemplate, resp, nil
}
