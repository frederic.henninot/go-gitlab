package gitlab

import (
	"context"
	"fmt"
	"time"
)

type ContainerRegistryService service

type ContainerRegistryTag struct {
	Name          *string    `json:"name,omitempty"`
	Path          *string    `json:"path,omitempty"`
	Location      *string    `json:"location,omitempty"`
	Revision      *string    `json:"revision,omitempty"`
	ShortRevision *string    `json:"short_revision,omitempty"`
	Digest        *string    `json:"digest,omitempty"`
	CreatedAt     *time.Time `json:"created_at,omitempty"`
	TotalSize     *uint64    `json:"total_size,omitempty"`
}

type ContainerRegistry struct {
	Id        *uint64                 `json:"id,omitempty"`
	Name      *string                 `json:"name,omitempty"`
	Path      *string                 `json:"path,omitempty"`
	ProjectId *uint64                 `json:"project_id,omitempty"`
	Location  *string                 `json:"location,omitempty"`
	CreatedAt *time.Time              `json:"created_at,omitempty"`
	Tags      *[]ContainerRegistryTag `json:"tags,omitempty"`
}

type ContainerRegistryOptions struct {
	Tags *bool `json:"tags,omitempty"`
	PaginationOptions
}

// List registry repositories within a project
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/container_registry.html#within-a-project
func (s *ContainerRegistryService) ProjectList(ctx context.Context, id string, opts *ContainerRegistryOptions) ([]*ContainerRegistry, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/registry/repositories", id), opts)
}

// List registry repositories within a group
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/container_registry.html#within-a-group
func (s *ContainerRegistryService) GroupList(ctx context.Context, id string, opts *ContainerRegistryOptions) ([]*ContainerRegistry, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/registry/repositories", id), opts)
}

// Delete registry repository
//
// id: project ID or path
// repository_id: ID of the repository
//
// GitLab API docs: https://docs.gitlab.com/ee/api/container_registry.html#delete-registry-repository
func (s *ContainerRegistryService) Delete(ctx context.Context, id string, repository_id int) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/registry/repositories/%v", id, repository_id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// List registry repository tags withi a project
//
// id: project ID or path
// repository_id: ID of the repository
//
// GitLab API docs: https://docs.gitlab.com/ee/api/container_registry.html#within-a-project-1
func (s *ContainerRegistryService) Tags(ctx context.Context, id string, repository_id int) ([]*ContainerRegistryTag, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/registry/repositories/%v", id, repository_id), nil)
	if err != nil {
		return nil, nil, err
	}

	var containerRegistryTags []*ContainerRegistryTag
	resp, err := s.client.Request(ctx, req, &containerRegistryTags)
	if err != nil {
		return nil, resp, err
	}

	return containerRegistryTags, resp, nil
}

// Get details of a registry repository tag
//
// id: project ID or path
// repository_id: ID of the repository
// tag_name: The name of tag
//
// GitLab API docs: https://docs.gitlab.com/ee/api/container_registry.html#get-details-of-a-registry-repository-tag
func (s *ContainerRegistryService) Tag(ctx context.Context, id string, repository_id int, tag_name string) (*ContainerRegistryTag, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/registry/repositories/%v/tags/%v", id, repository_id, tag_name), nil)
	if err != nil {
		return nil, nil, err
	}

	containerRegistryTag := new(ContainerRegistryTag)
	resp, err := s.client.Request(ctx, req, &containerRegistryTag)
	if err != nil {
		return nil, resp, err
	}

	return containerRegistryTag, resp, nil
}

// Delete a registry repository tag
//
// id: project ID or path
// repository_id: ID of the repository
// tag_name: The name of tag
//
// GitLab API docs: https://docs.gitlab.com/ee/api/container_registry.html#delete-a-registry-repository-tag
func (s *ContainerRegistryService) DeleteTag(ctx context.Context, id string, repository_id int, tag_name string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/registry/repositories/%v/tags/%v", id, repository_id, tag_name), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

type ContainerRegistryTagsOptions struct {
	NameRegex       *string `json:"name_regex,omitempty"`
	NameRegexDelete *string `json:"name_regex_delete,omitempty"`
	NameRegexKeep   *string `json:"name_regex_keep,omitempty"`
	KeepN           *uint8  `json:"keep_n,omitempty"`
	OlderThan       *string `json:"older_than,omitempty"`
}

// Delete registry repository tags in bulk
//
// id: project ID or path
// repository_id: ID of the repository
//
// GitLab API docs: https://docs.gitlab.com/ee/api/container_registry.html#delete-registry-repository-tags-in-bulk
func (s *ContainerRegistryService) DeleteTags(ctx context.Context, id string, repository_id int, opts ContainerRegistryTagsOptions) (*Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/registry/repositories/%v/tags", id, repository_id), opts)
	if err != nil {
		return nil, err
	}

	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (s *ContainerRegistryService) list(ctx context.Context, url string, opts *ContainerRegistryOptions) ([]*ContainerRegistry, *Response, error) {
	if opts != nil {
		u, err := addUrlOptions(url, opts)
		if err != nil {
			return nil, nil, err
		}
		url = u
	}

	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	var containerRegistries []*ContainerRegistry
	resp, err := s.client.Request(ctx, req, &containerRegistries)
	if err != nil {
		return nil, resp, err
	}

	return containerRegistries, resp, nil
}
