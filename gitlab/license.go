package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"time"
)

type LicenseService service

type License struct {
	Id               *uint64          `json:"id,omitempty"`
	Plan             *string          `json:"plan,omitempty"`
	CreatedAt        *time.Time       `json:"created_at,omitempty"`
	StartsAt         *JSONDate        `json:"starts_at,omitempty"`
	ExpiresAt        *JSONDate        `json:"expires_at,omitempty"`
	HistoricalMax    *uint            `json:"historical_max,omitempty"`
	MaximumUserCount *uint            `json:"maximum_user_count,omitempty"`
	Expired          *bool            `json:"expired,omitempty"`
	Overage          *uint            `json:"overage,omitempty"`
	UserLimit        *uint            `json:"user_limit,omitempty"`
	ActiveUsers      *uint            `json:"active_users,omitempty"`
	Licensee         *json.RawMessage `json:"licensee,omitempty"`
	AddOns           *json.RawMessage `json:"add_ons,omitempty"`
}

type LicenseData struct {
	License *string `json:"license"`
}

// Retrieve information about all licenses
//
// GitLab API docs: https://docs.gitlab.com/ee/api/license.html#retrieve-information-about-all-licenses
func (s *LicenseService) List(ctx context.Context) ([]*License, *Response, error) {
	req, err := s.client.NewRequest("GET", "licenses", nil)
	if err != nil {
		return nil, nil, err
	}

	var licenses []*License
	resp, err := s.client.Request(ctx, req, &licenses)
	if err != nil {
		return nil, resp, err
	}

	return licenses, resp, nil
}

// Retrieve information about the current license
//
// GitLab API docs: https://docs.gitlab.com/ee/api/license.html#retrieve-information-about-the-current-license
func (s *LicenseService) Current(ctx context.Context) (*License, *Response, error) {
	req, err := s.client.NewRequest("GET", "license", nil)
	if err != nil {
		return nil, nil, err
	}

	license := new(License)
	resp, err := s.client.Request(ctx, req, &license)
	if err != nil {
		return nil, resp, err
	}

	return license, resp, nil
}

// Add a new license
//
// GitLab API docs: https://docs.gitlab.com/ee/api/license.html#add-a-new-license
func (s *LicenseService) Create(ctx context.Context, data *LicenseData) (*License, *Response, error) {
	req, err := s.client.NewRequest("POST", "license", data)
	if err != nil {
		return nil, nil, err
	}

	license := new(License)
	resp, err := s.client.Request(ctx, req, &license)
	if err != nil {
		return nil, resp, err
	}

	return license, resp, nil
}

// Delete a license
//
// id: ID of the GitLab license.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/license.html#delete-a-license
func (s *LicenseService) Delete(ctx context.Context, id uint64) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("license/%v", id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
