package gitlab

import (
	"context"
	"fmt"
	"time"
)

type MembersService service

type GroupSamlIdentity struct {
	SamlProviderId *uint64 `json:"saml_provider_id,omitempty"`
	ExternUid      *string `json:"extern_uid,omitempty"`
	Provider       *string `json:"provider,omitempty"`
}

type Member struct {
	Author
	ExpiresAt         *time.Time         `json:"expires_at,omitempty"`
	AccessLevel       *string            `json:"access_level,omitempty"`
	GroupSamlIdentity *GroupSamlIdentity `json:"group_saml_identity,omitempty"`
}

type MemberOptions struct {
	Query   *string `json:"query,omitempty"`
	UserIds *[]uint `json:"user_ids,omitempty"`
}

type MemberData struct {
	UserId      *uint     `json:"user_id,omitempty"`
	AccessLevel *uint     `json:"access_level,omitempty"`
	ExpiresAt   *JSONDate `json:"expires_at,omitempty"`
}

// List all members of a project
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project
func (s *MembersService) ProjectList(ctx context.Context, id string, opts *MemberOptions) ([]*Member, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/members", id), opts)
}

// List all members of a project including inherited members
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project-including-inherited-members
func (s *MembersService) ProjectListAll(ctx context.Context, id string, opts *MemberOptions) ([]*Member, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/members/all", id), opts)
}

// Get a member of a project
//
// id: project ID or path
// user_id: The user ID of the member
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project
func (s *MembersService) ProjectSingle(ctx context.Context, id string, user_id uint64) (*Member, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/members/%v", id, user_id))
}

// Get a member of a project including inherited members
//
// id: project ID or path
// user_id: The user ID of the member
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project
func (s *MembersService) ProjectInheritedSingle(ctx context.Context, id string, user_id uint64) (*Member, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/members/all/%v", id, user_id))
}

// Add a member to a project
//
// id: Project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
func (s *MembersService) AddToProject(ctx context.Context, id string, data MemberData) (interface{}, *Response, error) {
	member := new(Member)
	return s.post(ctx, fmt.Sprintf("projects/%v/members", id), data, &member)
}

// Add a member from a project
//
// id: Project ID or path
// user_id: The user ID of the member
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#edit-a-member-of-a-group-or-project
func (s *MembersService) EditFromProject(ctx context.Context, id string, user_id uint64, data MemberData) (interface{}, *Response, error) {
	member := new(Member)
	return s.post(ctx, fmt.Sprintf("projects/%v/members/%v", id, user_id), data, &member)
}

// Delete a member from project
//
// id: Project ID or path
// user_id: The user ID of the member
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#remove-a-member-from-a-group-or-project
func (s *MembersService) DeleteFromProject(ctx context.Context, id string, user_id uint64) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/members/%v", id, user_id))
}

// List all members of a group
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project
func (s *MembersService) GroupList(ctx context.Context, id string, opts *MemberOptions) ([]*Member, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/members", id), opts)
}

// List all members of a group including inherited members
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project-including-inherited-members
func (s *MembersService) GroupListAll(ctx context.Context, id string, opts *MemberOptions) ([]*Member, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/members/all", id), opts)
}

// Get a member of a group
//
// id: group ID or path
// user_id: The user ID of the member
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project
func (s *MembersService) GroupSingle(ctx context.Context, id string, user_id uint64) (*Member, *Response, error) {
	return s.single(ctx, fmt.Sprintf("groups/%v/members/%v", id, user_id))
}

// Get a member of a group including inherited members
//
// id: group ID or path
// user_id: The user ID of the member
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project
func (s *MembersService) GroupInheritedSingle(ctx context.Context, id string, user_id uint64) (*Member, *Response, error) {
	return s.single(ctx, fmt.Sprintf("groups/%v/members/all/%v", id, user_id))
}

// Add a member to a group
//
// id: Group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
func (s *MembersService) AddToGroup(ctx context.Context, id string, data MemberData) (interface{}, *Response, error) {
	member := new(Member)
	return s.post(ctx, fmt.Sprintf("groups/%v/members", id), data, &member)
}

// Add a member from a group
//
// id: Group ID or path
// user_id: The user ID of the member
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#edit-a-member-of-a-group-or-project
func (s *MembersService) EditFromGroup(ctx context.Context, id string, user_id uint64, data MemberData) (interface{}, *Response, error) {
	member := new(Member)
	return s.post(ctx, fmt.Sprintf("groups/%v/members/%v", id, user_id), data, &member)
}

// Delete a member from group
//
// id: Group ID or path
// user_id: The user ID of the member
//
// GitLab API docs: https://docs.gitlab.com/ee/api/members.html#remove-a-member-from-a-group-or-project
func (s *MembersService) DeleteFromGroup(ctx context.Context, id string, user_id uint64) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("groups/%v/members/%v", id, user_id))
}

func (s *MembersService) list(ctx context.Context, url string, opts *MemberOptions) ([]*Member, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var members []*Member
	resp, err := s.client.Request(ctx, req, &members)
	if err != nil {
		return nil, resp, err
	}

	return members, resp, nil
}

func (s *MembersService) single(ctx context.Context, url string) (*Member, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	member := new(Member)
	resp, err := s.client.Request(ctx, req, &member)
	if err != nil {
		return nil, resp, err
	}

	return member, resp, nil
}

func (s *MembersService) post(ctx context.Context, url string, data interface{}, member interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("POST", url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &member)
	if err != nil {
		return nil, resp, err
	}

	return member, resp, nil
}

func (s *MembersService) put(ctx context.Context, url string, data interface{}, member interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("PUT", url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &member)
	if err != nil {
		return nil, resp, err
	}

	return member, resp, nil
}

func (s *MembersService) delete(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
