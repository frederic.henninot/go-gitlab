package gitlab

import (
	"context"
	"fmt"
	"time"
)

type DeployKeysService service

type DeployKeyData struct {
	Title     *string    `json:"title,omitempty"`
	Key       *string    `json:"key,omitempty"`
	CreatedAt *time.Time `json:"created_at,omitempty"`
	CanPush   *bool      `json:"can_push,omitempty"`
}
type DeployKey struct {
	Id *uint64 `json:"id,omitempty"`
	DeployKeyData
}

// List all deploy keys
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deploy_keys.html#list-all-deploy-keys
func (s *DeployKeysService) All(ctx context.Context, opts *PaginationOptions) ([]*DeployKey, *Response, error) {
	return s.list(ctx, "deploy_keys", opts)
}

// List project deploy keys
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deploy_keys.html#list-project-deploy-keys
func (s *DeployKeysService) List(ctx context.Context, target string, id string, opts *PaginationOptions) ([]*DeployKey, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/deploy_keys", id), opts)
}

// Single deploy key
//
// id: project/group/user ID or path
// key: The key of the attribute
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deploy_keys.html#single-deploy-key
func (s *DeployKeysService) Single(ctx context.Context, id string, key string) (*DeployKey, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/deploy_keys/%v", id, key), nil)
	if err != nil {
		return nil, nil, err
	}

	deployKey := new(DeployKey)
	resp, err := s.client.Request(ctx, req, &deployKey)
	if err != nil {
		return nil, resp, err
	}

	return deployKey, resp, nil
}

// Add deploy key
//
// id: project/group/user ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/broadcast_messages.html#create-a-broadcast-message
func (s *DeployKeysService) Create(ctx context.Context, id string, data *DeployKeyData) (*DeployKey, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/deploy_keys", id), data)
	if err != nil {
		return nil, nil, err
	}

	deployKey := new(DeployKey)
	resp, err := s.client.Request(ctx, req, &deployKey)
	if err != nil {
		return nil, resp, err
	}

	return deployKey, resp, nil
}

// Update deploy key
//
// id: project/group/user ID or path
// key: The key of the attribute
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deploy_keys.html#update-deploy-key
func (s *DeployKeysService) Update(ctx context.Context, id string, key string, data *DeployKeyData) (*DeployKey, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("projects/%v/deploy_keys/%v", id, key), data)
	if err != nil {
		return nil, nil, err
	}

	deployKey := new(DeployKey)
	resp, err := s.client.Request(ctx, req, &deployKey)
	if err != nil {
		return nil, resp, err
	}

	return deployKey, resp, nil
}

// Delete deploy key
//
// id: project/group/user ID or path
// key: The key of the attribute
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deploy_keys.html#delete-deploy-key
func (s *DeployKeysService) Delete(ctx context.Context, id string, key string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/deploy_keys/%v", id, key), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// Enable a deploy key
//
// id: project/group/user ID or path
// key: The key of the attribute
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deploy_keys.html#enable-a-deploy-key
func (s *DeployKeysService) Enable(ctx context.Context, id string, key string) (*DeployKey, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/deploy_keys/%v/enable", id, key), nil)
	if err != nil {
		return nil, nil, err
	}

	deployKey := new(DeployKey)
	resp, err := s.client.Request(ctx, req, &deployKey)
	if err != nil {
		return nil, resp, err
	}

	return deployKey, resp, nil
}

func (s *DeployKeysService) list(ctx context.Context, url string, opts *PaginationOptions) ([]*DeployKey, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var deployKeys []*DeployKey
	resp, err := s.client.Request(ctx, req, &deployKeys)
	if err != nil {
		return nil, resp, err
	}

	return deployKeys, resp, nil
}
