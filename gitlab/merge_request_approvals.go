package gitlab

import (
	"context"
	"fmt"
	"time"
)

type MergeRequestApprovalsService service

type ApprovalConfig struct {
	ApprovalsBeforeMerge                      *uint64 `json:"approvals_before_merge,omitempty"`
	ResetApprovalsOnPush                      *bool   `json:"reset_approvals_on_push,omitempty"`
	DisableOverridingApproversPerMergeRequest *bool   `json:"disable_overriding_approvers_per_merge_request,omitempty"`
	MergeRequestsAuthorApproval               *bool   `json:"merge_requests_author_approval,omitempty"`
	MergeRequestsDisableCommittersApproval    *bool   `json:"merge_requests_disable_committers_approval,omitempty"`
	RequirePasswordToApprove                  *bool   `json:"require_password_to_approve,omitempty"`
}

type ApprovalRule struct {
	Id                *uint64            `json:"id,omitempty"`
	Name              *string            `json:"name,omitempty"`
	RuleType          *string            `json:"rule_type,omitempty"`
	EligibleApprovers *[]Author          `json:"eligible_approvers,omitempty"`
	ApprovalsRequired *uint              `json:"approvals_required,omitempty"`
	Users             *[]Author          `json:"users,omitempty"`
	Groups            *[]Groupe          `json:"groups,omitempty"`
	ProtectedBranches *[]ProtectedBranch `json:"protected_branches,omitempty"`
}

type MergeRequestApprovalConfigData struct {
	ApprovalsRequired *uint `json:"approvals_required,omitempty"`
}

type ApprovalData struct {
	Name               *string `json:"name,omitempty"`
	ApprovalsRequired  *uint   `json:"approvals_required,omitempty"`
	UserIds            *[]uint `json:"user_ids,omitempty"`
	GroupIds           *[]uint `json:"group_ids,omitempty"`
	ProtectedBranchIds *[]uint `json:"protected_branch_ids,omitempty"`
}

type ApproverData struct {
	ApproverIds *[]uint `json:"approver_ids,omitempty"`
	GroupIds    *[]uint `json:"group_ids,omitempty"`
}

type MergeRequestApproval struct {
	Id                *uint64    `json:"id,omitempty"`
	Iid               *uint64    `json:"iid,omitempty"`
	ProjectId         *uint64    `json:"project_id,omitempty"`
	Title             *string    `json:"title,omitempty"`
	Description       *string    `json:"description,omitempty"`
	State             *string    `json:"state,omitempty"`
	CreatedAt         *time.Time `json:"created_at,omitempty"`
	UpdatedAt         *time.Time `json:"updated_at,omitempty"`
	MergeStatus       *string    `json:"merge_status,omitempty"`
	ApprovalsRequired *uint      `json:"approvals_required,omitempty"`
	ApprovalsLeft     *uint      `json:"approvals_left,omitempty"`
	ApprovedBy        *Author    `json:"approved_by,omitempty"`
	Approvers         *[]Author  `json:"approvers,omitempty"`
	ApproverGroups    *[]Groupe  `json:"approver_groups,omitempty"`
}

type MergeRequestRule struct {
	Id                   *uint64   `json:"id,omitempty"`
	Name                 *string   `json:"name,omitempty"`
	RuleType             *string   `json:"rule_type,omitempty"`
	EligibleApprovers    *[]Author `json:"eligible_approvers,omitempty"`
	ApprovalsRequired    *uint     `json:"approvals_required,omitempty"`
	Users                *[]Author `json:"users,omitempty"`
	Groups               *[]Groupe `json:"groups,omitempty"`
	ContainsHiddenGroups *bool     `json:"contains_hidden_groups,omitempty"`
	ApprovedBy           *Author   `json:"approved_by,omitempty"`
	SourceRule           *string   `json:"source_rule,omitempty"`
	Approved             *bool     `json:"approved,omitempty"`
	Overridden           *string   `json:"overridden,omitempty"`
}

type MergeRequestRules struct {
	ApprovalRulesOverwritten *bool               `json:"approval_rules_overwritten,omitempty"`
	Rules                    *[]MergeRequestRule `json:"rules,omitempty"`
}

type MergeRequestRuleData struct {
	Name                  *string `json:"name,omitempty"`
	ApprovalsRequired     *uint   `json:"approvals_required,omitempty"`
	UserIds               *[]uint `json:"user_ids,omitempty"`
	GroupIds              *[]uint `json:"group_ids,omitempty"`
	ApprovalProjectRuleId *uint   `json:"approval_project_rule_id,omitempty"`

	ProtectedBranchIds   *[]uint   `json:"protected_branch_ids,omitempty"`
	RuleType             *string   `json:"rule_type,omitempty"`
	EligibleApprovers    *[]Author `json:"eligible_approvers,omitempty"`
	Users                *[]Author `json:"users,omitempty"`
	Groups               *[]Groupe `json:"groups,omitempty"`
	ContainsHiddenGroups *bool     `json:"contains_hidden_groups,omitempty"`
	ApprovedBy           *Author   `json:"approved_by,omitempty"`
	SourceRule           *string   `json:"source_rule,omitempty"`
	Approved             *bool     `json:"approved,omitempty"`
	Overridden           *string   `json:"overridden,omitempty"`
}

type MergeRequestApproveData struct {
	Sha              *string `json:"sha,omitempty"`
	ApprovalPassword *string `json:"approval_password,omitempty"`
}

// Get Project Level MR Approval Configuration
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#change-configuration
func (s *MergeRequestApprovalsService) ProjectConfig(ctx context.Context, id string) (interface{}, *Response, error) {
	approvalConfig := new(ApprovalConfig)
	return s.single(ctx, fmt.Sprintf("projects/%v/approvals", id), approvalConfig)
}

// Project Level MR Approval Configuration Change
//
// id: Project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#change-configuration
func (s *IssuesService) ProjectConfigUpdate(ctx context.Context, id string, data ApprovalConfig) (interface{}, *Response, error) {
	approvalConfig := new(ApprovalConfig)
	return s.post(ctx, fmt.Sprintf("projects/%v/approvals", id), &data, &approvalConfig)
}

// Project Level MR Approval Rules
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#get-project-level-rules
func (s *MergeRequestApprovalsService) ProjectRules(ctx context.Context, id string) (interface{}, *Response, error) {
	var rules []*ApprovalRule
	return s.list(ctx, fmt.Sprintf("projects/%v/approval_rules", id), nil, rules)
}

// Project Level MR Approval Create Rule
//
// id: Project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#create-project-level-rule
func (s *MergeRequestApprovalsService) ProjectRuleCreate(ctx context.Context, id string, data ApprovalData) (interface{}, *Response, error) {
	approvalRule := new(ApprovalRule)
	return s.post(ctx, fmt.Sprintf("projects/%v/approval_rules", id), &data, &approvalRule)
}

// Project Level MR Approval Update Rule
//
// id: Project ID or path
// rule_id: The ID of a approval rule
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#update-project-level-rule
func (s *MergeRequestApprovalsService) ProjectRuleUpdate(ctx context.Context, id string, rule_id uint, data ApprovalData) (interface{}, *Response, error) {
	approvalRule := new(ApprovalRule)
	return s.put(ctx, fmt.Sprintf("projects/%v/approval_rules/%v", id, rule_id), &data, &approvalRule)
}

// Project Level MR Approval Delete Rule
//
// id: Project ID
// rule_id: The ID of a approval rule
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#delete-project-level-rule
func (s *MergeRequestApprovalsService) ProjectRuleDelete(ctx context.Context, id string, rule_id uint) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/approval_rules/%v", id, rule_id))
}

// Project Level MR Approval Change Approvers
//
// id: Project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#change-allowed-approvers
func (s *MergeRequestApprovalsService) ProjectApproversChange(ctx context.Context, id string, data ApproverData) (interface{}, *Response, error) {
	approvalRule := new(ApprovalRule)
	return s.put(ctx, fmt.Sprintf("projects/%v/approvers", id), &data, &approvalRule)
}

// Get Merge Request Level MR Approval Configuration
//
// id: project ID or path
// merge_request_iid: The IID of MR
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#get-configuration-1
func (s *MergeRequestApprovalsService) MergeRequestConfig(ctx context.Context, id string, merge_request_iid uint) (interface{}, *Response, error) {
	mergeRequestApproval := new(MergeRequestApproval)
	return s.single(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approvals", id, merge_request_iid), mergeRequestApproval)
}

// Get Merge Request Level MR Approval Configuration Change
//
// id: Project ID or path
// merge_request_iid: The IID of MR
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#change-approval-configuration
func (s *IssuesService) MergeRequestConfigUpdate(ctx context.Context, id string, merge_request_iid uint, data MergeRequestApprovalConfigData) (interface{}, *Response, error) {
	mergeRequestApproval := new(MergeRequestApproval)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approvals", id, merge_request_iid), &data, &mergeRequestApproval)
}

// Merge Request Level MR Approval Change Approvers
//
// id: Project ID or path
// merge_request_iid: The IID of MR
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#change-allowed-approvers
func (s *MergeRequestApprovalsService) MergeRequestApproversChange(ctx context.Context, id string, merge_request_iid uint, data ApproverData) (interface{}, *Response, error) {
	mergeRequestApproval := new(MergeRequestApproval)
	return s.put(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approvers", id, merge_request_iid), &data, &mergeRequestApproval)
}

// Get the approval state of merge requests
//
// id: project ID or path
// merge_request_iid: The IID of MR
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#get-the-approval-state-of-merge-requests
func (s *MergeRequestApprovalsService) MergeRequestApprovalState(ctx context.Context, id string, merge_request_iid uint) (interface{}, *Response, error) {
	mergeRequestRules := new(MergeRequestRules)
	return s.single(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approval_state", id, merge_request_iid), mergeRequestRules)
}

// Get merge request level rules
//
// id: project ID or path
// merge_request_iid: The IID of MR
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#get-merge-request-level-rules
func (s *MergeRequestApprovalsService) MergeRequestRules(ctx context.Context, id string, merge_request_iid uint) (interface{}, *Response, error) {
	var mergeRequestRules []*MergeRequestRule
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approval_rules", id, merge_request_iid), nil, mergeRequestRules)
}

// merge request Level MR Approval Create Rule
//
// id: Project ID or path
// merge_request_iid: The IID of MR
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#create-merge-request-level-rule
func (s *MergeRequestApprovalsService) MergeRequestRuleCreate(ctx context.Context, id string, merge_request_iid uint, data MergeRequestRuleData) (interface{}, *Response, error) {
	mergeRequestRule := new(MergeRequestRule)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approval_rules", id, merge_request_iid), &data, &mergeRequestRule)
}

// merge request Level MR Approval Update Rule
//
// id: Project ID or path
// merge_request_iid: The IID of MR
// rule_id: The ID of a approval rule
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#update-merge-request-level-rule
func (s *MergeRequestApprovalsService) MergeRequestRuleUpdate(ctx context.Context, id string, merge_request_iid uint, rule_id uint, data MergeRequestRuleData) (interface{}, *Response, error) {
	mergeRequestRule := new(MergeRequestRule)
	return s.put(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approval_rules/%v", id, merge_request_iid, rule_id), &data, &mergeRequestRule)
}

// merge request Level MR Approval Delete Rule
//
// id: Project ID
// merge_request_iid: The IID of MR
// rule_id: The ID of a approval rule
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#delete-merge-request-level-rule
func (s *MergeRequestApprovalsService) MergeRequestRuleDelete(ctx context.Context, id string, merge_request_iid uint, rule_id uint) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approval_rules/%v", id, merge_request_iid, rule_id))
}

// Approve Merge Request
//
// id: Project ID or path
// merge_request_iid: The IID of MR
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#approve-merge-request
func (s *MergeRequestApprovalsService) Approve(ctx context.Context, id string, merge_request_iid uint, data MergeRequestApproveData) (interface{}, *Response, error) {
	mergeRequestRule := new(MergeRequestRule)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/approve", id, merge_request_iid), &data, &mergeRequestRule)
}

// Unapprove Merge Request
//
// id: Project ID or path
// merge_request_iid: The IID of MR
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_request_approvals.html#unapprove-merge-request
func (s *MergeRequestApprovalsService) Unapprove(ctx context.Context, id string, merge_request_iid uint) (interface{}, *Response, error) {
	mergeRequestRule := new(MergeRequestRule)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/unapprove", id, merge_request_iid), nil, &mergeRequestRule)
}

func (s *MergeRequestApprovalsService) list(ctx context.Context, url string, opts interface{}, datas interface{}) (interface{}, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &datas)
	if err != nil {
		return nil, resp, err
	}

	return datas, resp, nil
}

func (s *MergeRequestApprovalsService) single(ctx context.Context, url string, data interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &data)
	if err != nil {
		return nil, resp, err
	}

	return data, resp, nil
}

func (s *MergeRequestApprovalsService) post(ctx context.Context, url string, data interface{}, result interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("POST", url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &result)
	if err != nil {
		return nil, resp, err
	}

	return result, resp, nil
}

func (s *MergeRequestApprovalsService) put(ctx context.Context, url string, data interface{}, result interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("PUT", url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &result)
	if err != nil {
		return nil, resp, err
	}

	return result, resp, nil
}

func (s *MergeRequestApprovalsService) delete(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
