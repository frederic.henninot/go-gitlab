package gitlab

import (
	"context"
	"fmt"
	"time"
)

type EventsService service

type Event struct {
	Title          *string    `json:"title,omitempty"`
	ProjectId      *uint64    `json:"project_id,omitempty"`
	ActionName     *string    `json:"action_name,omitempty"`
	TargetId       *uint64    `json:"target_id,omitempty"`
	TargetIid      *uint64    `json:"target_iid,omitempty"`
	TargetType     *string    `json:"target_type,omitempty"`
	AuthorId       *uint64    `json:"author_id,omitempty"`
	TargetTitle    *string    `json:"target_title,omitempty"`
	AuthorUsername *string    `json:"author_username,omitempty"`
	Author         *Author    `json:"author,omitempty"`
	CreatedAt      *time.Time `json:"created_at,omitempty"`
}

type EventOption struct {
	Action     *string   `json:"action,omitempty"`
	TargetType *string   `json:"target_type,omitempty"`
	Before     *JSONDate `json:"before,omitempty"`
	After      *JSONDate `json:"after,omitempty"`
	Scope      *string   `json:"scope,omitempty"`
	Sort       *string   `json:"sort,omitempty"`
}

// List currently authenticated user’s events
//
// GitLab API docs: https://docs.gitlab.com/ee/api/events.html#list-currently-authenticated-users-events
func (s *EventsService) List(ctx context.Context, opts *EventOption) ([]*Event, *Response, error) {
	return s.list(ctx, "events", opts)
}

// Get user contribution events
//
// user_id: The ID or Username of the user
//
// GitLab API docs: https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events
func (s *EventsService) User(ctx context.Context, user_id string, opts *EventOption) ([]*Event, *Response, error) {
	return s.list(ctx, fmt.Sprintf("users/%v/events", user_id), opts)
}

// Get user contribution events
//
// project_id: The ID or URL-encoded path of the project
//
// GitLab API docs: https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events
func (s *EventsService) Project(ctx context.Context, project_id string, opts *EventOption) ([]*Event, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/events", project_id), opts)
}

func (s *EventsService) list(ctx context.Context, url string, opts *EventOption) ([]*Event, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var events []*Event
	resp, err := s.client.Request(ctx, req, &events)
	if err != nil {
		return nil, resp, err
	}

	return events, resp, nil
}
