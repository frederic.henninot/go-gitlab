package gitlab

import "context"

type ApplicationSettingsService service

type ApplicationSettings struct {
	AdminNotificationEmail                    *string   `json:"admin_notification_email,omitempty"`
	AfterSignOutPath                          *string   `json:"after_sign_out_path,omitempty"`
	AfterSignInText                           *string   `json:"after_sign_up_text,omitempty"`
	AkismetApiKey                             *string   `json:"akismet_api_key,omitempty"`
	AkismetEnabled                            *bool     `json:"akismet_enabled,omitempty"`
	AllowGroupOwnersToManageLdap              *bool     `json:"allow_group_owners_to_manage_ldap,omitempty"`
	AllowLocalRequestsFromHooksAndServices    *bool     `json:"allow_local_requests_from_hooks_and_services,omitempty"`
	AllowLocalRequestsFromSystemHooks         *bool     `json:"allow_local_requests_from_system_hooks,omitempty"`
	AllowLocalRequestsFromWebHooksAndServices *bool     `json:"allow_local_requests_from_web_hooks_and_services,omitempty"`
	ArchiveBuildsInHumanReadable              *bool     `json:"archive_builds_in_human_readable,omitempty"`
	AssetProxyEnabled                         *bool     `json:"asset_proxy_enabled,omitempty"`
	AssetProxySecretKey                       *string   `json:"asset_proxy_secret_key,omitempty"`
	AssetProxyUrl                             *string   `json:"asset_proxy_url,omitempty"`
	AssetProxyWhitelist                       *[]string `json:"asset_proxy_whitelist,omitempty"`
	AuthorizedKeysEnabled                     *bool     `json:"authorized_keys_enabled,omitempty"`
	AutoDevopsDomain                          *string   `json:"auto_devops_domain,omitempty"`
	AutoDevopsEnabled                         *bool     `json:"auto_devops_enabled,omitempty"`
	CheckNamespacePlan                        *bool     `json:"check_namespace_plan,omitempty"`
	CommitEmailHostname                       *string   `json:"commit_email_hostname,omitempty"`
	ContainerRegistryTokenExpireDelay         *uint16   `json:"container_registry_token_expire_delay,omitempty"`
	DefaultArtifactsExpireIn                  *string   `json:"default_artifacts_expire_in,omitempty"`
	DefaultBranchProtection                   *uint8    `json:"default_branch_protection,omitempty"`
	DefaultCiConfigPath                       *string   `json:"default_ci_config_path,omitempty"`
	DefaultGroupVisibility                    *string   `json:"default_group_visibility,omitempty"`
	DefaultProjectCreation                    *uint8    `json:"default_project_creation,omitempty"`
	DefaultProjectLimit                       *uint8    `json:"default_projects_limit,omitempty"`
	DefaultProjectVisibility                  *string   `json:"default_project_visibility,omitempty"`
	DefaultSnippetVisibility                  *string   `json:"default_snippet_visibility,omitempty"`
	DiffMaxPatchBytes                         *uint64   `json:"diff_max_patch_bytes,omitempty"`
	DisabledOauthSignInSources                *[]string `json:"disabled_oauth_sign_in_sources,omitempty"`
	DnsRebindingProtectionEnabled             *bool     `json:"dns_rebinding_protection_enabled,omitempty"`
	DomainBlacklist                           *[]string `json:"domain_blacklist,omitempty"`
	DomainBlacklistEnabled                    *bool     `json:"domain_blacklist_enabled,omitempty"`
	DomainWhitelist                           *[]string `json:"domain_whitelist,omitempty"`
	DsaKeyRestriction                         *int8     `json:"dsa_key_restriction,omitempty"`
	EcdsaKeyRestriction                       *int8     `json:"ecdsa_key_restriction,omitempty"`
	Ed25519KeyRestriction                     *int8     `json:"ed25519_key_restriction,omitempty"`
	EksIntegrationEnabled                     *bool     `json:"eks_integration_enabled,omitempty"`
	EksAccountId                              *string   `json:"eks_account_id,omitempty"`
	EksAccessKey                              *string   `json:"eks_access_key_id,omitempty"`
	EksSecretAccessKey                        *string   `json:"eks_secret_access_key,omitempty"`
	ElasticSearchAwsAccessKey                 *string   `json:"elasticsearch_aws_access_key,omitempty"`
	ElasticSearchAws                          *bool     `json:"elasticsearch_aws,omitempty"`
	ElasticSearchAwsRegion                    *string   `json:"elasticsearch_aws_region,omitempty"`
	ElasticSearchAwsSecretAccessKey           *string   `json:"elasticsearch_aws_secret_access_key,omitempty"`
	ElasticSearchIndexing                     *bool     `json:"elasticsearch_indexing,omitempty"`
	ElasticSearchLimitIndexing                *bool     `json:"elasticsearch_limit_indexing,omitempty"`
	ElasticSearchNamespaceIds                 *[]uint64 `json:"elasticsearch_namespace_ids,omitempty"`
	ElasticSearchProjectIds                   *[]uint64 `json:"elasticsearch_project_ids,omitempty"`
	ElasticSearchSearch                       *bool     `json:"elasticsearch_search,omitempty"`
	ElasticSearchUrl                          *string   `json:"elasticsearch_url,omitempty"`
	EmailAdditionalText                       *string   `json:"email_additional_text,omitempty"`
	EmailAuthorInBody                         *bool     `json:"email_author_in_body,omitempty"`
	EnableGitAccessProtocol                   *string   `json:"enabled_git_access_protocol,omitempty"`
	EnforceTerms                              *bool     `json:"enforce_terms,omitempty"`
	ExternalAuthClientCert                    *string   `json:"external_auth_client_cert,omitempty"`
	ExternalAuthClientKeyPass                 *string   `json:"external_auth_client_key_pass,omitempty"`
	ExternalAuthClientKey                     *string   `json:"external_auth_client_key,omitempty"`
	ExternalAuthorizationServiceDefaultLabel  *string   `json:"external_authorization_service_default_label,omitempty"`
	ExternalAuthorizationServiceEnabled       *bool     `json:"external_authorization_service_enabled,omitempty"`
	ExternalAuthorizationServiceTimeout       *float32  `json:"external_authorization_service_timeout,omitempty"`
	ExternalAuthorizationServiceUrl           *string   `json:"external_authorization_service_url,omitempty"`
	FileTemplateId                            *uint64   `json:"file_template_project_id,omitempty"`
	FirstDayOfWeek                            *uint8    `json:"first_day_of_week,omitempty"`
	GeoNodeAllowedIps                         *string   `json:"geo_node_allowed_ips,omitempty"`
	GeoStatusTimeout                          *uint64   `json:"geo_status_timeout,omitempty"`
	GitalyTimeoutDefault                      *uint64   `json:"gitaly_timeout_default,omitempty"`
	GitalyTimeoutFast                         *uint64   `json:"gitaly_timeout_fast,omitempty"`
	GitalyTimeoutMedium                       *uint64   `json:"gitaly_timeout_medium,omitempty"`
	GrafanaEnabled                            *bool     `json:"grafana_enabled,omitempty"`
	GrafaUrl                                  *string   `json:"grafana_url,omitempty"`
	GravatarEnabled                           *bool     `json:"gravatar_enabled,omitempty"`
	HashedStorageEnabled                      *bool     `json:"hashed_storage_enabled,omitempty"`
	HelpPageHideCommercialContent             *bool     `json:"help_page_hide_commercial_content,omitempty"`
	HelpPageSupportUrl                        *string   `json:"help_page_support_url,omitempty"`
	HelpPageText                              *string   `json:"help_page_text,omitempty"`
	HelpText                                  *string   `json:"help_text,omitempty"`
	HideThirdPartyOffers                      *bool     `json:"hide_third_party_offers,omitempty"`
	HomePageUrl                               *string   `json:"home_page_url,omitempty"`
	HousekeepingBitmapsEnabled                *bool     `json:"housekeeping_bitmaps_enabled,omitempty"`
	HousekeepingEnabled                       *bool     `json:"housekeeping_enabled,omitempty"`
	HousekeepingFullRepackPeriod              *uint64   `json:"housekeeping_full_repack_period,omitempty"`
	HousekeepingGcPeriod                      *uint64   `json:"housekeeping_gc_period,omitempty"`
	HousekeepingIncrementalRepackPeriod       *uint64   `json:"housekeeping_incremental_repack_period,omitempty"`
	HtmlEmailsEnabled                         *bool     `json:"html_emails_enabled,omitempty"`
	ImportSources                             *[]string `json:"import_sources,omitempty"`
	InstanceStatisticsVisibilityPrivate       *bool     `json:"instance_statistics_visibility_private,omitempty"`
	LocalMarkdownVersion                      *uint8    `json:"local_markdown_version,omitempty"`
	MaxArtifactsSize                          *uint64   `json:"max_artifacts_size,omitempty"`
	MaxAttachmentSize                         *uint64   `json:"max_attachment_size,omitempty"`
	MaxPagesSize                              *uint64   `json:"max_pages_size,omitempty"`
	MaxPersonnalAccessTokenLifetime           *uint64   `json:"max_personal_access_token_lifetime,omitempty"`
	MetricsEnabled                            *bool     `json:"metrics_enabled,omitempty"`
	MetricsHost                               *string   `json:"metrics_host,omitempty"`
	MetricsMethodCallThreshold                *uint64   `json:"metrics_method_call_threshold,omitempty"`
	MetricsPacketSize                         *uint64   `json:"metrics_packet_size,omitempty"`
	MetricsPoolSize                           *uint64   `json:"metrics_pool_size,omitempty"`
	MetricsPort                               *uint16   `json:"metrics_port,omitempty"`
	MetricsSampleInterval                     *uint16   `json:"metrics_sample_interval,omitempty"`
	MetricsTimeout                            *uint16   `json:"metrics_timeout,omitempty"`
	MirrorAvailable                           *bool     `json:"mirror_available,omitempty"`
	MirrorCapacityThreshold                   *uint64   `json:"mirror_capacity_threshold,omitempty"`
	MirrorMaxCapacity                         *uint64   `json:"mirror_max_capacity,omitempty"`
	MirrorMaxDelay                            *uint16   `json:"mirror_max_delay,omitempty"`
	OutboundLocalRequestsWhitelist            *[]string `json:"outbound_local_requests_whitelist,omitempty"`
	PageDomainVerificationEnabled             *bool     `json:"pages_domain_verification_enabled,omitempty"`
	PasswordAuthenticationEnabled             *bool     `json:"password_authentication_enabled_for_git,omitempty"`
	PasswordAuthenticationEnabledForWeb       *bool     `json:"password_authentication_enabled_for_web,omitempty"`
	PerformanceBarAllowedGroupId              *string   `json:"performance_bar_allowed_group_id,omitempty"`
	PerformanceBarAllowedGroupPath            *string   `json:"performance_bar_allowed_group_path,omitempty"`
	PerformanceBarEnabled                     *bool     `json:"performance_bar_enabled,omitempty"`
	PlantUmlEnabled                           *bool     `json:"plantuml_enabled,omitempty"`
	PlantUmlUrl                               *string   `json:"plantuml_url,omitempty"`
	PollingIntervalMultiplier                 *float32  `json:"polling_interval_multiplier,omitempty"`
	DeletionAdjournedPeriod                   *uint16   `json:"deletion_adjourned_period,omitempty"`
	ProjectExportEnabled                      *bool     `json:"project_export_enabled,omitempty"`
	PrometheusMetricsEnabled                  *bool     `json:"prometheus_metrics_enabled,omitempty"`
	ProtectedCiVariables                      *bool     `json:"protected_ci_variables,omitempty"`
	PseudonymizerEnabled                      *bool     `json:"pseudonymizer_enabled,omitempty"`
	PushEventHooksLimit                       *uint16   `json:"push_event_hooks_limit,omitempty"`
	PushEventActivitiesLimit                  *uint16   `json:"push_event_activities_limit,omitempty"`
	RecaptchaEnabled                          *bool     `json:"recaptcha_enabled,omitempty"`
	RecaptchaPrivateKey                       *string   `json:"recaptcha_private_key,omitempty"`
	RecaptchaSiteKey                          *string   `json:"recaptcha_site_key,omitempty"`
	ReceiveMaxInputSize                       *uint16   `json:"receive_max_input_size,omitempty"`
	RepositoryCheckEnabled                    *bool     `json:"repository_checks_enabled,omitempty"`
	RepositorySizeLimit                       *uint64   `json:"repository_size_limit,omitempty"`
	RepositoryStorages                        *[]string `json:"repository_storages,omitempty"`
	RequireTwoFactorAuthentication            *bool     `json:"require_two_factor_authentication,omitempty"`
	RestrictedVisibilityLevels                *[]string `json:"require_two_factor_authentication,omitempty"`
	RsaKeyRestriction                         *int8     `json:"rsa_key_restriction,omitempty"`
	SendUserConfirmationEmail                 *bool     `json:"send_user_confirmation_email,omitempty"`
	SessionExpireDelay                        *uint16   `json:"session_expire_delay,omitempty"`
	SharedRunnersEnabled                      *bool     `json:"shared_runners_enabled,omitempty"`
	SharedRunnersMinutes                      *uint16   `json:"shared_runners_minutes,omitempty"`
	SharedRunnersText                         *string   `json:"shared_runners_text,omitempty"`
	SigninEnabled                             *bool     `json:"signin_enabled,omitempty"`
	SigninText                                *string   `json:"sign_in_text,omitempty"`
	SignupEnabled                             *bool     `json:"signup_enabled,omitempty"`
	SlackEnabled                              *bool     `json:"slack_app_enabled,omitempty"`
	SlackAppId                                *string   `json:"slack_app_id,omitempty"`
	SlackAppSecret                            *string   `json:"slack_app_secret,omitempty"`
	SlackAppVerificationToken                 *string   `json:"slack_app_verification_token,omitempty"`
	SnowplowCollectorHostname                 *string   `json:"snowplow_collector_hostname,omitempty"`
	SnowplowCookieDomain                      *string   `json:"snowplow_cookie_domain,omitempty"`
	SnowplowEnabled                           *bool     `json:"snowplow_enabled,omitempty"`
	SnowplowAppId                             *string   `json:"snowplow_app_id,omitempty"`
	SnowplowIgluRegistryUrl                   *string   `json:"snowplow_iglu_registry_url,omitempty"`
	SourcegraphEnabled                        *bool     `json:"sourcegraph_enabled,omitempty"`
	SourcegraphUrl                            *string   `json:"sourcegraph_url,omitempty"`
	SourcegraphPublicOnly                     *bool     `json:"sourcegraph_public_only,omitempty"`
	TerminalMaxSessionTime                    *uint16   `json:"terminal_max_session_time,omitempty"`
	Terms                                     *string   `json:"terms,omitempty"`
	ThrottleAuthenticatedApiEnabled           *bool     `json:"throttle_authenticated_api_enabled,omitempty"`
	ThrottleAuthenticatedApiPeriodInSeconds   *uint16   `json:"throttle_authenticated_api_period_in_seconds,omitempty"`
	ThrottleAuthenticatedApiRequestsPerPeriod *uint16   `json:"throttle_authenticated_api_requests_per_period,omitempty"`
	ThrottleAuthenticatedWebEnabled           *bool     `json:"throttle_authenticated_web_enabled,omitempty"`
	ThrottleAuthenticatedWebPeriodInSeconds   *uint16   `json:"throttle_authenticated_web_period_in_seconds,omitempty"`
	ThrottleAuthenticatedWebRequestsPerPeriod *bool     `json:"throttle_authenticated_web_requests_per_period,omitempty"`
	ThrottleUnauthenticatedEnabled            *bool     `json:"throttle_unauthenticated_enabled,omitempty"`
	ThrottleUnauthenticatedPeriodInSeconds    *bool     `json:"throttle_unauthenticated_period_in_seconds,omitempty"`
	ThrottleUnauthenticatedRequestsPerPeriod  *bool     `json:"throttle_unauthenticated_requests_per_period,omitempty"`
	TimeTrackingLimitToHours                  *bool     `json:"time_tracking_limit_to_hours,omitempty"`
	TwoFactorGracePeriod                      *uint16   `json:"two_factor_grace_period,omitempty"`
	UniqueIpsLimitEnabled                     *bool     `json:"unique_ips_limit_enabled,omitempty"`
	UniqueIpsLimitPerUser                     *uint8    `json:"unique_ips_limit_per_user,omitempty"`
	UniqueIpsLimitTimeWindow                  *uint8    `json:"unique_ips_limit_time_window,omitempty"`
	UsagePingEnabled                          *bool     `json:"usage_ping_enabled,omitempty"`
	UserDefaultExternal                       *bool     `json:"user_default_external,omitempty"`
	UserDefaultInernalRegex                   *string   `json:"user_default_internal_regex,omitempty"`
	UserOauthApplications                     *bool     `json:"user_oauth_applications,omitempty"`
	UserShowAddSshKeyMEssage                  *bool     `json:"user_show_add_ssh_key_message,omitempty"`
	VersionCheckEnabled                       *bool     `json:"version_check_enabled,omitempty"`
	WebIdeClientsidePreviewEnabled            *bool     `json:"web_ide_clientside_preview_enabled,omitempty"`
	SnippetSizeLimit                          *uint64   `json:"snippet_size_limit,omitempty"`
}

// Get application settings
//
// GitLab API docs: https://docs.gitlab.com/ee/api/settings.html#get-current-application-settings
func (s *ApplicationSettingsService) Get(ctx context.Context) (*ApplicationSettings, *Response, error) {
	req, err := s.client.NewRequest("GET", "application/settings", nil)
	if err != nil {
		return nil, nil, err
	}

	applicationSettings := new(ApplicationSettings)
	resp, err := s.client.Request(ctx, req, &applicationSettings)
	if err != nil {
		return nil, resp, err
	}

	return applicationSettings, resp, nil
}

// Change application settings
//
// GitLab API docs: https://docs.gitlab.com/ee/api/settings.html#change-application-settings
func (s *ApplicationSettingsService) Change(ctx context.Context, settings *ApplicationSettings) (*ApplicationSettings, *Response, error) {
	req, err := s.client.NewRequest("PUT", "application/settings", settings)
	if err != nil {
		return nil, nil, err
	}

	applicationSettings := new(ApplicationSettings)
	resp, err := s.client.Request(ctx, req, &applicationSettings)
	if err != nil {
		return nil, resp, err
	}

	return applicationSettings, resp, nil
}
