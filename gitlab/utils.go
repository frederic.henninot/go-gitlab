package gitlab

import (
	"net/url"
	"time"

	"github.com/google/go-querystring/query"
)

const jsonDateLayout = "2006-01-02"

type Error struct {
	Message   *string `json:"message,omitempty"`
	ErrorCode *string `json:"error_code,omitempty"`
}

type Author struct {
	Id        *uint64 `json:"id,omitempty"`
	Name      *string `json:"name,omitempty"`
	Username  *string `json:"username,omitempty"`
	State     *string `json:"state,omitempty"`
	AvatarUrl *string `json:"avatar_url,omitempty"`
	WebUrl    *string `json:"web_url,omitempty"`
}

func addUrlOptions(s string, opts interface{}) (string, error) {
	u, err := url.Parse(s)
	if err != nil {
		return s, err
	}

	qs, err := query.Values(opts)
	if err != nil {
		return s, err
	}

	u.RawQuery = qs.Encode()
	return u.String(), nil
}

// Manage Json Date Format YYYY-MM-DD
type JSONDate struct {
	time.Time
}

func (t *JSONDate) UnmarshalJSON(b []byte) error {
	parsed, err := time.Parse(jsonDateLayout, string(b))
	if err != nil {
		return err
	}

	t.Time = parsed
	return nil
}
func (t *JSONDate) MarshalJSON() ([]byte, error) {
	s := t.Format(jsonDateLayout)
	return []byte(s), nil
}
