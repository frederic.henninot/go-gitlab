package gitlab

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
)

const (
	endpoint       = "/api/v4/"
	authUrlToken   = "url_token"
	authHttpToken  = "http_token"
	authOauthToken = "oauth_token"

	headerRateRemaining = "RateLimit-ResetTime"

	accessLevelNo         = 0
	accessLevelGuest      = 10
	accessLevelReporter   = 20
	accessLevelDevelopper = 30
	accessLevelMaintainer = 40
	accessLevelOwner      = 50
)

type Client struct {
	clientMutex sync.Mutex
	client      *http.Client
	BaseUrl     *url.URL

	AuthMode string
	Token    string

	UserAgent string

	AccessRequest          *AccessRequestService
	ApplicationAppearance  *ApplicationAppearanceService
	ApplicationSettings    *ApplicationSettingsService
	ApplicationStatistics  *ApplicationStatisticsService
	Applications           *ApplicationsService
	AuditEvents            *AuditEventsService
	Avatar                 *AvatarService
	AwardEmoji             *AwardEmojiService
	Badges                 *BadgesService
	Branches               *BranchesService
	BroadcastMessages      *BroadcastMessagesService
	Clusters               *ClustersService
	Commits                *CommitsService
	ContainerRegistry      *ContainerRegistryService
	CustomAttributes       *CustomAttributesService
	Dependencies           *DependenciesService
	DeployKeys             *DeployKeysService
	Deployments            *DeploymentsService
	Discussions            *DiscussionsService
	DockerFiles            *DockerFilesServices
	Environments           *EnvironmentsService
	Epics                  *EpicsService
	EpicLinks              *EpicLinksService
	Events                 *EventsService
	FeaturesFlags          *FeaturesFlagsService
	GeoNodes               *GeoNodesService
	Gitignore              *GitignoreService
	GitlabCi               *GitlabCiService
	GroupActivityAnalytics *GroupActivityAnalyticsService
	Groups                 *GroupsService
	Hooks                  *HooksService
	Import                 *ImportService
	IssueBoards            *IssueBoardsService
	IssueLinks             *IssueLinksService
	IssueStatistics        *IssueStatisticsService
	Issues                 *IssuesService
	Jobs                   *JobsService
	Keys                   *KeysService
	Labels                 *LabelsService
	License                *LicenseService
	Licenses               *LicensesService
	ManagedLicenses        *ManagedLicensesService
	Markdown               *MarkdownService
	Members                *MembersService
	MergeRequestApprovals  *MergeRequestApprovalsService
	MergeRequests          *MergeRequestsService
	Metrics                *MetricsService
	Milestones             *MilestonesService
	Namespaces             *NamespacesService
	Notes                  *NotesService
	NotificationSettings   *NotificationSettingsService
	Packages               *PackagesService
	Pages                  *PagesService
	Pipelines              *PipelinesService
	PipelinesSchedules     *PipelinesSchedulesService
	PipelinesTriggers      *PipelinesTriggersService
	ProjectAliases         *ProjectAliasesService
	ProjectImportExport    *ProjectImportExportService
	ProjectSnippets        *ProjectSnippetsService
	ProjectStatistics      *ProjectStatisticsService
	ProjectTemplates       *ProjectTemplatesService
	Projects               *ProjectsService
	ProtectedBranches      *ProtectedBranchesService
	ProtectedTags          *ProtectedTagsService
	ReleaseLinks           *ReleaseLinksService
	Releases               *ReleasesService
	Repositories           *RepositoriesService
	RepositoryFiles        *RepositoryFilesService
	RepositorySubmodules   *RepositorySubmodulesService
	ResourceLabelEvents    *ResourceLabelEventsService
	Runners                *RunnersService
	Scim                   *ScimService
	Search                 *SearchService
	Services               *ServicesService
	Snippets               *SnippetsService
	Suggestions            *SuggestionsService
	Tags                   *TagsService
	Todos                  *TodosService
	Users                  *UsersService
	Variables              *VariablesService
	Version                *VersionService
	Vulnerabilities        *VulnerabilitiesService
	Wikis                  *WikisService
}

type service struct {
	client *Client
}

type PaginationOptions struct {
	// For paginated result sets, page of results to retrieve.
	Page int `url:"page,omitempty"`

	// For paginated result sets, the number of results to include per page.
	PerPage int `url:"per_page,omitempty"`
}

func NewClient(baseURL string, token string, authMethod string, httpClient *http.Client) (*Client, error) {
	if httpClient == nil {
		httpClient = &http.Client{}
	}
	baseEndpoint, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}
	if !strings.HasSuffix(baseEndpoint.Path, "/") {
		baseEndpoint.Path += "/"
	}
	if !strings.HasSuffix(baseEndpoint.Path, endpoint) {
		baseEndpoint.Path += endpoint
	}

	c := &Client{client: httpClient, BaseUrl: baseEndpoint, Token: token, AuthMode: authMethod}
	c.AccessRequest = &AccessRequestService{client: c}
	c.ApplicationAppearance = &ApplicationAppearanceService{client: c}
	c.ApplicationSettings = &ApplicationSettingsService{client: c}
	c.ApplicationStatistics = &ApplicationStatisticsService{client: c}
	c.Applications = &ApplicationsService{client: c}
	c.AuditEvents = &AuditEventsService{client: c}
	c.Avatar = &AvatarService{client: c}
	c.AwardEmoji = &AwardEmojiService{client: c}
	c.Badges = &BadgesService{client: c}
	c.Branches = &BranchesService{client: c}
	c.BroadcastMessages = &BroadcastMessagesService{client: c}
	c.Clusters = &ClustersService{client: c}
	c.Commits = &CommitsService{client: c}
	c.ContainerRegistry = &ContainerRegistryService{client: c}
	c.CustomAttributes = &CustomAttributesService{client: c}
	c.Dependencies = &DependenciesService{client: c}
	c.DeployKeys = &DeployKeysService{client: c}
	c.Deployments = &DeploymentsService{client: c}
	c.Discussions = &DiscussionsService{client: c}
	c.DockerFiles = &DockerFilesServices{client: c}
	c.Environments = &EnvironmentsService{client: c}
	c.Epics = &EpicsService{client: c}
	c.EpicLinks = &EpicLinksService{client: c}
	c.Events = &EventsService{client: c}
	c.FeaturesFlags = &FeaturesFlagsService{client: c}
	c.GeoNodes = &GeoNodesService{client: c}
	c.Gitignore = &GitignoreService{client: c}
	c.GitlabCi = &GitlabCiService{client: c}
	c.GroupActivityAnalytics = &GroupActivityAnalyticsService{client: c}
	c.Groups = &GroupsService{client: c}
	c.Hooks = &HooksService{client: c}
	c.Import = &ImportService{client: c}
	c.IssueBoards = &IssueBoardsService{client: c}
	c.Issues = &IssuesService{client: c}
	c.IssueStatistics = &IssueStatisticsService{client: c}
	c.Jobs = &JobsService{client: c}
	c.Keys = &KeysService{client: c}
	c.Labels = &LabelsService{client: c}
	c.License = &LicenseService{client: c}
	c.Licenses = &LicensesService{client: c}
	c.IssueLinks = &IssueLinksService{client: c}
	c.ManagedLicenses = &ManagedLicensesService{client: c}
	c.Markdown = &MarkdownService{client: c}
	c.Members = &MembersService{client: c}
	c.MergeRequestApprovals = &MergeRequestApprovalsService{client: c}
	c.MergeRequests = &MergeRequestsService{client: c}

	c.ProtectedBranches = &ProtectedBranchesService{client: c}

	return c, nil
}

func (c *Client) NewRequest(method, urlStr string, body interface{}) (*http.Request, error) {
	if !strings.HasSuffix(c.BaseUrl.Path, "/") {
		return nil, fmt.Errorf("BaseURL must have a trailing slash, but %q does not", c.BaseUrl)
	}
	u, err := c.BaseUrl.Parse(urlStr)
	if err != nil {
		return nil, err
	}
	if authUrlToken == c.AuthMode {
		params := u.Query()
		params.Set("private_token", c.Token)
		u.RawQuery = params.Encode()
	}

	var buf io.ReadWriter
	if body != nil {
		buf = &bytes.Buffer{}
		enc := json.NewEncoder(buf)
		enc.SetEscapeHTML(false)
		err := enc.Encode(body)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}

	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	switch c.AuthMode {
	case authHttpToken:
		req.Header.Set("Private-Token", c.Token)
		break
	case authOauthToken:
		req.Header.Set("Authorization", "Bearer "+c.Token)
		break
	}
	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}
	return req, nil
}

func (c *Client) Request(ctx context.Context, req *http.Request, v interface{}) (*Response, error) {
	if ctx == nil {
		return nil, &ErrorResponse{Message: "context must be non-nil"}
	}
	req = req.WithContext(ctx)

	resp, err := c.client.Do(req)
	if err != nil {
		// If we got an error, and the context has been canceled,
		// the context's error is probably more useful.
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}

		// If the error type is *url.Error, sanitize its URL before returning.
		if e, ok := err.(*url.Error); ok {
			if url, err := url.Parse(e.URL); err == nil {
				e.URL = url.String()
				return nil, e
			}
		}

		return nil, err
	}
	defer resp.Body.Close()

	response := newResponse(resp)

	err = CheckResponse(resp)
	if err != nil {
		return response, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			io.Copy(w, resp.Body)
		} else {
			decErr := json.NewDecoder(resp.Body).Decode(v)
			if decErr == io.EOF {
				decErr = nil // ignore EOF errors caused by empty response body
			}
			if decErr != nil {
				err = decErr
			}
		}
	}

	return response, err
}

type Response struct {
	*http.Response

	NextPage  int
	PrevPage  int
	FirstPage int
	LastPage  int

	NextPageToken string
}

func newResponse(r *http.Response) *Response {
	response := &Response{Response: r}
	response.populatePageValues()
	return response
}

func (r *Response) populatePageValues() {
	if p := r.Response.Header.Get("X-Page"); p != "" {
		page, _ := strconv.Atoi(p)
		if page >= 1 {
			r.FirstPage = 1
		}
	}
	if p := r.Response.Header.Get("X-Next-Page"); p != "" {
		r.NextPage, _ = strconv.Atoi(p)
	}
	if p := r.Response.Header.Get("X-Prev-Page"); p != "" {
		r.PrevPage, _ = strconv.Atoi(p)
	}
	if p := r.Response.Header.Get("X-Total-Pages"); p != "" {
		r.LastPage, _ = strconv.Atoi(p)
	}
}

type ErrorResponse struct {
	Response *http.Response // HTTP response that caused this error
	Message  string         `json:"message"` // error message
}

func (r *ErrorResponse) Error() string {
	return r.Message
}

func CheckResponse(r *http.Response) error {
	if c := r.StatusCode; 200 <= c && c <= 299 {
		return nil
	}
	errorResponse := &ErrorResponse{Response: r}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && data != nil {
		json.Unmarshal(data, errorResponse)
	}

	r.Body = ioutil.NopCloser(bytes.NewBuffer(data))
	switch {
	case r.StatusCode == http.StatusForbidden:
		errorResponse.Message = "Forbidden"
		return errorResponse
	case r.StatusCode == http.StatusUnauthorized:
		errorResponse.Message = "Unauthorized"
		return errorResponse
	case r.StatusCode == http.StatusTooManyRequests:
		errorResponse.Message = "Too Many Request"
		return errorResponse
	default:
		return errorResponse
	}
}
