package gitlab

import (
	"context"
	"fmt"
)

type LabelsService service

type Label struct {
	Id                     *uint64 `json:"id,omitempty"`
	Name                   *string `json:"name,omitempty"`
	Color                  *string `json:"color,omitempty"`
	TextColor              *string `json:"text_color,omitempty"`
	Description            *string `json:"description,omitempty"`
	DescriptionHtml        *string `json:"description_html,omitempty"`
	OpenIssuesCount        *uint   `json:"open_issues_count,omitempty"`
	ClosedIssuesCount      *uint   `json:"closed_issues_count,omitempty"`
	OpenMergeRequestsCount *uint   `json:"open_merge_requests_count,omitempty"`
	Subscribed             *bool   `json:"subscribed,omitempty"`
	Piority                *uint   `json:"priority,omitempty"`
	IsProjectLabel         *bool   `json:"is_project_label,omitempty"`
}

type LabelOption struct {
	WithCounts            *bool `json:"with_counts,omitempty"`
	IncludeAncestorGroups *bool `json:"include_ancestor_groups,omitempty"`
}
type LabelData struct {
	Color       *string `json:"color,omitempty"`
	Description *string `json:"description,omitempty"`
	Piority     *uint   `json:"priority,omitempty"`
}
type CreateLabelData struct {
	Name *string `json:"name,omitempty"`
	LabelData
}
type UpdateLabelData struct {
	NewName *string `json:"new_name,omitempty"`
	LabelData
}

// List labels of given project
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#list-labels
func (s *LabelsService) ProjectList(ctx context.Context, id string, opts *LabelOption) ([]*Label, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/labels", id), opts)
}

// Get a single project label
//
// id: project ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#get-a-single-project-label
func (s *LabelsService) ProjectSingle(ctx context.Context, id string, label_id string) (*Label, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/labels/%v", id, label_id))
}

// Create a new label
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#create-a-new-label
func (s *LabelsService) ProjectCreate(ctx context.Context, id string, opts *CreateLabelData) (*Label, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/labels", id), opts)
}

// Edit an existing label
//
// id: project ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#edit-an-existing-label
func (s *LabelsService) ProjectUpdate(ctx context.Context, id string, label_id string, opts *UpdateLabelData) (*Label, *Response, error) {
	return s.put(ctx, fmt.Sprintf("projects/%v/labels/%v", id, label_id), opts)
}

// Delete a label
//
// id: project ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#delete-a-label
func (s *LabelsService) ProjectDelete(ctx context.Context, id string, label_id string) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/labels/%v", id, label_id))
}

// Promote a project label to a group label
//
// id: project ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#promote-a-project-label-to-a-group-label
func (s *LabelsService) Promote(ctx context.Context, id string, label_id string) (*Label, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/labels/%v/promote", id, label_id), nil)
}

// Subscribe to a label
//
// id: project ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#subscribe-to-a-label
func (s *LabelsService) ProjectSubscribe(ctx context.Context, id string, label_id string) (*Label, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/labels/%v/subscribe", id, label_id), nil)
}

// Unsubscribe from a label
//
// id: project ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#unsubscribe-from-a-label
func (s *LabelsService) ProjectUnsubscribe(ctx context.Context, id string, label_id string) (*Label, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/labels/%v/unsubscribe", id, label_id), nil)
}

// List group labels
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_labels.html#list-group-labels
func (s *LabelsService) GroupList(ctx context.Context, id string, opts *LabelOption) ([]*Label, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/labels", id), opts)
}

// Get a single group label
//
// id: group ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_labels.html#get-a-single-group-label
func (s *LabelsService) GroupSingle(ctx context.Context, id string, label_id string) (*Label, *Response, error) {
	return s.single(ctx, fmt.Sprintf("groups/%v/labels/%v", id, label_id))
}

// Create a new group label
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_labels.html#create-a-new-group-label
func (s *LabelsService) GroupCreate(ctx context.Context, id string, opts *CreateLabelData) (*Label, *Response, error) {
	return s.post(ctx, fmt.Sprintf("groups/%v/labels", id), opts)
}

// Update a group label
//
// id: group ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_labels.html#delete-a-group-label
func (s *LabelsService) GroupEdit(ctx context.Context, id string, label_id string, opts *UpdateLabelData) (*Label, *Response, error) {
	return s.put(ctx, fmt.Sprintf("groups/%v/labels/%v", id, label_id), opts)
}

// Delete a group label
//
// id: group ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_badges.html#remove-a-badge-from-a-group
func (s *LabelsService) GroupDelete(ctx context.Context, id string, label_id string) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("groups/%v/labels/%v", id, label_id))
}

// Subscribe to a label
//
// id: project ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#subscribe-to-a-label
func (s *LabelsService) GroupSubscribe(ctx context.Context, id string, label_id string) (*Label, *Response, error) {
	return s.post(ctx, fmt.Sprintf("groups/%v/labels/%v/subscribe", id, label_id), nil)
}

// Unsubscribe from a label
//
// id: project ID or path
// label_id: The ID or title of a group’s label
//
// GitLab API docs: https://docs.gitlab.com/ee/api/labels.html#unsubscribe-from-a-label
func (s *LabelsService) GroupUnsubscribe(ctx context.Context, id string, label_id string) (*Label, *Response, error) {
	return s.post(ctx, fmt.Sprintf("groups/%v/labels/%v/unsubscribe", id, label_id), nil)
}

func (s *LabelsService) list(ctx context.Context, url string, opts *LabelOption) ([]*Label, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var labels []*Label
	resp, err := s.client.Request(ctx, req, &labels)
	if err != nil {
		return nil, resp, err
	}

	return labels, resp, nil
}

func (s *LabelsService) single(ctx context.Context, url string) (*Label, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	label := new(Label)
	resp, err := s.client.Request(ctx, req, &label)
	if err != nil {
		return nil, resp, err
	}

	return label, resp, nil
}

func (s *LabelsService) post(ctx context.Context, url string, data interface{}) (*Label, *Response, error) {
	req, err := s.client.NewRequest("POST", url, data)
	if err != nil {
		return nil, nil, err
	}

	label := new(Label)
	resp, err := s.client.Request(ctx, req, &label)
	if err != nil {
		return nil, resp, err
	}

	return label, resp, nil
}

func (s *LabelsService) put(ctx context.Context, url string, data interface{}) (*Label, *Response, error) {
	req, err := s.client.NewRequest("PUT", url, data)
	if err != nil {
		return nil, nil, err
	}

	label := new(Label)
	resp, err := s.client.Request(ctx, req, &label)
	if err != nil {
		return nil, resp, err
	}

	return label, resp, nil
}

func (s *LabelsService) delete(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
