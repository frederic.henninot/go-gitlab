package gitlab

import (
	"context"
)

type ApplicationAppearanceService service

type ApplicationAppearance struct {
	Title                       *string `json:"title,omitempty"`
	Description                 *string `json:"description,omitempty"`
	Logo                        *string `json:"logo,omitempty"`
	HeaderLogo                  *string `json:"header_logo,omitempty"`
	Favicon                     *string `json:"favicon,omitempty"`
	NewProjectGuidelines        *string `json:"new_project_guidelines,omitempty"`
	HeaderMessage               *string `json:"header_message,omitempty"`
	FooterMessage               *string `json:"footer_message,omitempty"`
	MessageBackgroundColor      *string `json:"message_background_color,omitempty"`
	MessageFontColor            *string `json:"message_font_color,omitempty"`
	EmailHeaderAndFooterEnabled *bool   `json:"email_header_and_footer_enabled,omitempty"`
}

// Get current appearance configuration
//
// GitLab API docs: https://docs.gitlab.com/ee/api/appearance.html#get-current-appearance-configuration
func (s *ApplicationAppearanceService) Get(ctx context.Context) (*ApplicationAppearance, *Response, error) {
	req, err := s.client.NewRequest("GET", "application/appearance", nil)
	if err != nil {
		return nil, nil, err
	}

	applicationAppearance := new(ApplicationAppearance)
	resp, err := s.client.Request(ctx, req, &applicationAppearance)
	if err != nil {
		return nil, resp, err
	}

	return applicationAppearance, resp, nil
}

// Change current appearance configuration
//
// GitLab API docs: https://docs.gitlab.com/ee/api/appearance.html#change-appearance-configuration
func (s *ApplicationAppearanceService) Change(ctx context.Context, appearance *ApplicationAppearance) (*ApplicationAppearance, *Response, error) {
	req, err := s.client.NewRequest("PUT", "application/appearance", appearance)
	if err != nil {
		return nil, nil, err
	}

	applicationAppearance := new(ApplicationAppearance)
	resp, err := s.client.Request(ctx, req, &applicationAppearance)
	if err != nil {
		return nil, resp, err
	}

	return applicationAppearance, resp, nil
}
