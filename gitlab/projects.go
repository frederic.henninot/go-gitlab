package gitlab

import "time"

type ProjectsService service

type ProjectOwner struct {
	Id        *uint64    `json:"id,omitempty"`
	Name      *string    `json:"name,omitempty"`
	CreatedAt *time.Time `json:"created_at,omitempty"`
}

type ProjectNamespace struct {
	Id       *uint64 `json:"id,omitempty"`
	Name     *string `json:"name,omitempty"`
	Path     *string `json:"path,omitempty"`
	Kind     *string `json:"kind,omitempty"`
	FullPath *string `json:"full_path,omitempty"`
}
type PermissionAccessLevel struct {
	AccessLevel       *uint8 `json:"access_level,omitempty"`
	NotificationLevel *uint8 `json:"notification_level,omitempty"`
}
type ProjectPermissions struct {
	ProjectAccess *PermissionAccessLevel `json:"project_access,omitempty"`
	GroupAccess   *PermissionAccessLevel `json:"group_access,omitempty"`
}

type ProjectSharedGroup struct {
	GroupId          *uint64 `json:"group_id,omitempty"`
	GroupeName       *string `json:"group_name,omitempty"`
	GroupFullPath    *string `json:"group_full_path,omitempty"`
	GroupAccessLevel *uint8  `json:"group_access_level,omitempty"`
}

type ProjectStatistics struct {
	CommitCount      *uint32 `json:"commit_count,omitempty"`
	StorageSize      *uint32 `json:"storage_size,omitempty"`
	RepositorySize   *uint32 `json:"repository_size,omitempty"`
	WikiSize         *uint32 `json:"wiki_size,omitempty"`
	LfsObjectsSize   *uint32 `json:"lfs_objects_size,omitempty"`
	JobArtifactsSize *uint32 `json:"job_artifacts_size,omitempty"`
	PackagesSize     *uint32 `json:"packages_size,omitempty"`
}

type ProjectLinks struct {
	Self          *string `json:"self,omitempty"`
	Issues        *string `json:"issues,omitempty"`
	MergeRequests *string `json:"merge_requests,omitempty"`
	RepoBranches  *string `json:"repo_branches,omitempty"`
	Labels        *string `json:"labels,omitempty"`
	Events        *string `json:"events,omitempty"`
	Members       *string `json:"members,omitempty"`
}

type Project struct {
	Id                                        *uint64                `json:"id,omitempty"`
	DefaultBranch                             *string                `json:"default_branch,omitempty"`
	Visibility                                *string                `json:"visibility,omitempty"`
	SshUrlToRepo                              *string                `json:"ssh_url_to_repo,omitempty"`
	HttpUrlToRepo                             *string                `json:"http_url_to_repo,omitempty"`
	WebUrl                                    *string                `json:"web_url,omitempty"`
	ReadmeUrl                                 *string                `json:"readme_url,omitempty"`
	TagList                                   *[]string              `json:"tag_list,omitempty"`
	Owner                                     *ProjectOwner          `json:"owner,omitempty"`
	Name                                      *string                `json:"name,omitempty"`
	NameWithNamespace                         *string                `json:"name_with_namespace,omitempty"`
	Path                                      *string                `json:"path,omitempty"`
	PathWithNamespace                         *string                `json:"path_with_namespace,omitempty"`
	IssuesEnabled                             *bool                  `json:"issues_enabled,omitempty"`
	OpenIssuesCount                           *uint8                 `json:"open_issues_count,omitempty"`
	MergeRequestsEnabled                      *bool                  `json:"merge_requests_enabled,omitempty"`
	JobsEnabled                               *bool                  `json:"jobs_enabled,omitempty"`
	WikiEnabled                               *bool                  `json:"wiki_enabled,omitempty"`
	SnippetsEnabled                           *bool                  `json:"snippets_enabled,omitempty"`
	CanCreateMergeRequestIn                   *bool                  `json:"can_create_merge_request_in,omitempty"`
	ResoleOutdatedDiffDiscussions             *bool                  `json:"resolve_outdated_diff_discussions,omitempty"`
	ContainerRegistryEnabled                  *bool                  `json:"container_registry_enabled,omitempty"`
	CreatedAt                                 *time.Time             `json:"created_at,omitempty"`
	LastActivityAt                            *time.Time             `json:"last_activity_at,omitempty"`
	Creator                                   *uint64                `json:"creator_id,omitempty"`
	Namespace                                 *ProjectNamespace      `json:"namespace,omitempty"`
	ImportStatus                              *string                `json:"import_status,omitempty"`
	Archived                                  *bool                  `json:"archived,omitempty"`
	Permissions                               *PermissionAccessLevel `json:"permissions,omitempty"`
	AvatarUrl                                 *string                `json:"avatar_url,omitempty"`
	SharedRunnersEnabled                      *bool                  `json:"shared_runners_enabled,omitempty"`
	ForksCount                                *uint16                `json:"forks_count,omitempty"`
	StarCount                                 *uint16                `json:"star_count,omitempty"`
	RunnersToken                              *string                `json:"runners_token,omitempty"`
	CiDefaultGitDepth                         *uint8                 `json:"ci_default_git_depth,omitempty"`
	PublicJobs                                *bool                  `json:"public_jobs,omitempty"`
	SharedWithGroups                          *[]ProjectSharedGroup  `json:"shared_with_groups,omitempty"`
	OnlyAllowMergeIfPipelineSucceeds          *bool                  `json:"only_allow_merge_if_pipeline_succeeds,omitempty"`
	OnlyAllowMergeIfAllDiscussionsAreResolved *bool                  `json:"only_allow_merge_if_all_discussions_are_resolved,omitempty"`
	RemoveSourceBranchAfterMerge              *bool                  `json:"remove_source_branch_after_merge,omitempty"`
	RequestAccessEnabled                      *bool                  `json:"request_access_enabled,omitempty"`
	MergeMethod                               *string                `json:"merge_method,omitempty"`
	AutocloseReferencedIssues                 *bool                  `json:"autoclose_referenced_issues,omitempty"`
	SuggestionCommitMessage                   *bool                  `json:"suggestion_commit_message,omitempty"`
	Statistics                                *ProjectStatistics     `json:"statistics,omitempty"`
	Links                                     *ProjectLinks          `json:"_links,omitempty"`
}
