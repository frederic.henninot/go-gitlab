package gitlab

import "time"

type PipelinesService service

type Pipeline struct {
	Id        *uint64    `json:"id,omitempty"`
	Ref       *string    `json:"ref,omitempty"`
	Sha       *string    `json:"sha,omitempty"`
	Status    *string    `json:"status,omitempty"`
	WebUrl    *string    `json:"web_url,omitempty"`
	CreatedAt *time.Time `json:"created_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
}
