package gitlab

import (
	"context"
	"fmt"
)

type ApplicationsService service

type Application struct {
	Id              *uint64 `json:"id,omitempty"`
	ApplicationId   *string `json:"application_id,omitempty"`
	ApplicationName *string `json:"application_name,omitempty"`
	Secret          *string `json:"secret,omitempty"`
	CallbackUrl     *string `json:"callback_url,omitempty"`
	Confidential    *bool   `json:"confidential,omitempty"`
}

type ApplicationOption struct {
	Name         *string `json:"name,omitempty"`
	RedirectUri  *string `json:"redirect_uri,omitempty"`
	Scopes       *string `json:"scopes,omitempty"`
	Confidential *bool   `json:"confidential,omitempty"`
}

// List all applications
//
// GitLab API docs: https://docs.gitlab.com/ee/api/applications.html#list-all-applications
func (s *ApplicationsService) List(ctx context.Context, opts *PaginationOptions) ([]*Application, *Response, error) {
	u, err := addUrlOptions("applications", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var applications []*Application
	resp, err := s.client.Request(ctx, req, &applications)
	if err != nil {
		return nil, resp, err
	}

	return applications, resp, nil
}

// Create an application
//
// GitLab API docs: https://docs.gitlab.com/ee/api/applications.html#create-an-application
func (s *ApplicationsService) Create(ctx context.Context, applicationData *ApplicationOption) (*Application, *Response, error) {
	req, err := s.client.NewRequest("POST", "application", applicationData)
	if err != nil {
		return nil, nil, err
	}

	application := new(Application)
	resp, err := s.client.Request(ctx, req, &application)
	if err != nil {
		return nil, resp, err
	}

	return application, resp, nil
}

// Delete an application
//
// GitLab API docs: https://docs.gitlab.com/ee/api/applications.html#delete-an-application
func (s *ApplicationsService) Delete(ctx context.Context, id string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("applications/%v", id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
