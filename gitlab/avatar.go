package gitlab

import "context"

type AvatarService service

type Avatar struct {
	AvatarUrl *string `json:"avatar_url"`
}

type AvatarOptions struct {
	Email *string `json:"email"`
	Size  *uint16 `json:"size,omitempty"`
}

// Get current appearance configuration
//
// GitLab API docs: https://docs.gitlab.com/ee/api/appearance.html#get-current-appearance-configuration
func (s *AvatarService) Get(ctx context.Context, opts *AvatarOptions) (*Avatar, *Response, error) {
	u, err := addUrlOptions("avatar", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	avatar := new(Avatar)
	resp, err := s.client.Request(ctx, req, &avatar)
	if err != nil {
		return nil, resp, err
	}

	return avatar, resp, nil
}
