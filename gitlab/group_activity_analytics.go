package gitlab

import (
	"context"
	"fmt"
)

type GroupActivityAnalyticsService service

type GroupActivityAnalytics struct {
	IssuesCount        *uint `json:"issues_count,omitempty"`
	MergeRequestsCount *uint `json:"merge_requests_count,omitempty"`
	NewMembersCount    *uint `json:"new_members_count,omitempty"`
}

type GroupActivityAnalyticsOption struct {
	GroupPath *string `json:"group_path,omitempty"`
}

// Get Target Count
//
// target: must be issues_count or merge_requests_count or new_members_count
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_activity_analytics.html
func (s *GroupActivityAnalyticsService) Get(ctx context.Context, target string, opts *GroupActivityAnalyticsOption) (*GroupActivityAnalytics, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("analytics/group_activity/%v", target), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	groupActivityAnalytics := new(GroupActivityAnalytics)
	resp, err := s.client.Request(ctx, req, &groupActivityAnalytics)
	if err != nil {
		return nil, resp, err
	}

	return groupActivityAnalytics, resp, nil
}
