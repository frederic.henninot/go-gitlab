package gitlab

import (
	"context"
	"fmt"
)

type BadgesService service

type BadgeData struct {
	LinkUrl  *string `json:"link_url,omitempty"`
	ImageUrl *string `json:"image_url,omitempty"`
}

type Badge struct {
	Id   *uint64 `json:"id,omitempty"`
	Name *string `json:"name,omitempty"`
	BadgeData
	RenderedLinkUrl  *string `json:"rendered_link_url,omitempty"`
	RenderedImageUrl *string `json:"rendered_image_url,omitempty"`
	Kind             *string `json:"kind,omitempty"`
}

// List all badges of a project
//
// id: project ID or path
// GitLab API docs: https://docs.gitlab.com/ee/api/project_badges.html#list-all-badges-of-a-project
func (s *BadgesService) ProjectList(ctx context.Context, id string, opts *PaginationOptions) ([]*Badge, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/badges", id), opts)
}

// Get a badge of a project
//
// id: project ID or path
// badge_id: Badge ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_badges.html#get-a-badge-of-a-project
func (s *BadgesService) ProjectSingle(ctx context.Context, id string, badge_id string) (*Badge, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/badges/%v", id, badge_id))
}

// Add a badge to a project
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_badges.html#add-a-badge-to-a-project
func (s *BadgesService) ProjectAdd(ctx context.Context, id string, opts *BadgeData) (*Badge, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/badges", id), true, opts)
}

// Edit a badge to a project
//
// id: project ID or path
// badge_id: Badge ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_badges.html#edit-a-badge-of-a-project
func (s *BadgesService) ProjectEdit(ctx context.Context, id string, badge_id string, opts *BadgeData) (*Badge, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/badges/%v", id, badge_id), false, opts)
}

// Remove a badge from a project
//
// id: project ID or path
// badge_id: Badge ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_badges.html#remove-a-badge-from-a-project
func (s *BadgesService) ProjectRemove(ctx context.Context, id string, badge_id string) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("projects/%v/badges/%v", id, badge_id))
}

// Preview a badge from a project
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_badges.html#preview-a-badge-from-a-project
func (s *BadgesService) ProjectPreview(ctx context.Context, id string, opts *BadgeData) (*Badge, *Response, error) {
	return s.preview(ctx, fmt.Sprintf("projects/%v/badges/render", id), opts)
}

// List all badges of a group
//
// id: group ID or path
// GitLab API docs: https://docs.gitlab.com/ee/api/group_badges.html#list-all-badges-of-a-group
func (s *BadgesService) GroupList(ctx context.Context, id string, opts *PaginationOptions) ([]*Badge, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/badges", id), opts)
}

// Get a badge of a group
//
// id: group ID or path
// badge_id: Badge ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_badges.html#get-a-badge-of-a-group
func (s *BadgesService) GroupSingle(ctx context.Context, id string, badge_id string) (*Badge, *Response, error) {
	return s.single(ctx, fmt.Sprintf("groups/%v/badges/%v", id, badge_id))
}

// Add a badge to a group
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_badges.html#add-a-badge-to-a-group
func (s *BadgesService) GroupAdd(ctx context.Context, id string, opts *BadgeData) (*Badge, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/badges", id), true, opts)
}

// Edit a badge to a group
//
// id: group ID or path
// badge_id: Badge ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_badges.html#edit-a-badge-of-a-group
func (s *BadgesService) GroupEdit(ctx context.Context, id string, badge_id string, opts *BadgeData) (*Badge, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/badges/%v", id, badge_id), false, opts)
}

// Remove a badge from a group
//
// id: group ID or path
// badge_id: Badge ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_badges.html#remove-a-badge-from-a-group
func (s *BadgesService) GroupRemove(ctx context.Context, id string, badge_id string) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("groups/%v/badges/%v", id, badge_id))
}

// Preview a badge from a group
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_badges.html#preview-a-badge-from-a-group
func (s *BadgesService) GroupPreview(ctx context.Context, id string, opts *BadgeData) (*Badge, *Response, error) {
	return s.preview(ctx, fmt.Sprintf("groups/%v/badges/render", id), opts)
}

func (s *BadgesService) list(ctx context.Context, url string, opts *PaginationOptions) ([]*Badge, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var badges []*Badge
	resp, err := s.client.Request(ctx, req, &badges)
	if err != nil {
		return nil, resp, err
	}

	return badges, resp, nil
}

func (s *BadgesService) single(ctx context.Context, url string) (*Badge, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	badge := new(Badge)
	resp, err := s.client.Request(ctx, req, &badge)
	if err != nil {
		return nil, resp, err
	}

	return badge, resp, nil
}

func (s *BadgesService) addOrEdit(ctx context.Context, url string, add bool, data *BadgeData) (*Badge, *Response, error) {
	method := "PUT"
	if true == add {
		method = "POST"
	}
	req, err := s.client.NewRequest(method, url, data)
	if err != nil {
		return nil, nil, err
	}

	badge := new(Badge)
	resp, err := s.client.Request(ctx, req, &badge)
	if err != nil {
		return nil, resp, err
	}

	return badge, resp, nil
}

func (s *BadgesService) remove(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (s *BadgesService) preview(ctx context.Context, url string, opts *BadgeData) (*Badge, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	badge := new(Badge)
	resp, err := s.client.Request(ctx, req, &badge)
	if err != nil {
		return nil, resp, err
	}

	return badge, resp, nil
}
