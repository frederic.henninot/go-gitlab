package gitlab

import (
	"context"
	"fmt"
	"time"
)

type CommitsService service

type CommitOptions struct {
	RefName     *string `json:"ref_name,omitempty"`
	Since       *string `json:"since,omitempty"`
	Until       *string `json:"until,omitempty"`
	Path        *string `json:"path,omitempty"`
	All         *string `json:"all,omitempty"`
	WithStats   *string `json:"with_stats,omitempty"`
	FirstParent *string `json:"first_parent,omitempty"`
	Order       *string `json:"order,omitempty"`
	PaginationOptions
}

type CommitStats struct {
	Additions *uint16 `json:"additions,omitempty"`
	Deletions *uint16 `json:"deletions,omitempty"`
	Total     *uint16 `json:"total,omitempty"`
}

type Commit struct {
	Id            *string      `json:"id,omitempty"`
	ShortId       *string      `json:"short_id,omitempty"`
	Title         *string      `json:"title,omitempty"`
	AuthorName    *string      `json:"author_name,omitempty"`
	AuthorEmail   *string      `json:"author_email,omitempty"`
	AuthorDate    *time.Time   `json:"authored_date,omitempty"`
	CommitterName *string      `json:"committer_name,omitempty"`
	CommiterEmail *string      `json:"committer_email,omitempty"`
	CommittedDate *time.Time   `json:"committed_date,omitempty"`
	CreatedAt     *time.Time   `json:"created_at,omitempty"`
	Message       *string      `json:"message,omitempty"`
	LastPipeline  *Pipeline    `json:"last_pipeline,omitempty"`
	ParentIds     *[]string    `json:"parent_ids,omitempty"`
	Stats         *CommitStats `json:"stats,omitempty"`
	Status        *string      `json:"status,omitempty"`
	WebUrl        *string      `json:"web_url,omitempty"`
	Error
}

type CommitStatus struct {
	Id           *uint64    `json:"id,omitempty"`
	Ref          *string    `json:"ref,omitempty"`
	Sha          *string    `json:"sha,omitempty"`
	Status       *string    `json:"status,omitempty"`
	Name         *string    `json:"name,omitempty"`
	AllowFailure *bool      `json:"allow_failure,omitempty"`
	CreatedAt    *time.Time `json:"created_at,omitempty"`
	StartedAt    *time.Time `json:"started_at,omitempty"`
	FinishedAt   *time.Time `json:"finished_at,omitempty"`
	Author       *Author    `json:"author,omitempty"`
	Description  *string    `json:"description,omitempty"`
	TargetUrl    *string    `json:"target_url,omitempty"`
	Coverage     *float32   `json:"coverage,omitempty"`
}

type CommitStatusData struct {
	State       *string  `json:"state,omitempty"`
	Ref         *string  `json:"ref,omitempty"`
	Name        *string  `json:"name,omitempty"`
	Context     *string  `json:"context,omitempty"`
	TargetUrl   *string  `json:"target_url,omitempty"`
	Description *string  `json:"description,omitempty"`
	Coverage    *float32 `json:"coverage,omitempty"`
	PipelineId  *uint64  `json:"pipeline_id,omitempty"`
}

type CommitStatusOptions struct {
	Ref   *string `json:"ref,omitempty"`
	Stage *string `json:"stage,omitempty"`
	Name  *string `json:"name,omitempty"`
	All   *bool   `json:"all,omitempty"`
	PaginationOptions
}

type CommitActionData struct {
	Action          *string `json:"action,omitempty"`
	FilePath        *string `json:"file_path,omitempty"`
	PreviousPath    *string `json:"previous_path,omitempty"`
	Content         *string `json:"content,omitempty"`
	Encoding        *string `json:"encoding,omitempty"`
	LastCommitId    *string `json:"last_commit_id,omitempty"`
	ExecuteFilemode *string `json:"execute_filemode,omitempty"`
}

type CommitData struct {
	Branch        *string           `json:"branch,omitempty"`
	CommitMessage *string           `json:"commit_message,omitempty"`
	StartBranch   *string           `json:"start_branch,omitempty"`
	StartSha      *string           `json:"start_sha,omitempty"`
	StartProject  *string           `json:"start_project,omitempty"`
	Actions       *CommitActionData `json:"actions,omitempty"`
	AuthorEmail   *string           `json:"author_email,omitempty"`
	AuthorName    *string           `json:"author_name,omitempty"`
	Stats         *bool             `json:"stats,omitempty"`
	Force         *bool             `json:"force,omitempty"`
}

// List repository commits
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#list-repository-commits
func (s *CommitsService) List(ctx context.Context, id string, opts *CommitOptions) ([]*Commit, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/repository/commits", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var commits []*Commit
	resp, err := s.client.Request(ctx, req, &commits)
	if err != nil {
		return nil, resp, err
	}

	return commits, resp, nil
}

// Get a single commit
//
// id: project ID
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#get-a-single-commit
func (s *CommitsService) Single(ctx context.Context, id string, sha string) (*Commit, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/repository/commits/%v", id, sha), nil)
	if err != nil {
		return nil, nil, err
	}

	commit := new(Commit)
	resp, err := s.client.Request(ctx, req, &commit)
	if err != nil {
		return nil, resp, err
	}

	return commit, resp, nil
}

type CommitRefsOption struct {
	Type *string `json:"type,omitempty"`
}

type CommitRefs struct {
	Type *string `json:"type,omitempty"`
	Name *string `json:"name,omitempty"`
}

// Get references a commit is pushed to
//
// id: project ID
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#get-references-a-commit-is-pushed-to
func (s *CommitsService) Refs(ctx context.Context, id string, sha string, opts CommitRefsOption) ([]*CommitRefs, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/repository/commits/%v/refs", id, sha), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var commitrefs []*CommitRefs
	resp, err := s.client.Request(ctx, req, &commitrefs)
	if err != nil {
		return nil, resp, err
	}

	return commitrefs, resp, nil
}

// Create a commit with multiple files and actions
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions
func (s *CommitsService) Create(ctx context.Context, id string, data *CommitData) (*Commit, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/repository/commits", id), nil)
	if err != nil {
		return nil, nil, err
	}

	commit := new(Commit)
	resp, err := s.client.Request(ctx, req, commit)
	if err != nil {
		return nil, resp, err
	}

	return commit, resp, nil
}

// Create a commit with multiple files and actions
//
// id: project ID or path
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions
func (s *CommitsService) CherryPick(ctx context.Context, id string, sha string) (*Commit, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/repository/commits/%v/cherry_pick", id, sha), nil)
	if err != nil {
		return nil, nil, err
	}

	commit := new(Commit)
	resp, err := s.client.Request(ctx, req, commit)
	if err != nil {
		return nil, resp, err
	}

	return commit, resp, nil
}

// Revert a commit
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#revert-a-commit
func (s *CommitsService) Revert(ctx context.Context, id string, sha string) (*Commit, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/repository/commits/%v/revert", id, sha), nil)
	if err != nil {
		return nil, nil, err
	}

	commit := new(Commit)
	resp, err := s.client.Request(ctx, req, commit)
	if err != nil {
		return nil, resp, err
	}

	return commit, resp, nil
}

type CommitDiff struct {
	Diff        *string `json:"diff,omitempty"`
	NewPath     *string `json:"new_path,omitempty"`
	OldPath     *string `json:"old_path,omitempty"`
	AMode       *string `json:"a_mode,omitempty"`
	BMode       *string `json:"b_mode,omitempty"`
	NewFile     *bool   `json:"new_file,omitempty"`
	RenamedFile *bool   `json:"renamed_file,omitempty"`
	DeletedFile *bool   `json:"deleted_file,omitempty"`
}

// Get the diff of a commit
//
// id: project ID
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#get-the-diff-of-a-commit
func (s *CommitsService) Diff(ctx context.Context, id string, sha string) (*CommitDiff, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/repository/commits/%v/diff", id, sha), nil)
	if err != nil {
		return nil, nil, err
	}

	commitDiff := new(CommitDiff)
	resp, err := s.client.Request(ctx, req, &commitDiff)
	if err != nil {
		return nil, resp, err
	}

	return commitDiff, resp, nil
}

type CommitCommentData struct {
	LineType *string `json:"line_type,omitempty"`
	Path     *string `json:"path,omitempty"`
	Line     *uint64 `json:"line,omitempty"`
	Note     *string `json:"note,omitempty"`
}
type CommitComment struct {
	Author *Author `json:"author,omitempty"`
	CommitCommentData
	CreatedAt *time.Time `json:"created_at,omitempty"`
}

// Post comment to commit
//
// id: project ID or path
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#post-comment-to-commit
func (s *CommitsService) Comment(ctx context.Context, id string, sha string, data *CommitCommentData) (*CommitComment, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/repository/commits/%v/comments", id, sha), nil)
	if err != nil {
		return nil, nil, err
	}

	commitComment := new(CommitComment)
	resp, err := s.client.Request(ctx, req, commitComment)
	if err != nil {
		return nil, resp, err
	}

	return commitComment, resp, nil
}

// List the statuses of a commit
//
// id: project ID or path
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#list-the-statuses-of-a-commit
func (s *CommitsService) Statuses(ctx context.Context, id string, sha string, opts *CommitStatusOptions) ([]*CommitStatus, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/repository/commits/%v/statuses", id, sha), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var commitStatuses []*CommitStatus
	resp, err := s.client.Request(ctx, req, &commitStatuses)
	if err != nil {
		return nil, resp, err
	}

	return commitStatuses, resp, nil
}

// Post the build status to a commit
//
// id: project ID or path
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#post-the-build-status-to-a-commit
func (s *CommitsService) BuildStatus(ctx context.Context, id string, sha string, data *CommitStatusData) (*CommitStatus, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/statuses/%v", id, sha), nil)
	if err != nil {
		return nil, nil, err
	}

	commitStatus := new(CommitStatus)
	resp, err := s.client.Request(ctx, req, commitStatus)
	if err != nil {
		return nil, resp, err
	}

	return commitStatus, resp, nil
}

// List Merge Requests associated with a commit
//
// id: project ID or path
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#list-merge-requests-associated-with-a-commit
func (s *CommitsService) MergeRequests(ctx context.Context, id string, sha string) ([]*MergeRequest, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/repository/commits/%v/merge_requests", id, sha), nil)
	if err != nil {
		return nil, nil, err
	}

	var mergeRequests []*MergeRequest
	resp, err := s.client.Request(ctx, req, &mergeRequests)
	if err != nil {
		return nil, resp, err
	}

	return mergeRequests, resp, nil
}

type GPGSign struct {
	GPGKeyId           *uint64 `json:"gpg_key_id,omitempty"`
	GPGKeyPrimaryKeyid *string `json:"gpg_key_primary_keyid,omitempty"`
	GPGKeyUserName     *string `json:"gpg_key_user_name,omitempty"`
	GPGKeyUserEmail    *string `json:"gpg_key_user_email,omitempty"`
	VerificationStatus *string `json:"verification_status,omitempty"`
	GPGKeySubkeyId     *uint64 `json:"gpg_key_subkey_id,omitempty"`
	Error
}

// Get the diff of a commit
//
// id: project ID
// sha: Commit SHA
//
// GitLab API docs: https://docs.gitlab.com/ee/api/commits.html#get-the-diff-of-a-commit
func (s *CommitsService) GPGSign(ctx context.Context, id string, sha string) (*GPGSign, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/repository/commits/%v/signature", id, sha), nil)
	if err != nil {
		return nil, nil, err
	}

	gpgSign := new(GPGSign)
	resp, err := s.client.Request(ctx, req, &gpgSign)
	if err != nil {
		return nil, resp, err
	}

	return gpgSign, resp, nil
}
