package gitlab

import (
	"context"
	"fmt"
	"time"
)

type AwardEmojiService service

type AwardEmojiUser struct {
	Author
}

type AwardEmoji struct {
	Id            *uint64         `json:"id,omitempty"`
	Name          *string         `json:"name,omitempty"`
	User          *AwardEmojiUser `json:"user,omitempty"`
	CreatedAt     *time.Time      `json:"created_at,omitempty"`
	UpdatedAt     *time.Time      `json:"updated_at,omitempty"`
	AwardableId   *uint64         `json:"awardable_id,omitempty"`
	AwardableType *string         `json:"awardable_type,omitempty"`
}

// List an awardable’s award emoji
//
// id: project ID or path
// target: must be issues | merge_requests | snippets
// target_id: issues | merge_requests | snippets ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/award_emoji.html#list-an-awardables-award-emoji
func (s *AwardEmojiService) List(ctx context.Context, id string, target string, target_id string, opts *PaginationOptions) ([]*AwardEmoji, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/%v/%v/award_emoji", id, target, target_id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var awardEmoji []*AwardEmoji
	resp, err := s.client.Request(ctx, req, &awardEmoji)
	if err != nil {
		return nil, resp, err
	}

	return awardEmoji, resp, nil
}

// Get Single award emoji
//
// id: project ID or path
// target: must be issues | merge_requests | snippets
// target_id: issues | merge_requests | snippets ID
// award_id: Award ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/award_emoji.html#get-single-award-emoji
func (s *AwardEmojiService) Single(ctx context.Context, id string, target string, target_id string, award_id string) (*AwardEmoji, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/%v/%v/award_emoji/%v", id, target, target_id, award_id), nil)
	if err != nil {
		return nil, nil, err
	}

	awardEmoji := new(AwardEmoji)
	resp, err := s.client.Request(ctx, req, &awardEmoji)
	if err != nil {
		return nil, resp, err
	}

	return awardEmoji, resp, nil
}

type NewAwardOption struct {
	NamespacesService *string `json:"name,omitempty"`
}

// Award a new emoji
//
// id: project ID or path
// target: must be issues | merge_requests | snippets
// target_id: issues | merge_requests | snippets ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/award_emoji.html#award-a-new-emoji
func (s *AwardEmojiService) Create(ctx context.Context, id string, target string, target_id string, opts NewAwardOption) (*AwardEmoji, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/%v/%v/award_emoji/%v/award_emoji", id, target, target_id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("POST", u, nil)
	if err != nil {
		return nil, nil, err
	}

	awardEmoji := new(AwardEmoji)
	resp, err := s.client.Request(ctx, req, &awardEmoji)
	if err != nil {
		return nil, resp, err
	}

	return awardEmoji, resp, nil
}

// Delete an award emoji
//
// id: project ID or path
// target: must be issues | merge_requests | snippets
// target_id: issues | merge_requests | snippets ID
// award_id: Award ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/award_emoji.html#delete-an-award-emoji
func (s *AwardEmojiService) Delete(ctx context.Context, id string, target string, target_id string, award_id string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/%v/%v/award_emoji/%v", id, target, target_id, award_id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// List a comment's award emoji
//
// id: project ID or path
// issue_id: Id of project issue
// note_id:  Id of issue note
//
// GitLab API docs: https://docs.gitlab.com/ee/api/award_emoji.html#list-a-comments-award-emoji
func (s *AwardEmojiService) CommentList(ctx context.Context, id string, issue_id string, note_id string, opts *PaginationOptions) ([]*AwardEmoji, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/issues/%v/notes/%v/award_emoji", id, issue_id, note_id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var awardEmoji []*AwardEmoji
	resp, err := s.client.Request(ctx, req, &awardEmoji)
	if err != nil {
		return nil, resp, err
	}

	return awardEmoji, resp, nil
}

// Get an award emoji for a comment
//
// id: project ID or path
// issue_id: Id of project issue
// note_id:  Id of issue note
// award_id: Award ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/award_emoji.html#get-an-award-emoji-for-a-comment
func (s *AwardEmojiService) CommentSingle(ctx context.Context, id string, issue_id string, note_id string, award_id string) (*AwardEmoji, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/issues/%v/notes/%v/award_emoji/%v", id, issue_id, note_id, award_id), nil)
	if err != nil {
		return nil, nil, err
	}

	awardEmoji := new(AwardEmoji)
	resp, err := s.client.Request(ctx, req, &awardEmoji)
	if err != nil {
		return nil, resp, err
	}

	return awardEmoji, resp, nil
}

// Award a new emoji on a comment
//
// id: project ID or path
// issue_id: Id of project issue
// note_id:  Id of issue note
//
// GitLab API docs: https://docs.gitlab.com/ee/api/award_emoji.html#award-a-new-emoji-on-a-comment
func (s *AwardEmojiService) CommentCreate(ctx context.Context, id string, issue_id string, note_id string, opts NewAwardOption) (*AwardEmoji, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/issues/%v/notes/%v/award_emoji", id, issue_id, note_id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("POST", u, nil)
	if err != nil {
		return nil, nil, err
	}

	awardEmoji := new(AwardEmoji)
	resp, err := s.client.Request(ctx, req, &awardEmoji)
	if err != nil {
		return nil, resp, err
	}

	return awardEmoji, resp, nil
}

// Delete an award emoji from a comment
//
// id: project ID or path
// issue_id: Id of project issue
// note_id:  Id of issue note
// award_id: Award ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/award_emoji.html#delete-an-award-emoji-from-a-comment
func (s *AwardEmojiService) Delete(ctx context.Context, id string, issue_id string, note_id string, award_id string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/issues/%v/notes/%v/award_emoji/%v", id, issue_id, note_id, award_id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
