package gitlab

import "context"

type MarkdownService service

type MarkdownData struct {
	Text    *string `json:"text,omitempty"`
	Gfm     *bool   `json:"gfm,omitempty"`
	Project *string `json:"project,omitempty"`
}

type Markdown struct {
	Html *string `json:"html,omitempty"`
}

// Render an arbitrary Markdown document
//
// GitLab API docs: https://docs.gitlab.com/ee/api/markdown.html#render-an-arbitrary-markdown-document
func (s *MarkdownService) Create(ctx context.Context, data *MarkdownData) (*Markdown, *Response, error) {
	req, err := s.client.NewRequest("POST", "markdown", data)
	if err != nil {
		return nil, nil, err
	}

	markdown := new(Markdown)
	resp, err := s.client.Request(ctx, req, &markdown)
	if err != nil {
		return nil, resp, err
	}

	return markdown, resp, nil
}
