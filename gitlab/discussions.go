package gitlab

import (
	"context"
	"fmt"
	"time"
)

type DiscussionsService service

type Discussion struct {
	Id             *string `json:"id,omitempty"`
	IndividualNote *bool   `json:"individual_note,omitempty"`
	Notes          *[]Note `json:"notes,omitempty"`
}

type ThreadPosition struct {
	BaseSha      *string `json:"base_sha,omitempty"`
	StartSha     *string `json:"start_sha,omitempty"`
	HeadSha      *string `json:"head_sha,omitempty"`
	PositionType *string `json:"position_type,omitempty"`
	NewPath      *string `json:"new_path,omitempty"`
	NewLine      *uint32 `json:"new_line,omitempty"`
	OldPath      *string `json:"old_path,omitempty"`
	OldLine      *uint32 `json:"old_line,omitempty"`
	Width        *uint16 `json:"width,omitempty"`
	Height       *uint16 `json:"height,omitempty"`
	X            *uint16 `json:"x,omitempty"`
	Y            *uint16 `json:"y,omitempty"`
}
type ThreadData struct {
	Body      *string         `json:"body,omitempty"`
	CreatedAt *time.Time      `json:"created_at,omitempty"`
	Position  *ThreadPosition `json:"position,omitempty"`
}

type DiscussionData struct {
	NoteId *string `json:"note_id,omitempty"`
	ThreadData
}

// List project issue discussion items
//
// id: project ID or path
// issue_iid: IID of the issue
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#list-project-issue-discussion-items
func (s *DiscussionsService) IssueList(ctx context.Context, id string, issue_iid uint64, opts *PaginationOptions) ([]*Discussion, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/issues/%v/discussions", id, issue_iid), opts)
}

// Get single issue discussion item
//
// id: project ID or path
// issue_iid: IID of the issue
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#get-single-issue-discussion-item
func (s *DiscussionsService) IssueSingle(ctx context.Context, id string, issue_iid uint64, discussion_id uint64) (*Discussion, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/issues/%v/discussions/%v", id, issue_iid, discussion_id))
}

// Create new issue thread
//
// id: project ID or path
// issue_iid: IID of the issue
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#create-new-issue-thread
func (s *DiscussionsService) IssueThread(ctx context.Context, id string, issue_iid uint64, opts *ThreadData) (*Discussion, *Response, error) {
	return s.addThread(ctx, fmt.Sprintf("projects/%v/issues/%v/discussions", id, issue_iid), opts)
}

// Add note to existing issue thread
//
// id: project ID or path
// issue_iid: IID of the issue
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_badges.html#add-a-badge-to-a-project
func (s *DiscussionsService) IssueAdd(ctx context.Context, id string, issue_iid uint64, discussion_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/issues/%v/discussions/%v/notes", id, issue_iid, discussion_id), true, opts)
}

// Modify existing issue thread note
//
// id: project ID or path
// issue_iid: IID of the issue
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#modify-existing-issue-thread-note
func (s *DiscussionsService) IssueEdit(ctx context.Context, id string, issue_iid uint64, discussion_id uint64, note_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/issues/%v/discussions/%v/notes/%v", id, issue_iid, discussion_id, note_id), false, opts)
}

// Delete an issue thread note
//
// id: project ID or path
// issue_iid: IID of the issue
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#delete-an-issue-thread-note
func (s *DiscussionsService) IssueDelete(ctx context.Context, id string, issue_iid uint64, discussion_id uint64, note_id uint64) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/issues/%v/discussions/%v/notes/%v", id, issue_iid, discussion_id, note_id))
}

// List project snippet discussion items
//
// id: project ID or path
// snippet_id: IID of the snippet
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#list-project-snippet-discussion-items
func (s *DiscussionsService) SnippetList(ctx context.Context, id string, snippet_id uint64, opts *PaginationOptions) ([]*Discussion, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/snippets/%v/discussions", id, snippet_id), opts)
}

// Get single snippet discussion item
//
// id: project ID or path
// snippet_id: IID of the snippet
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#get-single-snippet-discussion-item
func (s *DiscussionsService) SnippetSingle(ctx context.Context, id string, snippet_id uint64, discussion_id uint64) (*Discussion, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/snippets/%v/discussions/%v", id, snippet_id, discussion_id))
}

// Create new snippet thread
//
// id: project ID or path
// snippet_id: IID of the snippet
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#create-new-snippet-thread
func (s *DiscussionsService) SnippetThread(ctx context.Context, id string, snippet_id uint64, opts *ThreadData) (*Discussion, *Response, error) {
	return s.addThread(ctx, fmt.Sprintf("projects/%v/snippets/%v/discussions", id, snippet_id), opts)
}

// Add note to existing snippet thread
//
// id: project ID or path
// snippet_id: IID of the snippet
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#add-note-to-existing-snippet-thread
func (s *DiscussionsService) SnippetAdd(ctx context.Context, id string, snippet_id uint64, discussion_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/snippets/%v/discussions/%v/notes", id, snippet_id, discussion_id), true, opts)
}

// Modify existing snippet thread note
//
// id: project ID or path
// snippet_id: IID of the snippet
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#modify-existing-snippet-thread-note
func (s *DiscussionsService) SnippetEdit(ctx context.Context, id string, snippet_id uint64, discussion_id uint64, note_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/snippets/%v/discussions/%v/notes/%v", id, snippet_id, discussion_id, note_id), false, opts)
}

// Delete a snippet thread note
//
// id: project ID or path
// snippet_id: IID of the snippet
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#delete-a-snippet-thread-note
func (s *DiscussionsService) SnippetDelete(ctx context.Context, id string, snippet_id uint64, discussion_id uint64, note_id uint64) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/snippets/%v/discussions/%v/notes/%v", id, snippet_id, discussion_id, note_id))
}

// List group epic discussion items
//
// id: group ID or path
// epic_id: ID of an epic
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#list-group-epic-discussion-items
func (s *DiscussionsService) EpicList(ctx context.Context, id string, epic_id uint64, opts *PaginationOptions) ([]*Discussion, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/epics/%v/discussions", id, epic_id), opts)
}

// Get single epic discussion item
//
// id: project ID or path
// epic_id: ID of an epic
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#get-single-epic-discussion-item
func (s *DiscussionsService) EpicSingle(ctx context.Context, id string, epic_id uint64, discussion_id uint64) (*Discussion, *Response, error) {
	return s.single(ctx, fmt.Sprintf("groups/%v/epics/%v/discussions/%v", id, epic_id, discussion_id))
}

// Create new epic thread
//
// id: project ID or path
// epic_id: ID of an epic
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#create-new-epic-thread
func (s *DiscussionsService) EpicThread(ctx context.Context, id string, epic_id uint64, opts *ThreadData) (*Discussion, *Response, error) {
	return s.addThread(ctx, fmt.Sprintf("groups/%v/epics/%v/discussions", id, epic_id), opts)
}

// Add note to existing epic thread
//
// id: project ID or path
// epic_id: ID of an epic
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#add-note-to-existing-epic-thread
func (s *DiscussionsService) EpicAdd(ctx context.Context, id string, epic_id uint64, discussion_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/epics/%v/discussions/%v/notes", id, epic_id, discussion_id), true, opts)
}

// Modify existing epic thread note
//
// id: project ID or path
// epic_id: ID of an epic
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#modify-existing-epic-thread-note
func (s *DiscussionsService) EpicEdit(ctx context.Context, id string, epic_id uint64, discussion_id uint64, note_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/epics/%v/discussions/%v/notes/%v", id, epic_id, discussion_id, note_id), false, opts)
}

// Delete an epic thread note
//
// id: project ID or path
// epic_id: ID of an epic
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#delete-an-epic-thread-note
func (s *DiscussionsService) EpicDelete(ctx context.Context, id string, epic_id uint64, discussion_id uint64, note_id uint64) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("groups/%v/epics/%v/discussions/%v/notes/%v", id, epic_id, discussion_id, note_id))
}

// List project merge request discussion items
//
// id: project ID or path
// merge_request_iid: IID of a merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#list-project-merge-request-discussion-items
func (s *DiscussionsService) MergeRequestList(ctx context.Context, id string, merge_request_iid uint64, opts *PaginationOptions) ([]*Discussion, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/discussions", id, merge_request_iid), opts)
}

// Get single merge request discussion item
//
// id: project ID or path
// merge_request_iid: IID of a merge request
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#get-single-merge-request-discussion-item
func (s *DiscussionsService) MergeRequestSingle(ctx context.Context, id string, merge_request_iid uint64, discussion_id uint64) (*Discussion, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/discussions/%v", id, merge_request_iid, discussion_id))
}

// Create new merge request thread
//
// id: project ID or path
// merge_request_iid: IID of a merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#create-new-merge-request-thread
func (s *DiscussionsService) MergeRequestThread(ctx context.Context, id string, merge_request_iid uint64, opts *ThreadData) (*Discussion, *Response, error) {
	return s.addThread(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/discussions", id, merge_request_iid), opts)
}

type DiscussionResolveData struct {
	Resolved *bool `json:"resolved,omitempty"`
}

// Resolve a merge request thread
//
// id: project ID or path
// merge_request_iid: IID of a merge request
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#resolve-a-merge-request-thread
func (s *DiscussionsService) MergeRequestResolve(ctx context.Context, id string, merge_request_iid uint64, discussion_id uint64, data *DiscussionResolveData) (*Discussion, *Response, error) {
	url := fmt.Sprintf("projects/%v/merge_requests/%v/discussions/%v", id, merge_request_iid, discussion_id)
	req, err := s.client.NewRequest("PUT", url, data)
	if err != nil {
		return nil, nil, err
	}

	discussion := new(Discussion)
	resp, err := s.client.Request(ctx, req, &discussion)
	if err != nil {
		return nil, resp, err
	}

	return discussion, resp, nil
}

// Add note to existing merge request thread
//
// id: project ID or path
// merge_request_iid: IID of a merge request
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#add-note-to-existing-merge-request-thread
func (s *DiscussionsService) MergeRequestAdd(ctx context.Context, id string, merge_request_iid uint64, discussion_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/discussions/%v/notes", id, merge_request_iid, discussion_id), true, opts)
}

// Modify an existing merge request thread note
//
// id: project ID or path
// merge_request_iid: IID of a merge request
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#modify-an-existing-merge-request-thread-note
func (s *DiscussionsService) MergeRequestEdit(ctx context.Context, id string, merge_request_iid uint64, discussion_id uint64, note_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/discussions/%v/notes/%v", id, merge_request_iid, discussion_id, note_id), false, opts)
}

// Delete a merge request thread note
//
// id: project ID or path
// merge_request_iid: IID of a merge request
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#delete-a-merge-request-thread-note
func (s *DiscussionsService) MergeRequestDelete(ctx context.Context, id string, merge_request_iid uint64, discussion_id uint64, note_id uint64) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/discussions/%v/notes/%v", id, merge_request_iid, discussion_id, note_id))
}

// List project commit discussion items
//
// id: project ID or path
// commit_id: ID of a commit
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#list-project-commit-discussion-items
func (s *DiscussionsService) CommitList(ctx context.Context, id string, merge_request_iid uint64, opts *PaginationOptions) ([]*Discussion, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/commits/%v/discussions", id, merge_request_iid), opts)
}

// Get single merge request discussion item
//
// id: project ID or path
// commit_id: ID of a commit
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#get-single-commit-discussion-item
func (s *DiscussionsService) CommitSingle(ctx context.Context, id string, commit_id uint64, discussion_id uint64) (*Discussion, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/commits/%v/discussions/%v", id, commit_id, discussion_id))
}

// Create new commit thread
//
// id: project ID or path
// commit_id: ID of a commit
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#create-new-commit-thread
func (s *DiscussionsService) CommitThread(ctx context.Context, id string, commit_id uint64, opts *ThreadData) (*Discussion, *Response, error) {
	return s.addThread(ctx, fmt.Sprintf("projects/%v/commits/%v/discussions", id, commit_id), opts)
}

// Add note to existing commit thread
//
// id: project ID or path
// commit_id: ID of a commit
// discussion_id: ID of the discution
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#add-note-to-existing-commit-thread
func (s *DiscussionsService) CommitAdd(ctx context.Context, id string, commit_id uint64, discussion_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/commits/%v/discussions/%v/notes", id, commit_id, discussion_id), true, opts)
}

// Modify an existing commit thread note
//
// id: project ID or path
// commit_id: ID of a commit
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#modify-an-existing-commit-thread-note
func (s *DiscussionsService) CommitEdit(ctx context.Context, id string, commit_id uint64, discussion_id uint64, note_id uint64, opts *DiscussionData) (*Discussion, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/commits/%v/discussions/%v/notes/%v", id, commit_id, discussion_id, note_id), false, opts)
}

// Delete a commit thread note
//
// id: project ID or path
// commit_id: ID of a commit
// discussion_id: ID of the discution
// note_id: ID of the note/reply
//
// GitLab API docs: https://docs.gitlab.com/ee/api/discussions.html#delete-a-commit-thread-note
func (s *DiscussionsService) CommitDelete(ctx context.Context, id string, commit_id uint64, discussion_id uint64, note_id uint64) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/commits/%v/discussions/%v/notes/%v", id, commit_id, discussion_id, note_id))
}

func (s *DiscussionsService) list(ctx context.Context, url string, opts *PaginationOptions) ([]*Discussion, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var discussions []*Discussion
	resp, err := s.client.Request(ctx, req, &discussions)
	if err != nil {
		return nil, resp, err
	}

	return discussions, resp, nil
}

func (s *DiscussionsService) single(ctx context.Context, url string) (*Discussion, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	discussion := new(Discussion)
	resp, err := s.client.Request(ctx, req, &discussion)
	if err != nil {
		return nil, resp, err
	}

	return discussion, resp, nil
}

func (s *DiscussionsService) addThread(ctx context.Context, url string, data *ThreadData) (*Discussion, *Response, error) {
	req, err := s.client.NewRequest("POST", url, data)
	if err != nil {
		return nil, nil, err
	}

	discussion := new(Discussion)
	resp, err := s.client.Request(ctx, req, &discussion)
	if err != nil {
		return nil, resp, err
	}

	return discussion, resp, nil
}

func (s *DiscussionsService) addOrEdit(ctx context.Context, url string, add bool, data *DiscussionData) (*Discussion, *Response, error) {
	method := "PUT"
	if true == add {
		method = "POST"
	}
	req, err := s.client.NewRequest(method, url, data)
	if err != nil {
		return nil, nil, err
	}

	discussion := new(Discussion)
	resp, err := s.client.Request(ctx, req, &discussion)
	if err != nil {
		return nil, resp, err
	}

	return discussion, resp, nil
}

func (s *DiscussionsService) delete(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
