package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"time"
)

type GeoNodesService service

type GeoNode struct {
	Id                               *uint64          `json:"id,omitempty"`
	Name                             *string          `json:"name,omitempty"`
	Url                              *string          `json:"url,omitempty"`
	InternalUrl                      *string          `json:"internal_url,omitempty"`
	Primary                          *bool            `json:"primary,omitempty"`
	Enabled                          *bool            `json:"enabled,omitempty"`
	Current                          *bool            `json:"current,omitempty"`
	FilesMaxCapacity                 *uint            `json:"files_max_capacity,omitempty"`
	ReposMaxCapacity                 *uint            `json:"repos_max_capacity,omitempty"`
	ContainerRepositoriesMaxCapacity *uint            `json:"container_repositories_max_capacity,omitempty"`
	VerificationMaxCapacity          *uint            `json:"verification_max_capacity,omitempty"`
	SelectiveSyncType                *string          `json:"selective_sync_type,omitempty"`
	SelectiveSyncShards              *json.RawMessage `json:"selective_sync_shards,omitempty"`
	SelectiveSyncNamespaceIds        *[]uint          `json:"selective_sync_namespace_ids,omitempty"`
	SelectiveReverificationInterval  *uint            `json:"selective_sync_namespace_ids,omitempty"`
	SyncObjectStorage                *bool            `json:"sync_object_storage,omitempty"`
	WebEditUrl                       *string          `json:"web_edit_url,omitempty"`
	WebGeoProjectsUrl                *string          `json:"web_geo_projects_url,omitempty"`
}

type GeoNodeStatus struct {
	GeoNodeId                               *uint64 `json:"geo_node_id,omitempty"`
	Healthy                                 *bool   `json:"healthy,omitempty"`
	Health                                  *string `json:"health,omitempty"`
	HealthStatus                            *string `json:"health_status,omitempty"`
	MissingOauthApplication                 *bool   `json:"missing_oauth_application,omitempty"`
	AttachmentsCount                        *uint   `json:"attachments_count,omitempty"`
	AttachmentsSyncedCount                  *uint   `json:"attachments_synced_count,omitempty"`
	AttachmentsFailedCount                  *uint   `json:"attachments_failed_count,omitempty"`
	AttachmentsSyncedMissingOnPrimaryCount  *uint   `json:"attachments_synced_missing_on_primary_count,omitempty"`
	AttachmentsSyncedInPercentage           *string `json:"attachments_synced_in_percentage,omitempty"`
	DbReplicationLagSeconds                 *uint   `json:"db_replication_lag_seconds,omitempty"`
	LfsObjectsCount                         *uint   `json:"lfs_objects_count,omitempty"`
	LfsObjectsSyncedCount                   *uint   `json:"lfs_objects_synced_count,omitempty"`
	LfsObjectsFailedCount                   *uint   `json:"lfs_objects_failed_count,omitempty"`
	LfsObjectsSyncedMissingOnPrimaryCount   *uint   `json:"lfs_objects_synced_missing_on_primary_count,omitempty"`
	LfsObjectsSyncedInPercentage            *string `json:"lfs_objects_synced_in_percentage,omitempty"`
	JobArtifactsCount                       *uint   `json:"job_artifacts_count,omitempty"`
	JobArtifactsSyncedCount                 *uint   `json:"job_artifacts_synced_count,omitempty"`
	JobArtifactsFailedCount                 *uint   `json:"job_artifacts_failed_count,omitempty"`
	JobArtifactsSyncedMissingOnPrimaryCount *uint   `json:"job_artifacts_synced_missing_on_primary_count,omitempty"`
	JobArtifactsSyncedInPercentage          *string `json:"job_artifacts_synced_in_percentage,omitempty"`
	ContainerRepositoriesCount              *uint   `json:"container_repositories_count,omitempty"`
	ContainerRepositoriesSyncedCount        *uint   `json:"container_repositories_synced_count,omitempty"`
	ContainerRepositoriesFailedCount        *uint   `json:"container_repositories_failed_count,omitempty"`
	ContainerRepositoriesSyncedInPercentage *string `json:"container_repositories_synced_in_percentage,omitempty"`
	DesignRepositoriesCount                 *uint   `json:"design_repositories_count,omitempty"`
	DesignRepositoriesSyncedCount           *uint   `json:"design_repositories_synced_count,omitempty"`
	DesignRepositoriesFailedCount           *uint   `json:"design_repositories_failed_count,omitempty"`
	DesignRepositoriesSyncedInPercentage    *string `json:"design_repositories_synced_in_percentage,omitempty"`
	ProjectsCount                           *uint   `json:"projects_count,omitempty"`
	RepositoriesFailedCount                 *uint   `json:"repositories_failed_count,omitempty"`
	RepositoriesSyncedCount                 *uint   `json:"repositories_synced_count,omitempty"`
	RepositoriesSyncedInPercentage          *string `json:"repositories_synced_in_percentage,omitempty"`
	WikisFailedCount                        *uint   `json:"wikis_failed_count,omitempty"`
	WikisSyncedCount                        *uint   `json:"wikis_synced_count,omitempty"`
	WikisSyncedInPercentage                 *string `json:"wikis_synced_in_percentage,omitempty"`
	ReplicationSlotsCount                   *uint   `json:"replication_slots_count,omitempty"`
	ReplicationSlotsUsedCount               *uint   `json:"replication_slots_used_count,omitempty"`
	ReplicationSlotsUsedInPercentage        *string `json:"replication_slots_used_in_percentage,omitempty"`
	ReplicationSlotsMaxRetainedWalBytes     *uint   `json:"replication_slots_max_retained_wal_bytes,omitempty"`
	RepositoriesCheckedCount                *uint   `json:"repositories_checked_count,omitempty"`
	RepositoriesCheckedFailedCount          *uint   `json:"repositories_checked_failed_count,omitempty"`
	RepositoriesCheckedInPercentage         *string `json:"repositories_checked_in_percentage,omitempty"`
	RepositoriesChecksummedCount            *uint   `json:"repositories_checksummed_count,omitempty"`
	RepositoriesChecksumFailedCount         *uint   `json:"repositories_checksum_failed_count,omitempty"`
	RepositoriesChecksummedInPercentage     *string `json:"repositories_checksummed_in_percentage,omitempty"`
	WikisChecksummedCount                   *uint   `json:"wikis_checksummed_count,omitempty"`
	WikisChecksumFailedCount                *uint   `json:"wikis_checksum_failed_count,omitempty"`
	WikisChecksummedInPercentage            *string `json:"wikis_checksummed_in_percentage,omitempty"`
	RepositoriesVerifiedCount               *uint   `json:"repositories_verified_count,omitempty"`
	RepositoriesVerificationFailedCount     *uint   `json:"repositories_verification_failed_count,omitempty"`
	RepositoriesVerifiedInPercentage        *string `json:"repositories_verified_in_percentage,omitempty"`
	RepositoriesChecksumMismatchCount       *uint   `json:"repositories_checksum_mismatch_count,omitempty"`
	WikisVerifiedCount                      *uint   `json:"wikis_verified_count,omitempty"`
	WikisVerificationFailedCount            *uint   `json:"wikis_verification_failed_count,omitempty"`
	WikisVerifiedInPercentage               *string `json:"wikis_verified_in_percentage,omitempty"`
	WikisChecksumMismatchCount              *uint   `json:"wikis_checksum_mismatch_count,omitempty"`
	RepositoriesRetryingVerificationCount   *uint   `json:"repositories_retrying_verification_count,omitempty"`
	WikisRetryingVerificationCount          *uint   `json:"wikis_retrying_verification_count,omitempty"`
	LastEventId                             *uint   `json:"last_event_id,omitempty"`
	LastEventTimestamp                      *uint   `json:"last_event_timestamp,omitempty"`
	CursorLastEventId                       *uint   `json:"cursor_last_event_id,omitempty"`
	CursorLastEventTimestamp                *uint   `json:"cursor_last_event_timestamp,omitempty"`
	LastSuccessfulStatusCheckTimestamp      *uint   `json:"last_successful_status_check_timestamp,omitempty"`
	Version                                 *string `json:"version,omitempty"`
	Revision                                *string `json:"revision,omitempty"`
}

type GeoNodeFailure struct {
	ProjectId                         *uint64    `json:"project_id,omitempty"`
	LastRepositorySuccessfulSyncAt    *time.Time `json:"last_repository_successful_sync_at,omitempty"`
	LastWikiSyncedAt                  *time.Time `json:"last_wiki_synced_at,omitempty"`
	LastWikiSuccessfulSyncAt          *time.Time `json:"last_wiki_successful_sync_at,omitempty"`
	RepositoryRetryCount              *uint      `json:"repository_retry_count,omitempty"`
	WikiRetryCount                    *uint      `json:"wiki_retry_count,omitempty"`
	LastRepositorySyncFailure         *string    `json:"last_repository_sync_failure,omitempty"`
	LastWikiSyncFailure               *string    `json:"last_wiki_sync_failure,omitempty"`
	LastRepositoryVerificationFailure *string    `json:"last_repository_verification_failure,omitempty"`
	LastWikiVerificationFailure       *string    `json:"last_wiki_verification_failure,omitempty"`
	RepositoryVerificationChecksumSha *string    `json:"repository_verification_checksum_sha,omitempty"`
	WikiVerificationChecksumSha       *string    `json:"wiki_verification_checksum_sha,omitempty"`
	RepositoryChecksumMismatch        *bool      `json:"repository_checksum_mismatch,omitempty"`
	WikiChecksumMismatch              *bool      `json:"wiki_checksum_mismatch,omitempty"`
}

type GeoNodeFailureOption struct {
	Type        *string `json:"type,omitempty"`
	FailureType *string `json:"failure_type,omitempty"`
}

// Retrieve configuration about all Geo nodes
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#retrieve-configuration-about-all-geo-nodes
func (s *GeoNodesService) List(ctx context.Context, opts *PaginationOptions) ([]*GeoNode, *Response, error) {
	u, err := addUrlOptions("geo_nodes", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var geoNodes []*GeoNode
	resp, err := s.client.Request(ctx, req, &geoNodes)
	if err != nil {
		return nil, resp, err
	}

	return geoNodes, resp, nil
}

// Retrieve configuration about a specific Geo node
//
// id: geo node ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#retrieve-configuration-about-a-specific-geo-node
func (s *GeoNodesService) Single(ctx context.Context, id string) (*GeoNode, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("geo_nodes/%v", id), nil)
	if err != nil {
		return nil, nil, err
	}

	geoNode := new(GeoNode)
	resp, err := s.client.Request(ctx, req, &geoNode)
	if err != nil {
		return nil, resp, err
	}

	return geoNode, resp, nil
}

// Create a new Geo node
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#create-a-new-geo-node
func (s *GeoNodesService) Create(ctx context.Context, data *GeoNode) (*GeoNode, *Response, error) {
	req, err := s.client.NewRequest("POST", "geo_nodes", data)
	if err != nil {
		return nil, nil, err
	}

	geoNode := new(GeoNode)
	resp, err := s.client.Request(ctx, req, &geoNode)
	if err != nil {
		return nil, resp, err
	}

	return geoNode, resp, nil
}

// Edit a Geo node
//
// id: geo node ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#edit-a-geo-node
func (s *GeoNodesService) Update(ctx context.Context, id string, data *GeoNode) (*GeoNode, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("geo_nodes/%v", id), data)
	if err != nil {
		return nil, nil, err
	}

	geoNode := new(GeoNode)
	resp, err := s.client.Request(ctx, req, &geoNode)
	if err != nil {
		return nil, resp, err
	}

	return geoNode, resp, nil
}

// Delete a Geo node
//
// id: geo node ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#delete-a-geo-node
func (s *GeoNodesService) Delete(ctx context.Context, id string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("geo_nodes/%v", id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// Repair a Geo node
//
// id: geo node ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#repair-a-geo-node
func (s *GeoNodesService) Repair(ctx context.Context, id string) (*GeoNode, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("geo_nodes/%v/repair", id), nil)
	if err != nil {
		return nil, nil, err
	}

	geoNode := new(GeoNode)
	resp, err := s.client.Request(ctx, req, &geoNode)
	if err != nil {
		return nil, resp, err
	}

	return geoNode, resp, nil
}

// Retrieve status about all Geo nodes
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#retrieve-status-about-all-geo-nodes
func (s *GeoNodesService) Statuses(ctx context.Context, opts *PaginationOptions) ([]*GeoNodeStatus, *Response, error) {
	u, err := addUrlOptions("geo_nodes/status", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var geoNodeStatuses []*GeoNodeStatus
	resp, err := s.client.Request(ctx, req, &geoNodeStatuses)
	if err != nil {
		return nil, resp, err
	}

	return geoNodeStatuses, resp, nil
}

// Retrieve status about a specific Geo node
//
// id: geo node ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#retrieve-status-about-a-specific-geo-node
func (s *GeoNodesService) Status(ctx context.Context, id string) (*GeoNodeStatus, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("geo_nodes/%v/status", id), nil)
	if err != nil {
		return nil, nil, err
	}

	geoNodeStatus := new(GeoNodeStatus)
	resp, err := s.client.Request(ctx, req, &geoNodeStatus)
	if err != nil {
		return nil, resp, err
	}

	return geoNodeStatus, resp, nil
}

// Retrieve project sync or verification failures that occurred on the current node
//
// GitLab API docs: https://docs.gitlab.com/ee/api/geo_nodes.html#retrieve-project-sync-or-verification-failures-that-occurred-on-the-current-node
func (s *GeoNodesService) Failures(ctx context.Context, opts *GeoNodeFailureOption) ([]*GeoNodeFailure, *Response, error) {
	u, err := addUrlOptions("geo_nodes/current/failures", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var geoNodeFailures []*GeoNodeFailure
	resp, err := s.client.Request(ctx, req, &geoNodeFailures)
	if err != nil {
		return nil, resp, err
	}

	return geoNodeFailures, resp, nil
}
