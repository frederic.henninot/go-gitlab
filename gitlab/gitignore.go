package gitlab

import (
	"context"
	"fmt"
)

type GitignoreService service

type Gitignore struct {
	Name    *string `json:"name,omitempty"`
	Content *string `json:"content,omitempty"`
}

// List all available Gitignore templates.
//
// Giltab API docs: https://docs.gitlab.com/ee/api/templates/gitignores.html
func (s GitignoreService) List(ctx context.Context) ([]string, *Response, error) {
	req, err := s.client.NewRequest("GET", "templates/gitignores", nil)
	if err != nil {
		return nil, nil, err
	}

	var availableTemplates []string
	resp, err := s.client.Request(ctx, req, &availableTemplates)
	if err != nil {
		return nil, resp, err
	}

	return availableTemplates, resp, nil
}

// Get a Gitignore by name.
//
// Giltab API docs: https://docs.gitlab.com/ee/api/templates/gitignores.html#single-gitignore-template
func (s GitignoreService) Get(ctx context.Context, name string) (*Gitignore, *Response, error) {
	u := fmt.Sprintf("templates/gitignores/%v", name)
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	gitignore := new(Gitignore)
	resp, err := s.client.Request(ctx, req, gitignore)
	if err != nil {
		return nil, resp, err
	}

	return gitignore, resp, nil
}
