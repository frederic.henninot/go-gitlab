package gitlab

import (
	"context"
	"fmt"
	"time"
)

type HooksService service

type BaseHook struct {
	Url                   *string    `json:"url,omitempty"`
	CreatedAt             *time.Time `json:"created_at,omitempty"`
	PushEvents            *bool      `json:"push_events,omitempty"`
	TagPushEvents         *bool      `json:"tag_push_events,omitempty"`
	MergeRequestsEvents   *bool      `json:"merge_requests_events,omitempty"`
	EnableSslVerification *bool      `json:"enable_ssl_verification,omitempty"`
}

type SystemHookData struct {
	BaseHook
	RepositoryUpdateEvents *bool   `json:"repository_update_events,omitempty"`
	Token                  *string `json:"token,omitempty"`
}

type SystemHook struct {
	Id *uint64 `json:"id,omitempty"`
	SystemHookData
}

type Hook struct {
	BaseHook
	IssuesEvents             *bool `json:"issues_events,omitempty"`
	ConfidentialIssuesEvents *bool `json:"confidential_issues_events,omitempty"`
	NoteEvents               *bool `json:"note_events,omitempty"`
	JobEvents                *bool `json:"job_events,omitempty"`
	PipelineEvents           *bool `json:"pipeline_events,omitempty"`
	WikiPageEvents           *bool `json:"wiki_page_events,omitempty"`
}

type GroupHookData struct {
	Hook
	GroupId                *uint64 `json:"group_id,omitempty"`
	ConfidentialNoteEvents *bool   `json:"confidential_note_events,omitempty"`
	Token                  *string `json:"token,omitempty"`
}

type GroupHook struct {
	Id *uint64 `json:"id,omitempty"`
	GroupHookData
}

type ProjectHookData struct {
	Hook
	ProjectId              *uint64 `json:"project_id,omitempty"`
	PushEventsBranchFilter *bool   `json:"push_events_branch_filter,omitempty"`
	Token                  *string `json:"token,omitempty"`
}

type ProjectHook struct {
	Id *uint64 `json:"id,omitempty"`
	ProjectHookData
}

// List system hooks
//
// GitLab API docs: https://docs.gitlab.com/ee/api/system_hooks.html#list-system-hooks
func (s *HooksService) SystemHooks(ctx context.Context) (interface{}, *Response, error) {
	var hooks []*SystemHook
	return s.list(ctx, "hooks", hooks)
}

// Add new system hook
//
// GitLab API docs: https://docs.gitlab.com/ee/api/system_hooks.html#add-new-system-hook
func (s *HooksService) SystemHookAdd(ctx context.Context, data SystemHookData) (interface{}, *Response, error) {
	hook := new(SystemHook)
	return s.addOrEdit(ctx, "hooks", true, &data, &hook)
}

// Delete system hook
//
// hook_id: Hook ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/system_hooks.html#delete-system-hook
func (s *HooksService) SystemHookDelete(ctx context.Context, hook_id uint) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("hooks/%v", hook_id))
}

type HookTest struct {
	ProjectId  *uint64 `json:"project_id,omitempty"`
	OwnerEmail *string `json:"owner_email,omitempty"`
	OwnerName  *string `json:"owner_name,omitempty"`
	Name       *string `json:"name,omitempty"`
	Path       *string `json:"path,omitempty"`
	EventName  *string `json:"event_name,omitempty"`
}

// Test system hook
//
// hook_id: Hook ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/system_hooks.html#test-system-hook
func (s *HooksService) Test(ctx context.Context, hook_id uint) (*HookTest, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("hooks/%v", hook_id), nil)
	if err != nil {
		return nil, nil, err
	}

	hook := new(HookTest)
	resp, err := s.client.Request(ctx, req, &hook)
	if err != nil {
		return nil, resp, err
	}

	return hook, resp, nil
}

// List group hooks
//
// id: Group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#list-group-hooks
func (s *HooksService) GroupHooks(ctx context.Context, id uint) (interface{}, *Response, error) {
	var hooks []*GroupHook
	return s.list(ctx, fmt.Sprintf("groups/%v/hooks", id), &hooks)
}

// Get group hook
//
// id: Group ID
// hook_id: Group Hook ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#get-group-hook
func (s *HooksService) GroupHook(ctx context.Context, id uint, hook_id uint) (interface{}, *Response, error) {
	hook := new(GroupHook)
	return s.list(ctx, fmt.Sprintf("groups/%v/hooks/%v", id, hook_id), &hook)
}

// Add group hook
//
// id: Group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#add-group-hook
func (s *HooksService) GroupHookAdd(ctx context.Context, id uint, data GroupHookData) (interface{}, *Response, error) {
	hook := new(GroupHook)
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/hooks", id), true, &data, &hook)
}

// Edit group hook
//
// id: Group ID
// hook_id: Group Hook ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#edit-group-hook
func (s *HooksService) GroupHookEdit(ctx context.Context, id uint, hook_id uint, data GroupHookData) (interface{}, *Response, error) {
	hook := new(GroupHook)
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/hooks/%v", id, hook_id), false, &data, &hook)
}

// Delete group hook
//
// id: Group ID
// hook_id: Hook ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#delete-group-hook
func (s *HooksService) GroupHookDelete(ctx context.Context, id uint, hook_id uint) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("groups/%v/hooks/%v", id, hook_id))
}

// List project hooks
//
// id: Project ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#list-group-hooks
func (s *HooksService) ProjectHooks(ctx context.Context, id uint) (interface{}, *Response, error) {
	var hooks []*ProjectHook
	return s.list(ctx, fmt.Sprintf("projects/%v/hooks", id), &hooks)
}

// Get project hook
//
// id: Project ID
// hook_id: Group Hook ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/projects.html#get-project-hook
func (s *HooksService) ProjectHook(ctx context.Context, id uint, hook_id uint) (interface{}, *Response, error) {
	hook := new(ProjectHook)
	return s.single(ctx, fmt.Sprintf("projects/%v/hooks/%v", id, hook_id), hook)
}

// Add project hook
//
// id: Project ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/projects.html#add-project-hook
func (s *HooksService) ProjectHookAdd(ctx context.Context, id uint, data ProjectHookData) (interface{}, *Response, error) {
	hook := new(ProjectHook)
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/hooks", id), true, &data, &hook)
}

// Edit project hook
//
// id: Project ID
// hook_id: Group Hook ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/projects.html#edit-project-hook
func (s *HooksService) ProjectHookEdit(ctx context.Context, id uint, hook_id uint, data ProjectHookData) (interface{}, *Response, error) {
	hook := new(ProjectHook)
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/hooks/%v", id, hook_id), false, &data, &hook)
}

// Delete project hook
//
// id: Project ID
// hook_id: Hook ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/projects.html#delete-project-hook
func (s *HooksService) ProjectHookDelete(ctx context.Context, id uint, hook_id uint) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("projects/%v/hooks/%v", id, hook_id))
}

func (s *HooksService) list(ctx context.Context, url string, hooks interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &hooks)
	if err != nil {
		return nil, resp, err
	}

	return hooks, resp, nil
}

func (s *HooksService) single(ctx context.Context, url string, hook interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &hook)
	if err != nil {
		return nil, resp, err
	}

	return hook, resp, nil
}

func (s *HooksService) addOrEdit(ctx context.Context, url string, add bool, data interface{}, hook interface{}) (interface{}, *Response, error) {
	method := "PUT"
	if true == add {
		method = "POST"
	}
	req, err := s.client.NewRequest(method, url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &hook)
	if err != nil {
		return nil, resp, err
	}

	return hook, resp, nil
}

func (s *HooksService) remove(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
