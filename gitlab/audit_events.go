package gitlab

import (
	"context"
	"fmt"
	"time"
)

type AuditEventsService service

type AuditEventsDetails struct {
	CustomMessage *string `json:"custom_message,omitempty"`
	Add           *string `json:"add,omitempty"`
	Change        *string `json:"change,omitempty"`
	From          *string `json:"from,omitempty"`
	To            *string `json:"to,omitempty"`
	AuthorName    *string `json:"author_name,omitempty"`
	TargetId      *string `json:"target_id,omitempty"`
	TargetType    *string `json:"target_type,omitempty"`
	TargetDetails *string `json:"target_details,omitempty"`
	IpAddress     *string `json:"ip_address,omitempty"`
	EntityPath    *string `json:"entity_path,omitempty"`
}
type AuditEvents struct {
	Id         *uint64             `json:"id,omitempty"`
	AuthorId   *uint64             `json:"author_id,omitempty"`
	EntityId   *uint64             `json:"entity_id,omitempty"`
	EntityType *string             `json:"entity_type,omitempty"`
	Details    *AuditEventsDetails `json:"details,omitempty"`
	CreatedAt  *time.Time          `json:"created_at,omitempty"`
}

type DateFilterOptions struct {
	CreatedAfter  *time.Time `json:"created_after,omitempty"`
	CreatedBefore *time.Time `json:"created_before,omitempty"`
}
type FilterOptions struct {
	DateFilterOptions
	EntityType *string `json:"entity_type,omitempty"`
	EntityId   *uint64 `json:"entity_id,omitempty"`
	PaginationOptions
}

type GroupFilterOptions struct {
	DateFilterOptions
	PaginationOptions
}

// Retreive all instance audit events
//
// GitLab API docs: https://docs.gitlab.com/ee/api/audit_events.html#retrieve-all-instance-audit-events
func (s *AuditEventsService) List(ctx context.Context, opts *FilterOptions) ([]*AuditEvents, *Response, error) {
	u, err := addUrlOptions("audit_events", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var auditEvents []*AuditEvents
	resp, err := s.client.Request(ctx, req, &auditEvents)
	if err != nil {
		return nil, resp, err
	}

	return auditEvents, resp, nil
}

// Retreive single instance audit event
//
// GitLab API docs: https://docs.gitlab.com/ee/api/audit_events.html#retrieve-single-instance-audit-event
func (s *AuditEventsService) Single(ctx context.Context, id string) (*AuditEvents, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("audit_events/%v", id), nil)
	if err != nil {
		return nil, nil, err
	}

	auditEvents := new(AuditEvents)
	resp, err := s.client.Request(ctx, req, &auditEvents)
	if err != nil {
		return nil, resp, err
	}

	return auditEvents, resp, nil
}

// Retreive all Group audit events
//
// GitLab API docs: https://docs.gitlab.com/ee/api/audit_events.html#group-audit-events-starter
func (s *AuditEventsService) GroupList(ctx context.Context, group string, opts *GroupFilterOptions) ([]*AuditEvents, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("groups/%v/audit_events", group), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var auditEvents []*AuditEvents
	resp, err := s.client.Request(ctx, req, &auditEvents)
	if err != nil {
		return nil, resp, err
	}

	return auditEvents, resp, nil
}

// Retreive single group audit event
//
// GitLab API docs: https://docs.gitlab.com/ee/api/audit_events.html#retrieve-a-specific-group-audit-event
func (s *AuditEventsService) GroupSingle(ctx context.Context, group string, id string) (*AuditEvents, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("groups/%v/audit_events/%v", group, id), nil)
	if err != nil {
		return nil, nil, err
	}

	auditEvents := new(AuditEvents)
	resp, err := s.client.Request(ctx, req, &auditEvents)
	if err != nil {
		return nil, resp, err
	}

	return auditEvents, resp, nil
}
