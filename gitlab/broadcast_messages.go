package gitlab

import (
	"context"
	"fmt"
	"time"
)

type BroadcastMessagesService service

type BroadcastMessage struct {
	Id            *string    `json:"id,omitempty"`
	Message       *string    `json:"message,omitempty"`
	Color         *string    `json:"color,omitempty"`
	Font          *string    `json:"font,omitempty"`
	Active        *bool      `json:"active,omitempty"`
	TargetPath    *string    `json:"target_path,omitempty"`
	StartsAt      *time.Time `json:"starts_at,omitempty"`
	EndsAt        *time.Time `json:"ends_at,omitempty"`
	BroadcastType *string    `json:"broadcast_type,omitempty"`
	Dismissable   *bool      `json:"dismissable,omitempty"`
}

// Get all broadcast messages
//
// GitLab API docs: https://docs.gitlab.com/ee/api/broadcast_messages.html#get-all-broadcast-messages
func (s *BroadcastMessagesService) List(ctx context.Context, opts *PaginationOptions) ([]*BroadcastMessage, *Response, error) {
	u, err := addUrlOptions("broadcast_messages", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var broadcastMessages []*BroadcastMessage
	resp, err := s.client.Request(ctx, req, &broadcastMessages)
	if err != nil {
		return nil, resp, err
	}

	return broadcastMessages, resp, nil
}

// Get a specific broadcast message
//
// id: broadcast message ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/broadcast_messages.html#get-a-specific-broadcast-message
func (s *BroadcastMessagesService) Single(ctx context.Context, id string) (*BroadcastMessage, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("broadcast_messages/%v", id), nil)
	if err != nil {
		return nil, nil, err
	}

	broadcastMessage := new(BroadcastMessage)
	resp, err := s.client.Request(ctx, req, &broadcastMessage)
	if err != nil {
		return nil, resp, err
	}

	return broadcastMessage, resp, nil
}

// Create a broadcast message
//
// GitLab API docs: https://docs.gitlab.com/ee/api/broadcast_messages.html#create-a-broadcast-message
func (s *BroadcastMessagesService) Create(ctx context.Context, data *BroadcastMessage) (*BroadcastMessage, *Response, error) {
	req, err := s.client.NewRequest("POST", "broadcast_messages", data)
	if err != nil {
		return nil, nil, err
	}

	broadcastMessage := new(BroadcastMessage)
	resp, err := s.client.Request(ctx, req, &broadcastMessage)
	if err != nil {
		return nil, resp, err
	}

	return broadcastMessage, resp, nil
}

// Update a broadcast message
//
// id: broadcast message ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/broadcast_messages.html#update-a-broadcast-message
func (s *BroadcastMessagesService) Update(ctx context.Context, id string, data *BroadcastMessage) (*BroadcastMessage, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("broadcast_messages/%v", id), data)
	if err != nil {
		return nil, nil, err
	}

	broadcastMessage := new(BroadcastMessage)
	resp, err := s.client.Request(ctx, req, &broadcastMessage)
	if err != nil {
		return nil, resp, err
	}

	return broadcastMessage, resp, nil
}

// Delete a broadcast message
//
// id: broadcast message ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/broadcast_messages.html#delete-a-broadcast-message
func (s *BroadcastMessagesService) Delete(ctx context.Context, id string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("broadcast_messages/%v", id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
