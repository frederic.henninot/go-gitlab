package gitlab

import (
	"context"
	"fmt"
)

type DockerFilesServices service

type DockerFile struct {
	Name    *string `json:"name,omitempty"`
	Content *string `json:"content,omitempty"`
}

// List all available dockerFile templates.
//
// Giltab API docs: https://docs.gitlab.com/ee/api/templates/dockerfiles.html
func (s DockerFilesServices) List(ctx context.Context) ([]string, *Response, error) {
	req, err := s.client.NewRequest("GET", "templates/dockerfiles", nil)
	if err != nil {
		return nil, nil, err
	}

	var availableTemplates []string
	resp, err := s.client.Request(ctx, req, &availableTemplates)
	if err != nil {
		return nil, resp, err
	}

	return availableTemplates, resp, nil
}

// Get a dockerFile by name.
//
// Giltab API docs: https://docs.gitlab.com/ee/api/templates/dockerfiles.html#single-dockerfile-template
func (s DockerFilesServices) Get(ctx context.Context, name string) (*DockerFile, *Response, error) {
	u := fmt.Sprintf("templates/dockerfiles/%v", name)
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	dockerfileTmpl := new(DockerFile)
	resp, err := s.client.Request(ctx, req, dockerfileTmpl)
	if err != nil {
		return nil, resp, err
	}

	return dockerfileTmpl, resp, nil
}
