package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"time"
)

type GroupsService service

type Groupe struct {
	Id                             *uint64          `json:"id,omitempty"`
	Name                           *string          `json:"name,omitempty"`
	Path                           *string          `json:"path,omitempty"`
	Description                    *string          `json:"description,omitempty"`
	Visibility                     *string          `json:"visibility,omitempty"`
	ShareWithGroupLock             *bool            `json:"share_with_group_lock,omitempty"`
	RequireTwoFactorAuthentication *bool            `json:"require_two_factor_authentication,omitempty"`
	TwoFactorGracePeriod           *uint16          `json:"two_factor_grace_period,omitempty"`
	ProjectCreationLevel           *string          `json:"project_creation_level,omitempty"`
	AutoDevopsEnabled              *bool            `json:"auto_devops_enabled,omitempty"`
	SubgroupCreationLevel          *string          `json:"subgroup_creation_level,omitempty"`
	EmailsDisabled                 *bool            `json:"emails_disabled,omitempty"`
	MentionsDisabled               *bool            `json:"mentions_disabled,omitempty"`
	LfsEnabled                     *bool            `json:"lfs_enabled,omitempty"`
	DefaultBranchProtection        *uint            `json:"default_branch_protection,omitempty"`
	AvatarUrl                      *string          `json:"avatar_url,omitempty"`
	WebUrl                         *string          `json:"web_url,omitempty"`
	RequestAccessEnabled           *bool            `json:"request_access_enabled,omitempty"`
	FullName                       *string          `json:"full_name,omitempty"`
	FullPath                       *string          `json:"full_path,omitempty"`
	FileTemplateProjectId          *uint64          `json:"file_template_project_id,omitempty"`
	ParentId                       *uint64          `json:"parent_id,omitempty"`
	CreatedAt                      *time.Time       `json:"created_at,omitempty"`
	Statistics                     *GroupStatistics `json:"statistics,omitempty"`
	Projects                       *[]Project       `json:"projects,omitempty"`
	SharedProjects                 *[]Project       `json:"shared_projects,omitempty"`
}

type GroupStatistics struct {
	StorageSize      *uint32 `json:"storage_size,omitempty"`
	RepositorySize   *uint32 `json:"repository_size,omitempty"`
	WikiSize         *uint32 `json:"wiki_size,omitempty"`
	LfsObjectsSize   *uint32 `json:"lfs_objects_size,omitempty"`
	JobArtifactsSize *uint32 `json:"job_artifacts_size,omitempty"`
	PackagesSize     *uint32 `json:"packages_size,omitempty"`
}

type GroupDetailOption struct {
	WithCustomAttributes *bool            `json:"with_custom_attributes,omitempty"`
	CustomAttributes     *CustomAttribute `json:"custom_attributes,omitempty"`
	WithProjects         *bool            `json:"with_projects,omitempty"`
}

type GroupOption struct {
	GroupDetailOption
	SkipGroups     *[]uint `json:"skip_groups,omitempty"`
	AllAvailable   *bool   `json:"all_available,omitempty"`
	Search         *string `json:"search,omitempty"`
	OrderBy        *string `json:"order_by,omitempty"`
	Sort           *string `json:"sort,omitempty"`
	Statistics     *bool   `json:"statistics,omitempty"`
	Owned          *bool   `json:"owned,omitempty"`
	MinAccessLevel *uint   `json:"min_access_level,omitempty"`
	PaginationOptions
}

type GroupProjectOption struct {
	GroupOption
	Archived                 *bool   `json:"archived,omitempty"`
	Visibility               *string `json:"visibility,omitempty"`
	Simple                   *bool   `json:"simple,omitempty"`
	Starred                  *bool   `json:"starred,omitempty"`
	WithIssuesEnabled        *bool   `json:"with_issues_enabled,omitempty"`
	WithMergeRequestsEnabled *bool   `json:"with_merge_requests_enabled,omitempty"`
	WithShared               *bool   `json:"with_shared,omitempty"`
	IncludeSubgroups         *bool   `json:"include_subgroups,omitempty"`
	WithSecurityReports      *bool   `json:"with_security_reports,omitempty"`
}

type GroupeData struct {
	Name                           *string          `json:"name,omitempty"`
	Path                           *string          `json:"path,omitempty"`
	Description                    *string          `json:"description,omitempty"`
	MembershipLock                 *bool            `json:"membership_lock,omitempty"`
	Visibility                     *string          `json:"visibility,omitempty"`
	ShareWithGroupLock             *bool            `json:"share_with_group_lock,omitempty"`
	RequireTwoFactorAuthentication *bool            `json:"require_two_factor_authentication,omitempty"`
	TwoFactorGracePeriod           *uint16          `json:"two_factor_grace_period,omitempty"`
	ProjectCreationLevel           *string          `json:"project_creation_level,omitempty"`
	AutoDevopsEnabled              *bool            `json:"auto_devops_enabled,omitempty"`
	SubgroupCreationLevel          *string          `json:"subgroup_creation_level,omitempty"`
	EmailsDisabled                 *bool            `json:"emails_disabled,omitempty"`
	Avatar                         *json.RawMessage `json:"avatar,omitempty"`
	MentionsDisabled               *bool            `json:"mentions_disabled,omitempty"`
	LfsEnabled                     *bool            `json:"lfs_enabled,omitempty"`
	RequestAccessEnabled           *bool            `json:"request_access_enabled,omitempty"`
	ParentId                       *uint64          `json:"parent_id,omitempty"`
	DefaultBranchProtection        *uint            `json:"default_branch_protection,omitempty"`
	SharedRunnersMinutesLimit      *uint            `json:"shared_runners_minutes_limit,omitempty"`
	ExtraSharedRunnersMinutesLimit *uint            `json:"extra_shared_runners_minutes_limit,omitempty"`
}

// List groups
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#list-groups
func (s *GroupsService) List(ctx context.Context, opts *GroupOption) ([]*Groupe, *Response, error) {
	return s.list(ctx, "groups", opts)
}

// List a group’s subgroups
//
// id: group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#list-a-groups-subgroups
func (s *GroupsService) Subgroups(ctx context.Context, id uint, opts *GroupOption) ([]*Groupe, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/subgroups", id), opts)
}

// List a group’s projects
//
// id: group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects
func (s *GroupsService) Projects(ctx context.Context, id uint, opts *GroupProjectOption) ([]*Groupe, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/projects", id), opts)
}

// Details of a group
//
// id: group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#details-of-a-group
func (s *GroupsService) Single(ctx context.Context, id uint, opts *GroupDetailOption) (*Groupe, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("groups/%v", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	groupe := new(Groupe)
	resp, err := s.client.Request(ctx, req, &groupe)
	if err != nil {
		return nil, resp, err
	}

	return groupe, resp, nil
}

// New group
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#new-group
func (s *GroupsService) Create(ctx context.Context, data *GroupeData) (*Groupe, *Response, error) {
	req, err := s.client.NewRequest("POST", "groups", data)
	if err != nil {
		return nil, nil, err
	}

	groupe := new(Groupe)
	resp, err := s.client.Request(ctx, req, &groupe)
	if err != nil {
		return nil, resp, err
	}

	return groupe, resp, nil
}

// Update group
//
// id: group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#update-group
func (s *GroupsService) Update(ctx context.Context, id uint, data *GroupeData) (*Groupe, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("groups/%v", id), data)
	if err != nil {
		return nil, nil, err
	}

	groupe := new(Groupe)
	resp, err := s.client.Request(ctx, req, &groupe)
	if err != nil {
		return nil, resp, err
	}

	return groupe, resp, nil
}

// Transfer project to group
//
// id: group ID
// project_id: Project ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#transfer-project-to-group
func (s *GroupsService) Transfert(ctx context.Context, id uint, project_id uint, data *GroupeData) (*Groupe, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("groups/%v/projects/%v", id, project_id), data)
	if err != nil {
		return nil, nil, err
	}

	groupe := new(Groupe)
	resp, err := s.client.Request(ctx, req, &groupe)
	if err != nil {
		return nil, resp, err
	}

	return groupe, resp, nil
}

// Remove group
//
// id: group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#remove-group
func (s *GroupsService) Delete(ctx context.Context, id uint) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("broadcast_messages/%v", id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// Restore group marked for deletion
//
// id: group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/groups.html#restore-group-marked-for-deletion-premium
func (s *GroupsService) Restore(ctx context.Context, id uint, data *GroupeData) (*Groupe, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("groups/%v/restore", id), data)
	if err != nil {
		return nil, nil, err
	}

	groupe := new(Groupe)
	resp, err := s.client.Request(ctx, req, &groupe)
	if err != nil {
		return nil, resp, err
	}

	return groupe, resp, nil
}

func (s *GroupsService) list(ctx context.Context, url string, opts *interface{}) ([]*Groupe, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var groupes []*Groupe
	resp, err := s.client.Request(ctx, req, &groupes)
	if err != nil {
		return nil, resp, err
	}

	return groupes, resp, nil
}
