package gitlab

import (
	"context"
	"fmt"
	"time"
)

type EpicLinksService service

type EpicLink struct {
	Id                           *uint64    `json:"id,omitempty"`
	Iid                          *uint64    `json:"iid,omitempty"`
	GroupId                      *uint64    `json:"group_id,omitempty"`
	ParentId                     *uint64    `json:"parent_id,omitempty"`
	Title                        *string    `json:"title,omitempty"`
	Description                  *string    `json:"description,omitempty"`
	Author                       *Author    `json:"author,omitempty"`
	StartDate                    *JSONDate  `json:"start_date,omitempty"`
	EndSate                      *JSONDate  `json:"end_date,omitempty"`
	StartDateIsFixed             *bool      `json:"start_date_is_fixed,omitempty"`
	StartDateFixed               *JSONDate  `json:"start_date_fixed,omitempty"`
	StartDateFromInheritedSource *JSONDate  `json:"start_date_from_inherited_source,omitempty"`
	DueDate                      *JSONDate  `json:"due_date,omitempty"`
	DueDateIsFixed               *bool      `json:"due_date_is_fixed,omitempty"`
	DueDateFixed                 *JSONDate  `json:"due_date_fixed,omitempty"`
	DueDateFromInheritedSource   *JSONDate  `json:"due_date_from_inherited_source,omitempty"`
	CreatedAt                    *time.Time `json:"created_at,omitempty"`
	UpdatedAt                    *time.Time `json:"updated_at,omitempty"`
	Labels                       *[]string  `json:"labels,omitempty"`
}

type EpicLinkAssignData struct {
	ChildEpicId *uint64 `json:"child_epic_id,omitempty"`
}

type EpicLinkData struct {
	Title *string `json:"title,omitempty"`
}

type EpicLinkOrderData struct {
	MoveBeforeId *uint64 `json:"move_before_id,omitempty"`
	MoveAfterId  *uint64 `json:"move_after_id,omitempty"`
}

// List epics related to a given epic
//
// id: The ID or URL-encoded path of the group
// epic_iid: The internal ID of the epic.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epic_links.html#list-epics-related-to-a-given-epic
func (s *EpicLinksService) List(ctx context.Context, id string, epic_iid uint64) ([]*EpicLink, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("groups/%v/epics/%v/epics", id, epic_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	var epicLinks []*EpicLink
	resp, err := s.client.Request(ctx, req, &epicLinks)
	if err != nil {
		return nil, resp, err
	}

	return epicLinks, resp, nil
}

// Assign a child epic
//
// id: The ID or URL-encoded path of the group
// epic_iid: The internal ID of the epic.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epic_links.html#assign-a-child-epic
func (s *EpicLinksService) Assign(ctx context.Context, id string, epic_iid uint64, data *EpicLinkAssignData) (*EpicLink, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("groups/%v/epics/%v/epics", id, epic_iid), data)
	if err != nil {
		return nil, nil, err
	}

	epicLink := new(EpicLink)
	resp, err := s.client.Request(ctx, req, &epicLink)
	if err != nil {
		return nil, resp, err
	}

	return epicLink, resp, nil
}

// Create and assign a child epic
//
// id: The ID or URL-encoded path of the group
// epic_iid: The internal ID of the epic.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epic_links.html#create-and-assign-a-child-epic
func (s *EpicLinksService) Create(ctx context.Context, id string, epic_iid uint64, data *EpicLinkData) (*EpicLink, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("groups/%v/epics/%v/epics", id, epic_iid), data)
	if err != nil {
		return nil, nil, err
	}

	epicLink := new(EpicLink)
	resp, err := s.client.Request(ctx, req, &epicLink)
	if err != nil {
		return nil, resp, err
	}

	return epicLink, resp, nil
}

// Re-order a child epic
//
// id: The ID or URL-encoded path of the group
// epic_iid: The internal ID of the epic.
// child_epic_id: The global ID of the child epic. Internal ID can’t be used because they can conflict with epics from other groups.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epic_links.html#re-order-a-child-epic
func (s *EpicLinksService) Move(ctx context.Context, id string, epic_iid uint64, child_epic_id uint64, data *EpicLinkOrderData) (*EpicLink, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("groups/%v/epics/%v/epics/%v", id, epic_iid, child_epic_id), data)
	if err != nil {
		return nil, nil, err
	}

	epicLink := new(EpicLink)
	resp, err := s.client.Request(ctx, req, &epicLink)
	if err != nil {
		return nil, resp, err
	}

	return epicLink, resp, nil
}

// Unassign a child epic
//
// id: The ID or URL-encoded path of the group
// epic_iid: The internal ID of the epic.
// child_epic_id: The global ID of the child epic. Internal ID can’t be used because they can conflict with epics from other groups.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/epic_links.html#unassign-a-child-epic
func (s *EpicLinksService) Unassign(ctx context.Context, id string, epic_iid uint64, child_epic_id uint64) (*EpicLink, *Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("groups/%v/epics/%v/epics/%v", id, epic_iid, child_epic_id), nil)
	if err != nil {
		return nil, nil, err
	}

	epicLink := new(EpicLink)
	resp, err := s.client.Request(ctx, req, &epicLink)
	if err != nil {
		return nil, resp, err
	}

	return epicLink, resp, nil
}
