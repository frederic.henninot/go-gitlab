package gitlab

import (
	"context"
	"fmt"
	"time"
)

type IssueStatisticsService service

type IssueStatisticsItemOptions struct {
	Labels           *string    `json:"labels,omitempty"`
	Milestone        *string    `json:"milestone,omitempty"`
	Scope            *string    `json:"scope,omitempty"`
	AuthorId         *uint      `json:"author_id,omitempty"`
	AuthorUsername   *string    `json:"author_username,omitempty"`
	AssigneeId       *uint      `json:"assignee_id,omitempty"`
	AssigneeUsername *[]string  `json:"assignee_username,omitempty"`
	MyReactionEmoji  *string    `json:"my_reaction_emoji,omitempty"`
	Iids             *[]uint    `json:"iids,omitempty"`
	Search           *string    `json:"search,omitempty"`
	CreatedAfter     *time.Time `json:"created_after,omitempty"`
	CreatedBefore    *time.Time `json:"created_before,omitempty"`
	UpdatedAfter     *time.Time `json:"updated_after,omitempty"`
	UpdatedBefore    *time.Time `json:"updated_before,omitempty"`
	Confidential     *bool      `json:"confidential,omitempty"`
	PaginationOptions
}
type IssueStatisticsOptions struct {
	In *string `json:"in,omitempty"`
}

type IssueStatisticsCount struct {
	All    *uint `json:"all,omitempty"`
	Closed *uint `json:"closed,omitempty"`
	Opened *uint `json:"opened,omitempty"`
}

type IssueStatistic struct {
	Counts *IssueStatisticsCount `json:"counts,omitempty"`
}

// Get issues statistics
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues_statistics.html#get-issues-statistics
func (s *IssueStatisticsService) List(ctx context.Context, opts *IssueStatisticsOptions) (*IssueStatistic, *Response, error) {
	u, err := addUrlOptions("issues_statistics", opts)
	if err != nil {
		return nil, nil, err
	}

	return s.list(ctx, u)
}

// Get group issues statistics
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues_statistics.html#get-group-issues-statistics
func (s *IssueStatisticsService) GroupList(ctx context.Context, id string, opts *IssueStatisticsItemOptions) (*IssueStatistic, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("groups/%v/issues_statistics", id), opts)
	if err != nil {
		return nil, nil, err
	}

	return s.list(ctx, u)
}

// Get project issues statistics
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues_statistics.html#get-project-issues-statistics
func (s *IssueStatisticsService) ProjectList(ctx context.Context, id string, opts *IssueStatisticsItemOptions) (*IssueStatistic, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/issues_statistics", id), opts)
	if err != nil {
		return nil, nil, err
	}

	return s.list(ctx, u)
}

func (s *IssueStatisticsService) list(ctx context.Context, url string) (*IssueStatistic, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	issueStatistics := new(IssueStatistic)
	resp, err := s.client.Request(ctx, req, &issueStatistics)
	if err != nil {
		return nil, resp, err
	}

	return issueStatistics, resp, nil
}
