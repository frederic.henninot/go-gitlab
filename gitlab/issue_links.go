package gitlab

import (
	"context"
	"fmt"
	"time"
)

type IssueLinksService service

type IssueLink struct {
	Id             *uint64    `json:"id,omitempty"`
	Iid            *uint64    `json:"iid,omitempty"`
	LinkType       *string    `json:"link_type,omitempty"`
	Title          *string    `json:"title,omitempty"`
	Description    *string    `json:"description,omitempty"`
	State          *string    `json:"state,omitempty"`
	IssueLinkId    *uint64    `json:"issue_link_id,omitempty"`
	ProjectId      *uint64    `json:"project_id,omitempty"`
	Assignees      *[]Author  `json:"assignees,omitempty"`
	Assignee       *Author    `json:"assignee,omitempty"`
	Labels         *[]string  `json:"labels,omitempty"`
	Author         *Author    `json:"author,omitempty"`
	Milestone      *Milestone `json:"milestone,omitempty"`
	Subscribed     *bool      `json:"subscribed,omitempty"`
	UserNotesCount *uint      `json:"user_notes_count,omitempty"`
	CreatedAt      *time.Time `json:"created_at,omitempty"`
	UpdatedAt      *time.Time `json:"updated_at,omitempty"`
	DueDate        *JSONDate  `json:"due_date,omitempty"`
	WebUrl         *string    `json:"web_url,omitempty"`
	Confidential   *bool      `json:"confidential,omitempty"`
	Weight         *uint      `json:"weight,omitempty"`
}

type IssueLinkResult struct {
	SourceIssue *IssueLink `json:"source_issue,omitempty"`
	TargetIssue *IssueLink `json:"target_issue,omitempty"`
}

type IssueLinkTypeData struct {
	LinkType *string `json:"link_type,omitempty"`
}

type IssueLinkData struct {
	IssueLinkTypeData
	TargetProjectId *uint64 `json:"target_project_id,omitempty"`
	TargetIssueIid  *uint64 `json:"target_issue_iid,omitempty"`
}

// List issue relations
//
// id: The ID or URL-encoded path of the project
// issue_iid: The internal ID of a project’s issue
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issue_links.html#list-issue-relations
func (s *IssueLinksService) List(ctx context.Context, id string, issue_iid uint64) ([]*IssueLink, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/issues/%v/links", id, issue_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	var issueLinks []*IssueLink
	resp, err := s.client.Request(ctx, req, &issueLinks)
	if err != nil {
		return nil, resp, err
	}

	return issueLinks, resp, nil
}

// Create an issue link
//
// id: The ID or URL-encoded path of the project
// issue_iid: The internal ID of a project’s issue
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issue_links.html#create-an-issue-link
func (s *IssueLinksService) Create(ctx context.Context, id string, issue_iid uint64, data *IssueLinkData) (*IssueLinkResult, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/issues/%v/links", id, issue_iid), data)
	if err != nil {
		return nil, nil, err
	}

	issueLinkResult := new(IssueLinkResult)
	resp, err := s.client.Request(ctx, req, &issueLinkResult)
	if err != nil {
		return nil, resp, err
	}

	return issueLinkResult, resp, nil
}

// Delete an issue link
//
// id: The ID or URL-encoded path of the project
// issue_iid: The internal ID of a project’s issue
// issue_link_id: The ID of an issue relationship
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issue_links.html#delete-an-issue-link
func (s *IssueLinksService) Delete(ctx context.Context, id string, issue_iid uint64, issue_link_id uint64, data *IssueLinkTypeData) (*IssueLinkResult, *Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/issues/%v/links/%v", id, issue_iid, issue_link_id), data)
	if err != nil {
		return nil, nil, err
	}

	issueLinkResult := new(IssueLinkResult)
	resp, err := s.client.Request(ctx, req, &issueLinkResult)
	if err != nil {
		return nil, resp, err
	}

	return issueLinkResult, resp, nil
}
