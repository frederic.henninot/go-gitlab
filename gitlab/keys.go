package gitlab

import (
	"context"
	"fmt"
	"time"
)

type KeysService service

type Key struct {
	Id        *uint64    `json:"id,omitempty"`
	Title     *string    `json:"title,omitempty"`
	Key       *string    `json:"key,omitempty"`
	CreatedAt *time.Time `json:"created_at,omitempty"`
	ExpiresAt *time.Time `json:"expires_at,omitempty"`
	User      *User      `json:"user,omitempty"`
}

type FingerprintOption struct {
	Fingerprint *string `json:"fingerprint,omitempty"`
}

// Get SSH key with user by ID of an SSH key
//
// id: The ID of an SSH key
//
// GitLab API docs: https://docs.gitlab.com/ee/api/keys.html#get-ssh-key-with-user-by-id-of-an-ssh-key
func (s *KeysService) ById(ctx context.Context, id string) (*Key, *Response, error) {
	return s.get(ctx, fmt.Sprintf("keys/%v", id))
}

// Get user by fingerprint of SSH key
//
// GitLab API docs: https://docs.gitlab.com/ee/api/keys.html#get-user-by-fingerprint-of-ssh-key
func (s *KeysService) ByFingerprint(ctx context.Context, opts FingerprintOption) (*Key, *Response, error) {
	u, err := addUrlOptions("keys", opts)
	if err != nil {
		return nil, nil, err
	}

	return s.get(ctx, u)
}

func (s *KeysService) get(ctx context.Context, url string) (*Key, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	key := new(Key)
	resp, err := s.client.Request(ctx, req, &key)
	if err != nil {
		return nil, resp, err
	}

	return key, resp, nil
}
