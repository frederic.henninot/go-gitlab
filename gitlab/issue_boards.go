package gitlab

import (
	"context"
	"fmt"
)

type IssueBoardsService service

type Board struct {
	Id        *uint64      `json:"id,omitempty"`
	Name      *string      `json:"name,omitempty"`
	Project   *Project     `json:"project,omitempty"`
	Group     *Groupe      `json:"group,omitempty"`
	Milestone *Milestone   `json:"milestone,omitempty"`
	Lists     *[]BoardList `json:"lists,omitempty"`
	Assignee  *Author      `json:"assignee,omitempty"`
	Labels    *[]Label     `json:"labels,omitempty"`
	Weight    *uint        `json:"weight,omitempty"`
}

type BoardList struct {
	Id        *uint64    `json:"id,omitempty"`
	Label     *Label     `json:"label,omitempty"`
	Position  *uint      `json:"position,omitempty"`
	Milestone *Milestone `json:"milestone,omitempty"`
}

type BoardData struct {
	Name        *string `json:"name,omitempty"`
	AssigneeId  *uint   `json:"assignee_id,omitempty"`
	MilestoneId *uint   `json:"milestone_id,omitempty"`
	Labels      *string `json:"labels,omitempty"`
	Weight      *uint   `json:"weight,omitempty"`
}

type BoardListData struct {
	LabelId     *uint `json:"label_id,omitempty"`
	AssigneeId  *uint `json:"assignee_id,omitempty"`
	MilestoneId *uint `json:"milestone_id,omitempty"`
}

type BoardListUpdateData struct {
	Position *uint `json:"position,omitempty"`
}

// Project Board
//
// id: Project ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#project-board
func (s *IssueBoardsService) ProjectBoards(ctx context.Context, id uint64) (interface{}, *Response, error) {
	var boards []*Board
	return s.list(ctx, fmt.Sprintf("projects/%v/boards", id), boards)
}

// Project Single Board
//
// id: Project ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#single-board
func (s *IssueBoardsService) ProjectBoard(ctx context.Context, id uint, board_id uint) (interface{}, *Response, error) {
	board := new(Board)
	return s.single(ctx, fmt.Sprintf("projects/%v/boards/%v", id, board_id), &board)
}

// Create a Project Board
//
// id: Project ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#create-a-board-starter
func (s *IssueBoardsService) ProjectBoardCreate(ctx context.Context, id uint, data BoardData) (interface{}, *Response, error) {
	board := new(Board)
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/boards", id), true, &data, &board)
}

// Update a Project Board
//
// id: Project ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#update-a-board-starter
func (s *IssueBoardsService) ProjectBoardUpdate(ctx context.Context, id uint, board_id uint, data BoardData) (interface{}, *Response, error) {
	board := new(Board)
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/boards/%v", id, board_id), false, &data, &board)
}

// Delete a board
//
// id: Project ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#delete-a-board-starter
func (s *IssueBoardsService) ProjectBoardDelete(ctx context.Context, id uint, board_id uint) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("projects/%v/boards/%v", id, board_id))
}

// Project Board Lists
//
// id: Project ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#list-board-lists
func (s *IssueBoardsService) ProjectBoardLists(ctx context.Context, id uint64, board_id uint) (interface{}, *Response, error) {
	var lists []*BoardList
	return s.list(ctx, fmt.Sprintf("projects/%v/boards/%v/lists", id, board_id), lists)
}

// Project Board Single List
//
// id: Project ID
// board_id: Board ID
// list_id: Board List ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#single-board-list
func (s *IssueBoardsService) ProjectBoardList(ctx context.Context, id uint64, board_id uint, list_id uint) (interface{}, *Response, error) {
	list := new(BoardList)
	return s.single(ctx, fmt.Sprintf("projects/%v/boards/%v/lists/%v", id, board_id, list_id), list)
}

// Create a Project Board List
//
// id: Project ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#new-board-list
func (s *IssueBoardsService) ProjectBoardListCreate(ctx context.Context, id uint, board_id, data BoardListData) (interface{}, *Response, error) {
	list := new(BoardList)
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/boards/%v/lists", id, board_id), true, &data, &list)
}

// Update a Project Board List
//
// id: Project ID
// board_id: Board ID
// list_id: Board List ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#edit-board-list
func (s *IssueBoardsService) ProjectBoardListUpdate(ctx context.Context, id uint, board_id uint, list_id uint, data BoardListUpdateData) (interface{}, *Response, error) {
	list := new(BoardList)
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/boards/%v/lists/%v", id, board_id, list_id), false, &data, &list)
}

// Delete a Project Board List
//
// id: Project ID
// board_id: Board ID
// list_id: Board List ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/boards.html#delete-a-board-list
func (s *IssueBoardsService) ProjectBoardListDelete(ctx context.Context, id uint, board_id uint, list_id uint) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("projects/%v/boards/%v/lists/%v", id, board_id, list_id))
}

// List all group issue boards in a group
//
// id: Group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#list-all-group-issue-boards-in-a-group
func (s *IssueBoardsService) GroupBoards(ctx context.Context, id uint64) (interface{}, *Response, error) {
	var boards []*Board
	return s.list(ctx, fmt.Sprintf("groups/%v/boards", id), boards)
}

// Single group issue board
//
// id: Group ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#single-group-issue-board
func (s *IssueBoardsService) GroupBoard(ctx context.Context, id uint, board_id uint) (interface{}, *Response, error) {
	board := new(Board)
	return s.single(ctx, fmt.Sprintf("groups/%v/boards/%v", id, board_id), &board)
}

// Create a group issue board
//
// id: Group ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#create-a-group-issue-board-premium
func (s *IssueBoardsService) GroupBoardCreate(ctx context.Context, id uint, data BoardData) (interface{}, *Response, error) {
	board := new(Board)
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/boards", id), true, &data, &board)
}

// Update a group issue board
//
// id: Group ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#update-a-group-issue-board-premium
func (s *IssueBoardsService) GroupBoardUpdate(ctx context.Context, id uint, board_id uint, data BoardData) (interface{}, *Response, error) {
	board := new(Board)
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/boards/%v", id, board_id), false, &data, &board)
}

// Delete a group issue board
//
// id: Group ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#delete-a-group-issue-board-premium
func (s *IssueBoardsService) GroupBoardDelete(ctx context.Context, id uint, board_id uint) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("groups/%v/boards/%v", id, board_id))
}

// List group issue board lists
//
// id: Group ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#list-group-issue-board-lists
func (s *IssueBoardsService) GroupBoardLists(ctx context.Context, id uint64, board_id uint) (interface{}, *Response, error) {
	var lists []*BoardList
	return s.list(ctx, fmt.Sprintf("groups/%v/boards/%v/lists", id, board_id), lists)
}

// Single group issue board list
//
// id: Group ID
// board_id: Board ID
// list_id: Board List ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#single-group-issue-board-list
func (s *IssueBoardsService) GroupBoardList(ctx context.Context, id uint64, board_id uint, list_id uint) (interface{}, *Response, error) {
	list := new(BoardList)
	return s.single(ctx, fmt.Sprintf("groups/%v/boards/%v/lists/%v", id, board_id, list_id), list)
}

// New group issue board list
//
// id: Group ID
// board_id: Board ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#new-group-issue-board-list
func (s *IssueBoardsService) GroupBoardListCreate(ctx context.Context, id uint, board_id, data BoardListData) (interface{}, *Response, error) {
	list := new(BoardList)
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/boards/%v/lists", id, board_id), true, &data, &list)
}

// Edit group issue board list
//
// id: Group ID
// board_id: Board ID
// list_id: Board List ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#edit-group-issue-board-list
func (s *IssueBoardsService) GroupBoardListUpdate(ctx context.Context, id uint, board_id uint, list_id uint, data BoardListUpdateData) (interface{}, *Response, error) {
	list := new(BoardList)
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/boards/%v/lists/%v", id, board_id, list_id), false, &data, &list)
}

// Delete a group issue board list
//
// id: Group ID
// board_id: Board ID
// list_id: Board List ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_boards.html#delete-a-group-issue-board-list
func (s *IssueBoardsService) GroupBoardListDelete(ctx context.Context, id uint, board_id uint, list_id uint) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("groups/%v/boards/%v/lists/%v", id, board_id, list_id))
}

func (s *IssueBoardsService) list(ctx context.Context, url string, values interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &values)
	if err != nil {
		return nil, resp, err
	}

	return values, resp, nil
}

func (s *IssueBoardsService) single(ctx context.Context, url string, value interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &value)
	if err != nil {
		return nil, resp, err
	}

	return value, resp, nil
}

func (s *IssueBoardsService) addOrEdit(ctx context.Context, url string, add bool, data interface{}, value interface{}) (interface{}, *Response, error) {
	method := "PUT"
	if true == add {
		method = "POST"
	}
	req, err := s.client.NewRequest(method, url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &value)
	if err != nil {
		return nil, resp, err
	}

	return value, resp, nil
}

func (s *IssueBoardsService) remove(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
