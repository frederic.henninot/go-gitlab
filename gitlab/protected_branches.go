package gitlab

import (
	"context"
	"fmt"
)

type ProtectedBranchesService service

type AccessLevel struct {
	AccessLevel            *uint8  `json:"access_level,omitempty"`
	UserId                 *uint64 `json:"user_id,omitempty"`
	GroupId                *uint64 `json:"group_id,omitempty"`
	AccessLevelDescription *string `json:"access_level_description,omitempty"`
}
type CodeOwnerApproval struct {
	CodeOwnerApprovalRequired *string `json:"code_owner_approval_required,omitempty"`
}
type AccessLevels struct {
	PushAccessLevels      *[]AccessLevel `json:"push_access_levels,omitempty"`
	MergeAccessLevels     *[]AccessLevel `json:"merge_access_levels,omitempty"`
	UnprotectAccessLevels *[]AccessLevel `json:"unprotect_access_level,omitempty"`
	CodeOwnerApproval
}

type ProtectedBranch struct {
	Id   *uint64 `json:"id,omitempty"`
	Name *string `json:"name,omitempty"`
	AccessLevels
}

type ProtectBranchOptions struct {
	Name *string `json:"name,omitempty"`
	AccessLevels
	AllowedToPush      *bool `json:"allowed_to_push,omitempty"`
	AllowedToMerge     *bool `json:"allowed_to_merge,omitempty"`
	AllowedToUnprotect *bool `json:"allowed_to_unprotect,omitempty"`
}

// List protected branches
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/protected_branches.html#list-protected-branches
func (s *ProtectedBranchesService) List(ctx context.Context, id string, opts *BranchOptions) ([]*ProtectedBranch, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/protected_branches", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var protectedBranches []*ProtectedBranch
	resp, err := s.client.Request(ctx, req, &protectedBranches)
	if err != nil {
		return nil, resp, err
	}

	return protectedBranches, resp, nil
}

// Get a single protected branch or wildcard protected branch
//
// id: project ID or path
// name: Branch name or wildcard
//
// GitLab API docs: https://docs.gitlab.com/ee/api/protected_branches.html#get-a-single-protected-branch-or-wildcard-protected-branch
func (s *ProtectedBranchesService) Single(ctx context.Context, id string, name string) (*ProtectedBranch, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/protected_branches/%v", id, name), nil)
	if err != nil {
		return nil, nil, err
	}

	protectedBranch := new(ProtectedBranch)
	resp, err := s.client.Request(ctx, req, &protectedBranch)
	if err != nil {
		return nil, resp, err
	}

	return protectedBranch, resp, nil
}

// Protect repository branches
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches
func (s *ProtectedBranchesService) Protect(ctx context.Context, id string, opts *ProtectBranchOptions) (*ProtectedBranch, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/protected_branches", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("POST", u, nil)
	if err != nil {
		return nil, nil, err
	}

	protectedBranch := new(ProtectedBranch)
	resp, err := s.client.Request(ctx, req, &protectedBranch)
	if err != nil {
		return nil, resp, err
	}

	return protectedBranch, resp, nil
}

// Unprotect repository branches
//
// id: project ID or path
// name: Branch name
//
// GitLab API docs: https://docs.gitlab.com/ee/api/protected_branches.html#unprotect-repository-branches
func (s *ProtectedBranchesService) Unprotect(ctx context.Context, id string, name string, opts *CodeOwnerApproval) (*Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/protected_branches/%v", id, name), opts)
	if err != nil {
		return nil, err
	}

	req, err := s.client.NewRequest("PATCH", u, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
