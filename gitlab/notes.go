package gitlab

import "time"

type NotesService service

type Note struct {
	Id           *uint64    `json:"id,omitempty"`
	Body         *string    `json:"body,omitempty"`
	Attachment   *string    `json:"attachment,omitempty"`
	Author       *Author    `json:"author,omitempty"`
	CreatedAt    *time.Time `json:"created_at,omitempty"`
	UpdatedAt    *time.Time `json:"updated_at,omitempty"`
	System       *bool      `json:"system,omitempty"`
	NoteableId   *uint64    `json:"noteable_id,omitempty"`
	NoteableType *string    `json:"noteable_type,omitempty"`
	NoteableIID  *uint64    `json:"noteable_iid,omitempty"`
	Resolvable   *bool      `json:"resolvable,omitempty"`
	Confidential *bool      `json:"confidential,omitempty"`
}
