package gitlab

import (
	"context"
	"fmt"
	"time"
)

type ClustersService service

type ClusterUser struct {
	Author
}

type ClusterPlatformKubernetes struct {
	ApiUrl            *string `json:"api_url,omitempty"`
	Namespace         *string `json:"namespace,omitempty"`
	AuthorizationType *string `json:"authorization_type,omitempty"`
	CaCert            *string `json:"ca_cert,omitempty"`
}

type ClusterManagementProject struct {
	Id                *uint64    `json:"id,omitempty"`
	Description       *string    `json:"description,omitempty"`
	Name              *string    `json:"name,omitempty"`
	NameWithNamespace *string    `json:"name_with_namespace,omitempty"`
	Path              *string    `json:"path,omitempty"`
	PathWithNamespace *string    `json:"path_with_namespace,omitempty"`
	CreatedAt         *time.Time `json:"created_at,omitempty"`
}

type ClusterGroup struct {
	Id     *uint64 `json:"id,omitempty"`
	Name   *string `json:"name,omitempty"`
	WebUrl *string `json:"web_url,omitempty"`
}

type Cluster struct {
	Id                 *uint64                    `json:"id,omitempty"`
	Name               *string                    `json:"name,omitempty"`
	Domain             *string                    `json:"domain,omitempty"`
	CreatedAt          *time.Time                 `json:"created_at,omitempty"`
	ProviderType       *string                    `json:"provider_type,omitempty"`
	PlatformType       *string                    `json:"platform_type,omitempty"`
	EnvironmentScope   *string                    `json:"environment_scope,omitempty"`
	ClusterType        *string                    `json:"cluster_type,omitempty"`
	User               *ClusterUser               `json:"user,omitempty"`
	PlatformKubernetes *ClusterPlatformKubernetes `json:"platform_kubernetes,omitempty"`
	ManagementProject  *ClusterManagementProject  `json:"management_project,omitempty"`
	Project            *Project                   `json:"project,omitempty"`
	Group              *ClusterGroup              `json:"group,omitempty"`
}

type ClusterPlatformKubernetesAttributes struct {
	ApiUrl            *string `json:"api_url,omitempty"`
	Token             *string `json:"token,omitempty"`
	CaCert            *string `json:"ca_cert,omitempty"`
	Namespace         *string `json:"namespace,omitempty"`
	AuthorizationType *string `json:"authorization_type,omitempty"`
}

type ClusterData struct {
	Name                         *string                              `json:"name,omitempty"`
	Domain                       *string                              `json:"domain,omitempty"`
	Enabled                      *bool                                `json:"enabled,omitempty"`
	Managed                      *bool                                `json:"managed,omitempty"`
	PlatformKubernetesAttributes *ClusterPlatformKubernetesAttributes `json:"platform_kubernetes_attributes,omitempty"`
	EnvironmentScope             *bool                                `json:"environment_scope,omitempty"`
}

// List project clusters
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_clusters.html#list-project-clusters
func (s *ClustersService) ProjectList(ctx context.Context, id string, opts *PaginationOptions) ([]*Cluster, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/clusters", id), opts)
}

// Get a single project cluster
//
// id: project ID or path
// cluster_id: Cluster ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_clusters.html#get-a-single-project-cluster
func (s *ClustersService) ProjectSingle(ctx context.Context, id string, cluster_id string) (*Cluster, *Response, error) {
	return s.single(ctx, fmt.Sprintf("projects/%v/clusters/%v", id, cluster_id))
}

// Add existing cluster to project
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_clusters.html#add-existing-cluster-to-project
func (s *ClustersService) ProjectAdd(ctx context.Context, id string, opts *ClusterData) (*Cluster, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/clusters/user", id), true, opts)
}

// Edit project cluster
//
// id: project ID or path
// cluster_id: Cluster ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_clusters.html#edit-project-cluster
func (s *ClustersService) ProjectEdit(ctx context.Context, id string, cluster_id string, opts *ClusterData) (*Cluster, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("projects/%v/clusters/%v", id, cluster_id), false, opts)
}

// Delete project cluster
//
// id: project ID or path
// cluster_id: Cluster ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/project_clusters.html#delete-project-cluster
func (s *ClustersService) ProjectRemove(ctx context.Context, id string, cluster_id string) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("projects/%v/clusters/%v", id, cluster_id))
}

// List group clusters
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_clusters.html#list-group-clusters
func (s *ClustersService) GroupList(ctx context.Context, id string, opts *PaginationOptions) ([]*Cluster, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/clusters", id), opts)
}

// Get a single group cluster
//
// id: group ID or path
// cluster_id: Cluster ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_clusters.html#get-a-single-group-cluster
func (s *ClustersService) GroupSingle(ctx context.Context, id string, cluster_id string) (*Cluster, *Response, error) {
	return s.single(ctx, fmt.Sprintf("groups/%v/clusters/%v", id, cluster_id))
}

// Add existing cluster to group
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_clusters.html#add-existing-cluster-to-group
func (s *ClustersService) GroupAdd(ctx context.Context, id string, opts *ClusterData) (*Cluster, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/clusters/user", id), true, opts)
}

// Edit group cluster
//
// id: group ID or path
// cluster_id: Cluster ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_clusters.html#edit-group-cluster
func (s *ClustersService) GroupEdit(ctx context.Context, id string, cluster_id string, opts *ClusterData) (*Cluster, *Response, error) {
	return s.addOrEdit(ctx, fmt.Sprintf("groups/%v/clusters/%v", id, cluster_id), false, opts)
}

// Delete group cluster
//
// id: group ID or path
// cluster_id: Cluster ID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/group_clusters.html#delete-group-cluster
func (s *ClustersService) GroupRemove(ctx context.Context, id string, cluster_id string) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("groups/%v/clusters/%v", id, cluster_id))
}

func (s *ClustersService) list(ctx context.Context, url string, opts *PaginationOptions) ([]*Cluster, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var clusters []*Cluster
	resp, err := s.client.Request(ctx, req, &clusters)
	if err != nil {
		return nil, resp, err
	}

	return clusters, resp, nil
}

func (s *ClustersService) single(ctx context.Context, url string) (*Cluster, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	cluster := new(Cluster)
	resp, err := s.client.Request(ctx, req, &cluster)
	if err != nil {
		return nil, resp, err
	}

	return cluster, resp, nil
}

func (s *ClustersService) addOrEdit(ctx context.Context, url string, add bool, data *ClusterData) (*Cluster, *Response, error) {
	method := "PUT"
	if true == add {
		method = "POST"
	}
	req, err := s.client.NewRequest(method, url, data)
	if err != nil {
		return nil, nil, err
	}

	cluster := new(Cluster)
	resp, err := s.client.Request(ctx, req, &cluster)
	if err != nil {
		return nil, resp, err
	}

	return cluster, resp, nil
}

func (s *ClustersService) remove(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
