package gitlab

import (
	"context"
	"fmt"
)

type GitlabCiService service

type GitlabCi struct {
	Name    *string `json:"name,omitempty"`
	Content *string `json:"content,omitempty"`
}

// List all available gitlab-ci.yml templates.
//
// Giltab API docs: https://docs.gitlab.com/ee/api/templates/gitlab_ci_ymls.html
func (s GitlabCiService) List(ctx context.Context) ([]string, *Response, error) {
	req, err := s.client.NewRequest("GET", "templates/gitlab_ci_ymls", nil)
	if err != nil {
		return nil, nil, err
	}

	var availableTemplates []string
	resp, err := s.client.Request(ctx, req, &availableTemplates)
	if err != nil {
		return nil, resp, err
	}

	return availableTemplates, resp, nil
}

// Get a gitlab-ci.yml by name.
//
// Giltab API docs: https://docs.gitlab.com/ee/api/templates/gitlab_ci_ymls.html#single-gitlab-ci-yml-template
func (s GitlabCiService) Get(ctx context.Context, name string) (*GitlabCi, *Response, error) {
	u := fmt.Sprintf("templates/gitignores/%v", name)
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	gitlabCiTmpl := new(GitlabCi)
	resp, err := s.client.Request(ctx, req, gitlabCiTmpl)
	if err != nil {
		return nil, resp, err
	}

	return gitlabCiTmpl, resp, nil
}
