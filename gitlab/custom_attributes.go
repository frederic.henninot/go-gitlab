package gitlab

import (
	"context"
	"fmt"
)

type CustomAttributesService service

type CustomAttribute struct {
	Key   *string `json:"key,omitempty"`
	Value *string `json:"value,omitempty"`
}

// List custom attributes
//
// target: must be users, groups or projects
// id: project/group/user ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/custom_attributes.html#list-custom-attributes
func (s *CustomAttributesService) List(ctx context.Context, target string, id string, opts *PaginationOptions) ([]*CustomAttribute, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("%v/%v/custom_attributes", target, id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var customAttributes []*CustomAttribute
	resp, err := s.client.Request(ctx, req, &customAttributes)
	if err != nil {
		return nil, resp, err
	}

	return customAttributes, resp, nil
}

// Single custom attribute
//
// target: must be users, groups or projects
// id: project/group/user ID or path
// key: The key of the attribute
//
// GitLab API docs: https://docs.gitlab.com/ee/api/custom_attributes.html#single-custom-attribute
func (s *CustomAttributesService) Single(ctx context.Context, target string, id string, key string) (*CustomAttribute, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("%v/%v/custom_attributes/%v", target, id, key), nil)
	if err != nil {
		return nil, nil, err
	}

	customAttribute := new(CustomAttribute)
	resp, err := s.client.Request(ctx, req, &customAttribute)
	if err != nil {
		return nil, resp, err
	}

	return customAttribute, resp, nil
}

type CustomAttributeData struct {
	Value *string `json:"value,omitempty"`
}

// Set custom attribute
//
// target: must be users, groups or projects
// id: project/group/user ID or path
// key: The key of the attribute
//
// GitLab API docs: https://docs.gitlab.com/ee/api/custom_attributes.html#set-custom-attribute
func (s *CustomAttributesService) Set(ctx context.Context, target string, id string, key string, data *CustomAttributeData) (*CustomAttribute, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("%v/%v/custom_attributes/%v", target, id, key), data)
	if err != nil {
		return nil, nil, err
	}

	customAttribute := new(CustomAttribute)
	resp, err := s.client.Request(ctx, req, customAttribute)
	if err != nil {
		return nil, resp, err
	}

	return customAttribute, resp, nil
}

// Delete custom attribute
//
// target: must be users, groups or projects
// id: project/group/user ID or path
// key: The key of the attribute
//
// GitLab API docs: https://docs.gitlab.com/ee/api/custom_attributes.html#delete-custom-attribute
func (s *CustomAttributesService) Delete(ctx context.Context, target string, id string, key string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("%v/%v/custom_attributes/%v", target, id, key), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
