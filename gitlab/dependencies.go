package gitlab

import (
	"context"
	"fmt"
)

type DependenciesService service

type Vulnerability struct {
	Name     *string `json:"name,omitempty"`
	Severity *string `json:"severity,omitempty"`
}

type Dependency struct {
	Name               *string          `json:"name,omitempty"`
	Version            *string          `json:"version,omitempty"`
	PackageManager     *string          `json:"package_manager,omitempty"`
	DependencyFilePath *string          `json:"dependency_file_path,omitempty"`
	Vulnerabilities    *[]Vulnerability `json:"vulnerabilities,omitempty"`
}

type DependenciesOption struct {
	PackageManager *string `json:"package_manager,omitempty"`
	PaginationOptions
}

// List project dependencies
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/dependencies.html#list-project-dependencies
func (s *DependenciesService) List(ctx context.Context, id string, opts *PaginationOptions) ([]*Dependency, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/dependencies", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var dependencies []*Dependency
	resp, err := s.client.Request(ctx, req, &dependencies)
	if err != nil {
		return nil, resp, err
	}

	return dependencies, resp, nil
}
