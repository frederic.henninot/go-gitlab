package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
)

type FeaturesFlagsService service

type Feature struct {
	Name  *string          `json:"name,omitempty"`
	State *string          `json:"state,omitempty"`
	Gates *json.RawMessage `json:"gates,omitempty"`
}

type FeatureData struct {
	Value        *json.RawMessage `json:"value,omitempty"`
	FeatureGroup *string          `json:"feature_group,omitempty"`
	User         *string          `json:"user,omitempty"`
	Group        *string          `json:"group,omitempty"`
	Project      *string          `json:"project,omitempty"`
}

// List all features
//
// GitLab API docs: https://docs.gitlab.com/ee/api/features.html#list-all-features
func (s *FeaturesFlagsService) List(ctx context.Context, opts *PaginationOptions) ([]*Feature, *Response, error) {
	u, err := addUrlOptions("features", opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var features []*Feature
	resp, err := s.client.Request(ctx, req, &features)
	if err != nil {
		return nil, resp, err
	}

	return features, resp, nil
}

// Set or create a feature
//
// name: Name of the feature to create or update
//
// GitLab API docs: https://docs.gitlab.com/ee/api/features.html#set-or-create-a-feature
func (s *FeaturesFlagsService) Set(ctx context.Context, name string, data *FeatureData) (*Feature, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("features/%v", name), data)
	if err != nil {
		return nil, nil, err
	}

	feature := new(Feature)
	resp, err := s.client.Request(ctx, req, feature)
	if err != nil {
		return nil, resp, err
	}

	return feature, resp, nil
}

// Delete a feature
//
// name: Name of the feature to create or update
//
// GitLab API docs: https://docs.gitlab.com/ee/api/features.html#delete-a-feature
func (s *FeaturesFlagsService) Delete(ctx context.Context, name string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("features/%v", name), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
