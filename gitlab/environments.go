package gitlab

import (
	"context"
	"fmt"
)

type EnvironmentsService service

type Environment struct {
	Id             *uint64     `json:"id,omitempty"`
	Name           *string     `json:"name,omitempty"`
	Slug           *string     `json:"slug,omitempty"`
	ExternalUrl    *string     `json:"external_url,omitempty"`
	State          *string     `json:"state,omitempty"`
	LastDeployment *Deployment `json:"last_deployment,omitempty"`
}

type EnvironmentOptions struct {
	Name   *string `json:"name,omitempty"`
	Search *string `json:"search,omitempty"`
	PaginationOptions
}

type EnvironmentData struct {
	Name        *string `json:"name,omitempty"`
	ExternalUrl *string `json:"external_url,omitempty"`
}

// List environments
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/environments.html#list-environments
func (s *EnvironmentsService) List(ctx context.Context, id string, opts *EnvironmentOptions) ([]*Environment, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/environments", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var environments []*Environment
	resp, err := s.client.Request(ctx, req, &environments)
	if err != nil {
		return nil, resp, err
	}

	return environments, resp, nil
}

// Get a specific environment
//
// id: project ID or path
// environment_id: The ID of the environment
//
// GitLab API docs: https://docs.gitlab.com/ee/api/environments.html#get-a-specific-environment
func (s *EnvironmentsService) Single(ctx context.Context, id string, environment_id uint64) (*Environment, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/environments/%v", id, environment_id), nil)
	if err != nil {
		return nil, nil, err
	}

	environment := new(Environment)
	resp, err := s.client.Request(ctx, req, &environment)
	if err != nil {
		return nil, resp, err
	}

	return environment, resp, nil
}

// Create a new environment
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/environments.html#create-a-new-environment
func (s *EnvironmentsService) Create(ctx context.Context, id string, data *EnvironmentData) (*Environment, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/environments", id), data)
	if err != nil {
		return nil, nil, err
	}

	environment := new(Environment)
	resp, err := s.client.Request(ctx, req, &environment)
	if err != nil {
		return nil, resp, err
	}

	return environment, resp, nil
}

// Edit an existing environment
//
// id: project ID or path
// environment_id: The ID of the environment
//
// GitLab API docs: https://docs.gitlab.com/ee/api/environments.html#edit-an-existing-environment
func (s *EnvironmentsService) Update(ctx context.Context, id string, environment_id uint64, data *EnvironmentData) (*Environment, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("projects/%v/environments/%v", id, environment_id), data)
	if err != nil {
		return nil, nil, err
	}

	environment := new(Environment)
	resp, err := s.client.Request(ctx, req, &environment)
	if err != nil {
		return nil, resp, err
	}

	return environment, resp, nil
}

// Delete an environment
//
// id: project ID or path
// environment_id: The ID of the environment
//
// GitLab API docs: https://docs.gitlab.com/ee/api/environments.html#delete-an-environment
func (s *EnvironmentsService) Delete(ctx context.Context, id string, environment_id uint64) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/environments/%v", id, environment_id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// Stop an environment
//
// id: project ID or path
// environment_id: The ID of the environment
//
// GitLab API docs: https://docs.gitlab.com/ee/api/environments.html#stop-an-environment
func (s *EnvironmentsService) Stop(ctx context.Context, id string, environment_id uint64) (*Environment, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/environments/%v/stop", id, environment_id), nil)
	if err != nil {
		return nil, nil, err
	}

	environment := new(Environment)
	resp, err := s.client.Request(ctx, req, &environment)
	if err != nil {
		return nil, resp, err
	}

	return environment, resp, nil
}
