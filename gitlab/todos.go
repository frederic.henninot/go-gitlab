package gitlab

import "time"

type TodosService service

type BaseTodo struct {
	Id         *uint64    `json:"id,omitempty"`
	Author     *Author    `json:"author,omitempty"`
	ActionName *string    `json:"action_name,omitempty"`
	TargetType *string    `json:"target_type,omitempty"`
	TargetUrl  *string    `json:"target_url,omitempty"`
	Body       *string    `json:"body,omitempty"`
	State      *string    `json:"state,omitempty"`
	CreatedAt  *time.Time `json:"created_at,omitempty"`
}
