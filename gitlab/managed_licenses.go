package gitlab

import (
	"context"
	"fmt"
)

type ManagedLicensesService service

type ManagedLicenceData struct {
	Name           *string `json:"name,omitempty"`
	ApprovalStatus *string `json:"approval_status,omitempty"`
}

type ManagedLicence struct {
	Id *uint64 `json:"id,omitempty"`
	ManagedLicenceData
}

// List managed licenses
//
// id: The ID or URL-encoded path of the project
//
// GitLab API docs: https://docs.gitlab.com/ee/api/managed_licenses.html#list-managed-licenses
func (s *ManagedLicensesService) List(ctx context.Context, id string) ([]*ManagedLicence, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/managed_licenses", id), nil)
	if err != nil {
		return nil, nil, err
	}

	var managedLicences []*ManagedLicence
	resp, err := s.client.Request(ctx, req, &managedLicences)
	if err != nil {
		return nil, resp, err
	}

	return managedLicences, resp, nil
}

// Show an existing managed license
//
// id: The ID or URL-encoded path of the project
// managed_license_id: The ID or URL-encoded name of the license belonging to the project
//
// GitLab API docs: https://docs.gitlab.com/ee/api/managed_licenses.html#show-an-existing-managed-license
func (s *ManagedLicensesService) Single(ctx context.Context, id string, managed_license_id string) (*ManagedLicence, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/managed_licenses/%v", id, managed_license_id), nil)
	if err != nil {
		return nil, nil, err
	}

	managedLicence := new(ManagedLicence)
	resp, err := s.client.Request(ctx, req, &managedLicence)
	if err != nil {
		return nil, resp, err
	}

	return managedLicence, resp, nil
}

// Create a new managed license
//
// id: The ID or URL-encoded path of the project
//
// GitLab API docs: https://docs.gitlab.com/ee/api/managed_licenses.html#create-a-new-managed-license
func (s *ManagedLicensesService) Create(ctx context.Context, id string, data *ManagedLicenceData) (*ManagedLicence, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/managed_licenses", id), data)
	if err != nil {
		return nil, nil, err
	}

	managedLicence := new(ManagedLicence)
	resp, err := s.client.Request(ctx, req, &managedLicence)
	if err != nil {
		return nil, resp, err
	}

	return managedLicence, resp, nil
}

// Edit an existing managed license
//
// id: The ID or URL-encoded path of the project
// managed_license_id: The ID or URL-encoded name of the license belonging to the project
//
// GitLab API docs: https://docs.gitlab.com/ee/api/managed_licenses.html#edit-an-existing-managed-license
func (s *ManagedLicensesService) Update(ctx context.Context, id string, managed_license_id string, data *ManagedLicenceData) (*ManagedLicence, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("projects/%v/managed_licenses/%v", id, managed_license_id), data)
	if err != nil {
		return nil, nil, err
	}

	managedLicence := new(ManagedLicence)
	resp, err := s.client.Request(ctx, req, &managedLicence)
	if err != nil {
		return nil, resp, err
	}

	return managedLicence, resp, nil
}

// Delete a managed license
//
// id: The ID or URL-encoded path of the project
// managed_license_id: The ID or URL-encoded name of the license belonging to the project
//
// GitLab API docs: https://docs.gitlab.com/ee/api/managed_licenses.html#delete-a-managed-license
func (s *ManagedLicensesService) Delete(ctx context.Context, id string, managed_license_id string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/managed_licenses/%v", id, managed_license_id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
