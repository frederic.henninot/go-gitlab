package gitlab

import (
	"context"
	"fmt"
	"time"
)

type DeploymentsService service

type DeployablePipeline struct {
	Id        *uint64    `json:"id,omitempty"`
	Ref       *string    `json:"ref,omitempty"`
	Sha       *string    `json:"sha,omitempty"`
	Status    *string    `json:"status,omitempty"`
	WebUrl    *string    `json:"web_url,omitempty"`
	CreatedAt *time.Time `json:"created_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
}

type DeployableCommit struct {
	Id          *string    `json:"id,omitempty"`
	ShortId     *string    `json:"short_id,omitempty"`
	Title       *string    `json:"title,omitempty"`
	AuthorName  *string    `json:"author_name,omitempty"`
	AuthorEmail *string    `json:"author_email,omitempty"`
	CreatedAt   *time.Time `json:"created_at,omitempty"`
	Message     *string    `json:"message,omitempty"`
}

type DeployableArtifacts struct {
	FileType   *string `json:"file_type,omitempty"`
	Size       *uint32 `json:"size,omitempty"`
	Filename   *string `json:"filename,omitempty"`
	FileFormat *string `json:"file_format,omitempty"`
}

type DeploymentEnvironment struct {
	Id          *uint64 `json:"id,omitempty"`
	Name        *string `json:"name,omitempty"`
	ExternalUrl *string `json:"external_url,omitempty"`
}

type DeploymentDeployable struct {
	Id                *uint64                `json:"id,omitempty"`
	Status            *string                `json:"status,omitempty"`
	Stage             *string                `json:"stage,omitempty"`
	Name              *string                `json:"name,omitempty"`
	Ref               *string                `json:"ref,omitempty"`
	Tag               *bool                  `json:"tag,omitempty"`
	Coverage          *float32               `json:"coverage,omitempty"`
	CreatedAt         *time.Time             `json:"created_at,omitempty"`
	StartedAt         *time.Time             `json:"started_at,omitempty"`
	FinishedAt        *time.Time             `json:"finished_at,omitempty"`
	User              *User                  `json:"user,omitempty"`
	Commit            *DeployableCommit      `json:"commit,omitempty"`
	Pipeline          *DeployablePipeline    `json:"pipeline,omitempty"`
	Runner            *string                `json:"runner,omitempty"`
	WebUrl            *string                `json:"web_url,omitempty"`
	ArtifactsExpireAt *time.Time             `json:"artifacts_expire_at,omitempty"`
	Artifacts         *[]DeployableArtifacts `json:"artifacts,omitempty"`
}

type Deployment struct {
	Id          *uint64                `json:"id,omitempty"`
	IId         *uint64                `json:"iid,omitempty"`
	Ref         *string                `json:"ref,omitempty"`
	Sha         *string                `json:"sha,omitempty"`
	CreatedAt   *time.Time             `json:"created_at,omitempty"`
	UpdatedAt   *time.Time             `json:"updated_at,omitempty"`
	User        *User                  `json:"user,omitempty"`
	Environment *DeploymentEnvironment `json:"environment,omitempty"`
	Deployable  *DeploymentDeployable  `json:"deployable,omitempty"`
}

type DeploymentOptions struct {
	OrderBy       *string    `json:"order_by,omitempty"`
	Sort          *string    `json:"sort,omitempty"`
	UpdatedAfter  *time.Time `json:"updated_after,omitempty"`
	UpdatedBefore *time.Time `json:"updated_before,omitempty"`
	Environment   *string    `json:"environment,omitempty"`
	Status        *string    `json:"status,omitempty"`
	PaginationOptions
}

type DeploymentUpdateData struct {
	Status *string `json:"status,omitempty"`
}
type DeploymentData struct {
	Environment *string `json:"environment,omitempty"`
	Ref         *string `json:"ref,omitempty"`
	Sha         *string `json:"sha,omitempty"`
	Tag         *bool   `json:"tag,omitempty"`
	DeploymentUpdateData
}

// List project deployments
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deployments.html#list-project-deployments
func (s *DeploymentsService) List(ctx context.Context, id string, opts *DeploymentOptions) ([]*Deployment, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/deployments", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var deployments []*Deployment
	resp, err := s.client.Request(ctx, req, &deployments)
	if err != nil {
		return nil, resp, err
	}

	return deployments, resp, nil
}

// Get a specific deployment
//
// id: project/group/user ID or path
// deploy_id: The ID of deployment
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deployments.html#get-a-specific-deployment
func (s *DeploymentsService) Single(ctx context.Context, id string, deploy_id string) (*Deployment, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/deployments/%v", id, deploy_id), nil)
	if err != nil {
		return nil, nil, err
	}

	deployment := new(Deployment)
	resp, err := s.client.Request(ctx, req, &deployment)
	if err != nil {
		return nil, resp, err
	}

	return deployment, resp, nil
}

// Create a deployment
//
// id: project/group/user ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deployments.html#create-a-deployment
func (s *DeploymentsService) Create(ctx context.Context, id string, data *DeploymentData) (*Deployment, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/deployments", id), data)
	if err != nil {
		return nil, nil, err
	}

	deployment := new(Deployment)
	resp, err := s.client.Request(ctx, req, &deployment)
	if err != nil {
		return nil, resp, err
	}

	return deployment, resp, nil
}

// Updating a deployment
//
// id: broadcast message ID
// deploy_id: The ID of deployment
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deployments.html#updating-a-deployment
func (s *DeploymentsService) Update(ctx context.Context, id string, deploy_id string, data *DeploymentUpdateData) (*Deployment, *Response, error) {
	req, err := s.client.NewRequest("PUT", fmt.Sprintf("projects/%v/deployments/%v", id, deploy_id), data)
	if err != nil {
		return nil, nil, err
	}

	deployment := new(Deployment)
	resp, err := s.client.Request(ctx, req, &deployment)
	if err != nil {
		return nil, resp, err
	}

	return deployment, resp, nil
}

// List of merge requests associated with a deployment
//
// id: project ID or path
// deploy_id: The ID of deployment
//
// GitLab API docs: https://docs.gitlab.com/ee/api/deployments.html#list-of-merge-requests-associated-with-a-deployment
func (s *DeploymentsService) MergeRequests(ctx context.Context, id string, deploy_id string, opts *MergeRequestOptions) ([]*MergeRequest, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/deployments/%v/merge_requests", id, deploy_id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var mergeRequests []*MergeRequest
	resp, err := s.client.Request(ctx, req, &mergeRequests)
	if err != nil {
		return nil, resp, err
	}

	return mergeRequests, resp, nil
}
