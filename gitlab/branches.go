package gitlab

import (
	"context"
	"fmt"
	"time"
)

type BranchesService service

type BranchCommit struct {
	Id            *string    `json:"id,omitempty"`
	ShortId       *string    `json:"short_id,omitempty"`
	Title         *string    `json:"title,omitempty"`
	Message       *string    `json:"message,omitempty"`
	AuthorEmail   *string    `json:"author_email,omitempty"`
	AuthorName    *string    `json:"author_name,omitempty"`
	AuthoredDate  *time.Time `json:"authored_date,omitempty"`
	ComittedDate  *time.Time `json:"committed_date,omitempty"`
	ComitterEmail *string    `json:"committer_email,omitempty"`
	ComitterName  *string    `json:"committer_name,omitempty"`
	ParentIds     *[]string  `json:"parent_ids,omitempty"`
}

type Branch struct {
	Name                *string       `json:"name,omitempty"`
	Merged              *bool         `json:"merged,omitempty"`
	Protected           *bool         `json:"protected,omitempty"`
	Default             *bool         `json:"default,omitempty"`
	DeveloppersCanPush  *bool         `json:"developers_can_push,omitempty"`
	DeveloppersCanMerge *bool         `json:"developers_can_merge,omitempty"`
	CanPush             *bool         `json:"can_push,omitempty"`
	Commit              *BranchCommit `json:"commit,omitempty"`
}

type BranchData struct {
	Branch *string `json:"branch,omitempty"`
	Ref    *string `json:"ref,omitempty"`
}

type BranchOptions struct {
	Search *string `json:"search,omitempty"`
	PaginationOptions
}

// List repository branches
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/branches.html#list-repository-branches
func (s *BranchesService) List(ctx context.Context, id string, opts *BranchOptions) ([]*Branch, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/repository/branches", id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var branches []*Branch
	resp, err := s.client.Request(ctx, req, &branches)
	if err != nil {
		return nil, resp, err
	}

	return branches, resp, nil
}

// Get single repository branch
//
// id: project ID or path
// name: Branch name
//
// GitLab API docs: https://docs.gitlab.com/ee/api/branches.html#get-single-repository-branch
func (s *BranchesService) Single(ctx context.Context, id string, name string) (*Branch, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/repository/branches/%v", id, name), nil)
	if err != nil {
		return nil, nil, err
	}

	branch := new(Branch)
	resp, err := s.client.Request(ctx, req, &branch)
	if err != nil {
		return nil, resp, err
	}

	return branch, resp, nil
}

// Create repository branch
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/branches.html#create-repository-branch
func (s *BranchesService) Create(ctx context.Context, id string, data *BranchData) (*Branch, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("projects/%v/repository/branches", id), data)
	if err != nil {
		return nil, nil, err
	}

	branch := new(Branch)
	resp, err := s.client.Request(ctx, req, &branch)
	if err != nil {
		return nil, resp, err
	}

	return branch, resp, nil
}

// Delete repository branch
//
// id: project ID or path
// name: Branch name
//
// GitLab API docs: https://docs.gitlab.com/ee/api/branches.html#delete-repository-branch
func (s *BranchesService) Delete(ctx context.Context, id string, name string) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("projects/%v/repository/branches/%v", id, name))
}

// Delete merged branches
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/branches.html#delete-merged-branches
func (s *BranchesService) DeleteMerged(ctx context.Context, id string) (*Response, error) {
	return s.remove(ctx, fmt.Sprintf("projects/%v/repository/merged_branches", id))
}

func (s *BranchesService) remove(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
