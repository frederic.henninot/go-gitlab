package gitlab

import (
	"context"
	"fmt"
	"time"
)

type JobsService service

type Artifact struct {
	FileType   *string `json:"file_type,omitempty"`
	Size       *uint   `json:"size,omitempty"`
	Filename   *string `json:"filename,omitempty"`
	FileFormat *string `json:"file_format,omitempty"`
}

type Job struct {
	Id                *uint64     `json:"id,omitempty"`
	Name              *string     `json:"name,omitempty"`
	Commit            *Commit     `json:"commit,omitempty"`
	Coverage          *float32    `json:"coverage,omitempty"`
	AllowFailure      *bool       `json:"allow_failure,omitempty"`
	Duration          *uint       `json:"duration,omitempty"`
	Pipeline          *Pipeline   `json:"pipeline,omitempty"`
	Ref               *string     `json:"ref,omitempty"`
	ArtifactsFile     *Artifact   `json:"artifacts_file,omitempty"`
	Artifacts         *[]Artifact `json:"artifacts,omitempty"`
	ArtifactsExpireAt *time.Time  `json:"artifacts_expire_at,omitempty"`
	Runner            *string     `json:"runner,omitempty"`
	Stage             *string     `json:"stage,omitempty"`
	Status            *string     `json:"status,omitempty"`
	Tag               *bool       `json:"tag,omitempty"`
	DownloadUrl       *string     `json:"download_url,omitempty"`
	WebUrl            *string     `json:"web_url,omitempty"`
	User              *User       `json:"user,omitempty"`
	CreatedAt         *time.Time  `json:"created_at,omitempty"`
	StartedAt         *time.Time  `json:"started_at,omitempty"`
	FinishedAt        *time.Time  `json:"finished_at,omitempty"`
}
type JobOption struct {
	Scope *[]string `json:"scope,omitempty"`
}

// List project jobs
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#list-project-jobs
func (s *JobsService) ProjectList(ctx context.Context, id string, opts *JobOption) ([]*Job, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/jobs", id), opts)
}

// List pipeline jobs
//
// id: project ID or path
// pipeline_id: ID of pipeline
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs
func (s *JobsService) PipelineList(ctx context.Context, id string, pipeline_id uint64, opts *JobOption) ([]*Job, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/pipelines/%v/jobs", id, pipeline_id), opts)
}

// Get a single job
//
// id: project ID or path
// job_id: ID of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#get-a-single-job
func (s *JobsService) Single(ctx context.Context, id string, job_id uint64) (*Job, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/jobs/%v", id, job_id), nil)
	if err != nil {
		return nil, nil, err
	}

	job := new(Job)
	resp, err := s.client.Request(ctx, req, &job)
	if err != nil {
		return nil, resp, err
	}

	return job, resp, nil
}

// Get job artifacts
//
// id: project ID or path
// job_id: ID of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#get-job-artifacts
func (s *JobsService) Artifacts(ctx context.Context, id string, job_id uint64) (*Response, error) {
	return s.download(ctx, fmt.Sprintf("projects/%v/jobs/%v/artifacts", id, job_id))
}

type JobNameOption struct {
	Job *string `json:"job,omitempty"`
}

// Download the artifacts archive
//
// id: project ID or path
// ref: Branch or Tag name
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#download-the-artifacts-archive
func (s *JobsService) LatestArtifact(ctx context.Context, id string, ref string, opts JobNameOption) (*Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/jobs/artifacts/%v/download", id, ref), opts)
	if err != nil {
		return nil, err
	}
	return s.download(ctx, u)
}

// Download a single artifact file by job ID
//
// id: project ID or path
// jobId: Id of job
// path: Path to a file inside the artifacts archive.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#download-a-single-artifact-file-by-job-id
func (s *JobsService) SingleArtifactJobFile(ctx context.Context, id string, jobId string, path string) (*Response, error) {
	return s.download(ctx, fmt.Sprintf("projects/%v/jobs/%v/artifacts/*%v", id, jobId, path))
}

// Download a single artifact file from specific tag or branch
//
// id: project ID or path
// ref: Branch or Tag name
// path: Path to a file inside the artifacts archive.
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#download-a-single-artifact-file-from-specific-tag-or-branch
func (s *JobsService) SingleArtifactRefFile(ctx context.Context, id string, ref string, path string, opts JobNameOption) (*Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/jobs/artifacts/%v/raw/*%v", id, ref, path), opts)
	if err != nil {
		return nil, err
	}
	return s.download(ctx, u)
}

// Get a log file
//
// id: project ID or path
// jobId: Id of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#get-a-log-file
func (s *JobsService) Log(ctx context.Context, id string, jobId string) (*Response, error) {
	return s.download(ctx, fmt.Sprintf("projects/%v/jobs/%v/trace", id, jobId))
}

// Cancel a job
//
// id: project ID or path
// job_id: ID of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#cancel-a-job
func (s *JobsService) Cancel(ctx context.Context, id string, job_id uint64) (*Job, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/jobs/%v/cancel", id, job_id))
}

// Retry a job
//
// id: project ID or path
// job_id: ID of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#retry-a-job
func (s *JobsService) Retry(ctx context.Context, id string, job_id uint64) (*Job, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/jobs/%v/retry", id, job_id))
}

// Erase a job
//
// id: project ID or path
// job_id: ID of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#erase-a-job
func (s *JobsService) Erase(ctx context.Context, id string, job_id uint64) (*Job, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/jobs/%v/erase", id, job_id))
}

// Keep artifacts
//
// id: project ID or path
// job_id: ID of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#keep-artifacts
func (s *JobsService) KeepArtifacts(ctx context.Context, id string, job_id uint64) (*Job, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/jobs/%v/artifacts/keep", id, job_id))
}

// Play a job
//
// id: project ID or path
// job_id: ID of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#play-a-job
func (s *JobsService) Play(ctx context.Context, id string, job_id uint64) (*Job, *Response, error) {
	return s.post(ctx, fmt.Sprintf("projects/%v/jobs/%v/play", id, job_id))
}

// Delete artifacts
//
// id: project ID or path
// job_id: ID of job
//
// GitLab API docs: https://docs.gitlab.com/ee/api/jobs.html#delete-artifacts
func (s *JobsService) DeleteArtifacts(ctx context.Context, id string, job_id uint64) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/jobs/%v/artifacts", id, job_id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (s *JobsService) list(ctx context.Context, url string, opts *JobOption) ([]*Job, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var jobs []*Job
	resp, err := s.client.Request(ctx, req, &jobs)
	if err != nil {
		return nil, resp, err
	}

	return jobs, resp, nil
}

func (s *JobsService) download(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (s *JobsService) post(ctx context.Context, url string) (*Job, *Response, error) {
	req, err := s.client.NewRequest("POST", url, nil)
	if err != nil {
		return nil, nil, err
	}

	job := new(Job)
	resp, err := s.client.Request(ctx, req, &job)
	if err != nil {
		return nil, resp, err
	}

	return job, resp, nil
}
