package gitlab

import (
	"context"
	"fmt"
	"time"
)

type MergeRequestsService service

type References struct {
	Short    *string `json:"short,omitempty"`
	Relative *string `json:"relative,omitempty"`
	Full     *string `json:"full,omitempty"`
}

type TimeStats struct {
	TimeEstimate        *uint8 `json:"time_estimate,omitempty"`
	TotalTimeSpent      *uint8 `json:"total_time_spent,omitempty"`
	HumanTimeEstimate   *uint8 `json:"human_time_estimate,omitempty"`
	HumanTotalTimeSpent *uint8 `json:"human_total_time_spent,omitempty"`
}

type TaskCompletionStatus struct {
	Count          *uint8 `json:"count,omitempty"`
	CompletedCount *uint8 `json:"completed_count,omitempty"`
}

type MergeRequest struct {
	Id                          *uint64               `json:"id,omitempty"`
	IId                         *uint64               `json:"iid,omitempty"`
	ProjectId                   *uint64               `json:"project_id,omitempty"`
	Title                       *string               `json:"title,omitempty"`
	Description                 *string               `json:"description,omitempty"`
	State                       *string               `json:"state,omitempty"`
	MergedBy                    *Author               `json:"merged_by,omitempty"`
	MergedAt                    *time.Time            `json:"merged_at,omitempty"`
	ClosedBy                    *Author               `json:"closed_by,omitempty"`
	ClosedAt                    *time.Time            `json:"closed_at,omitempty"`
	CreatedAt                   *time.Time            `json:"created_at,omitempty"`
	UpdatedAt                   *time.Time            `json:"updated_at,omitempty"`
	TargetBranch                *string               `json:"target_branch,omitempty"`
	SourceBranch                *string               `json:"source_branch,omitempty"`
	Upvotes                     *uint8                `json:"upvotes,omitempty"`
	Downvotes                   *uint8                `json:"upvotes,omitempty"`
	Author                      *Author               `json:"author,omitempty"`
	Assignee                    *Author               `json:"assignee,omitempty"`
	Assignees                   *[]Author             `json:"assignees,omitempty"`
	SourceProjectId             *uint64               `json:"source_project_id,omitempty"`
	TargetProjectId             *uint64               `json:"target_project_id,omitempty"`
	Labels                      *[]string             `json:"labels,omitempty"`
	WorkInPRogress              *bool                 `json:"work_in_progress,omitempty"`
	Milestone                   *Milestone            `json:"milestone,omitempty"`
	MergeWhenPipelineSucceeds   *bool                 `json:"merge_when_pipeline_succeeds,omitempty"`
	ApprovalsBeforeMerge        *bool                 `json:"approvals_before_merge,omitempty"`
	MergeStatus                 *string               `json:"merge_status,omitempty"`
	Sha                         *string               `json:"sha,omitempty"`
	MergeCommitSha              *string               `json:"merge_commit_sha,omitempty"`
	SquashCommitSha             *string               `json:"squash_commit_sha,omitempty"`
	UserNoteCount               *uint8                `json:"user_notes_count,omitempty"`
	DiscussionLocked            *bool                 `json:"discussion_locked,omitempty"`
	ShouldRemoveSourceBranch    *bool                 `json:"should_remove_source_branch,omitempty"`
	ForceRemoveSourceBranch     *bool                 `json:"force_remove_source_branch,omitempty"`
	AllowCollaboration          *bool                 `json:"allow_collaboration,omitempty"`
	AllowMaintainerToPush       *bool                 `json:"allow_maintainer_to_push,omitempty"`
	References                  *References           `json:"references,omitempty"`
	TimeStats                   *TimeStats            `json:"time_stats,omitempty"`
	Squash                      *bool                 `json:"squash,omitempty"`
	TaskCompletionStatus        *TaskCompletionStatus `json:"task_completion_status,omitempty"`
	HasConflicts                *bool                 `json:"has_conflicts,omitempty"`
	BlockingDiscussionsResolved *string               `json:"blocking_discussions_resolved,omitempty"`
	Changes                     *[]MergeRequestChange `json:"changes,omitempty"`
}

type CommitRef struct {
	CommitId *string `json:"commit_id,omitempty"`
}

type MergeRequestChange struct {
	OldPath     *string `json:"old_path,omitempty"`
	NewPath     *string `json:"new_path,omitempty"`
	AMode       *string `json:"a_mode,omitempty"`
	BMode       *string `json:"b_mode,omitempty"`
	Diff        *string `json:"diff,omitempty"`
	NewFile     *bool   `json:"new_file,omitempty"`
	RenamedFile *bool   `json:"renamed_file,omitempty"`
	DeletedFile *bool   `json:"deleted_file,omitempty"`
}

type MergeRequestData struct {
	TargetBranch          *string   `json:"target_branch,omitempty"`
	SourceBranch          *string   `json:"source_branch,omitempty"`
	Title                 *string   `json:"title,omitempty"`
	AssigneeId            *uint64   `json:"assignee_id,omitempty"`
	AssigneeIds           *[]uint64 `json:"assignee_ids,omitempty"`
	Description           *string   `json:"description,omitempty"`
	TargetProjectId       *uint64   `json:"target_project_id,omitempty"`
	Labels                *string   `json:"labels,omitempty"`
	StateEvent            *string   `json:"state_event,omitempty"`
	MilestoneId           *uint64   `json:"milestone_id,omitempty"`
	DiscussionLocked      *bool     `json:"discussion_locked,omitempty"`
	RemoveSourceBranch    *bool     `json:"remove_source_branch,omitempty"`
	AllowCollaboration    *bool     `json:"allow_collaboration,omitempty"`
	AllowMaintainerToPush *bool     `json:"allow_maintainer_to_push,omitempty"`
	Squash                *bool     `json:"squash,omitempty"`
}

type MergeRequestOptions struct {
	State            *string    `json:"state,omitempty"`
	OrderBy          *string    `json:"order_by,omitempty"`
	Sort             *string    `json:"sort,omitempty"`
	Milestone        *string    `json:"milestone,omitempty"`
	View             *string    `json:"view,omitempty"`
	Labels           *string    `json:"labels,omitempty"`
	WithLabelDetails *bool      `json:"with_labels_details,omitempty"`
	CreatedAFter     *time.Time `json:"created_after,omitempty"`
	CreatedBefore    *time.Time `json:"created_before,omitempty"`
	UpdatedAfter     *time.Time `json:"updated_after,omitempty"`
	UpdatedBefore    *time.Time `json:"updated_before,omitempty"`
	Scope            *string    `json:"scope,omitempty"`
	AuthorId         *uint64    `json:"author_id,omitempty"`
	AuthorUsername   *string    `json:"author_username,omitempty"`
	AssigneeId       *uint64    `json:"assignee_id,omitempty"`
	ApproverIds      *[]uint64  `json:"approver_ids,omitempty"`
	ApproverByIds    *[]uint64  `json:"approved_by_ids,omitempty"`
	MyReactionEmoji  *string    `json:"my_reaction_emoji,omitempty"`
	SourceBranch     *string    `json:"source_branch,omitempty"`
	TargetBranch     *string    `json:"target_branch,omitempty"`
	Search           *string    `json:"search,omitempty"`
	In               *string    `json:"in,omitempty"`
	Wip              *string    `json:"wip,omitempty"`
	PaginationOptions
}

type MergeRequestSingleOptions struct {
	Labels                      *string `json:"labels,omitempty"`
	RenderHtml                  *bool   `json:"render_html,omitempty"`
	IncludeDivergedCommitsCount *bool   `json:"include_diverged_commits_count,omitempty"`
	IncludeRebaseInProgress     *bool   `json:"include_rebase_in_progress,omitempty"`
}

type MergeData struct {
	MergeCommitMessage        *string `json:"merge_commit_message,omitempty"`
	SquashCommitMessage       *string `json:"squash_commit_message,omitempty"`
	Squash                    *bool   `json:"squash,omitempty"`
	ShouldRemoveSourceBranch  *bool   `json:"should_remove_source_branch,omitempty"`
	MergeWhenPipelineSucceeds *bool   `json:"merge_when_pipeline_succeeds,omitempty"`
	Sha                       *string `json:"sha,omitempty"`
}

type RebaseOption struct {
	SkipCi *bool `json:"skip_ci,omitempty"`
}

type Rebase struct {
	RebaseInProgress *bool   `json:"rebase_in_progress,omitempty"`
	MergeError       *string `json:"merge_error,omitempty"`
}

type MergeRequestTodo struct {
	BaseTodo
	Project *Project      `json:"project,omitempty"`
	Target  *MergeRequest `json:"target,omitempty"`
}

type Diff struct {
	Id             *uint64    `json:"id,omitempty"`
	HeadCommitSha  *string    `json:"head_commit_sha,omitempty"`
	BaseCommitSha  *string    `json:"base_commit_sha,omitempty"`
	StartCommitSha *string    `json:"start_commit_sha,omitempty"`
	CreatedAt      *time.Time `json:"created_at,omitempty"`
	MergeRequestId *uint64    `json:"merge_request_id,omitempty"`
	State          *string    `json:"state,omitempty"`
	RealSize       *string    `json:"real_size,omitempty"`
}

// List merge requests
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-requests
func (s *MergeRequestsService) List(ctx context.Context, id string, opts *MergeRequestOptions) (interface{}, *Response, error) {
	var mergeRequests []*MergeRequest
	return s.list(ctx, "merge_requests", opts, mergeRequests)
}

// List project merge requests
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#list-project-merge-requests
func (s *MergeRequestsService) ProjectList(ctx context.Context, id string, opts *MergeRequestOptions) (interface{}, *Response, error) {
	var mergeRequests []*MergeRequest
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests", id), opts, mergeRequests)
}

// List group merge requests
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#list-group-merge-requests
func (s *MergeRequestsService) GroupList(ctx context.Context, id string, opts *MergeRequestOptions) (interface{}, *Response, error) {
	var mergeRequests []*MergeRequest
	return s.list(ctx, fmt.Sprintf("groups/%v/merge_requests", id), opts, mergeRequests)
}

// Get single MR
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr
func (s *MergeRequestsService) Single(ctx context.Context, id string, merge_request_iid string, opts *MergeRequestSingleOptions) (interface{}, *Response, error) {
	mr := new(MergeRequest)
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/merge_requests/%v", id, merge_request_iid), opts)
	if err != nil {
		return nil, nil, err
	}

	return s.single(ctx, u, mr)
}

// Get single MR participants
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr-participants
func (s *MergeRequestsService) Participants(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	var authors []*Author
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/participants", id, merge_request_iid), nil, authors)
}

// Get single MR commits
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr-commits
func (s *MergeRequestsService) Commits(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	var commits []*Commit
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/commits", id, merge_request_iid), nil, commits)
}

// Get single MR changes
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr-changes
func (s *MergeRequestsService) Changes(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	var mergeRequests []*MergeRequest
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/changes", id, merge_request_iid), nil, mergeRequests)
}

// List MR pipelines
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#list-mr-pipelines
func (s *MergeRequestsService) Pipelines(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	var pipelines []*Pipeline
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/pipelines", id, merge_request_iid), nil, pipelines)
}

// Create MR Pipeline
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#create-mr-pipeline
func (s *MergeRequestsService) Pipeline(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	pipeline := new(Pipeline)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/pipelines", id, merge_request_iid), nil, pipeline)
}

// Create MR
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#create-mr
func (s *MergeRequestsService) Create(ctx context.Context, id string, data MergeRequestData) (interface{}, *Response, error) {
	mergeRequest := new(MergeRequest)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests", id), data, mergeRequest)
}

// Update MR
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#update-mr
func (s *MergeRequestsService) Update(ctx context.Context, id string, merge_request_iid string, data MergeRequestData) (interface{}, *Response, error) {
	mergeRequest := new(MergeRequest)
	return s.put(ctx, fmt.Sprintf("projects/%v/merge_requests/%v", id, merge_request_iid), data, mergeRequest)
}

// Delete a merge request
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#delete-a-merge-request
func (s *MergeRequestsService) Delete(ctx context.Context, id uint, merge_request_iid string) (*Response, error) {
	return s.delete(ctx, fmt.Sprintf("projects/%v/merge_requests/%v", id, merge_request_iid))
}

// Accept MR
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#accept-mr
func (s *MergeRequestsService) Accept(ctx context.Context, id string, merge_request_iid string, data MergeData) (interface{}, *Response, error) {
	mergeRequest := new(MergeRequest)
	return s.put(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/merge", id, merge_request_iid), data, mergeRequest)
}

// Merge to default merge ref path
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#merge-to-default-merge-ref-path
func (s *MergeRequestsService) MergeRef(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	mergeRequest := new(MergeRequest)
	return s.put(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/merge_ref", id, merge_request_iid), nil, mergeRequest)
}

// Cancel Merge When Pipeline Succeeds
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#cancel-merge-when-pipeline-succeeds
func (s *MergeRequestsService) Cancel(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	mergeRequest := new(MergeRequest)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/cancel_merge_when_pipeline_succeeds", id, merge_request_iid), nil, mergeRequest)
}

// Rebase a merge request
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#rebase-a-merge-request
func (s *MergeRequestsService) Rebase(ctx context.Context, id string, merge_request_iid string, opts RebaseOption) (interface{}, *Response, error) {
	rebase := new(Rebase)
	u, err := addUrlOptions(fmt.Sprintf("projects/%v/merge_requests/%v/rebase", id, merge_request_iid), opts)
	if err != nil {
		return nil, nil, err
	}

	return s.put(ctx, u, nil, rebase)
}

// List issues that will close on merge
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#list-issues-that-will-close-on-merge
func (s *MergeRequestsService) ClosedIssues(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	var issues []*Issue
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/closes_issues", id, merge_request_iid), nil, issues)
}

// Subscribe to a merge request
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#subscribe-to-a-merge-request
func (s *MergeRequestsService) Subscribe(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	mergeRequest := new(MergeRequest)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/subscribe", id, merge_request_iid), nil, mergeRequest)
}

// Unsubscribe from a merge request
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#unsubscribe-from-a-merge-request
func (s *MergeRequestsService) Unsubscribe(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	mergeRequest := new(MergeRequest)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/unsubscribe", id, merge_request_iid), nil, mergeRequest)
}

// Create a todo
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#create-a-todo
func (s *MergeRequestsService) Todo(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	mergeRequestTodo := new(MergeRequestTodo)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/todo", id, merge_request_iid), nil, &mergeRequestTodo)
}

// Get MR diff versions
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#get-mr-diff-versions
func (s *MergeRequestsService) Versions(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	var diffs []*Diff
	return s.list(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/versions", id, merge_request_iid), nil, diffs)
}

// Get a single MR diff version
//
// id: project ID or path
// merge_request_iid: The internal ID of the merge request
// version_id: The ID of the merge request diff version
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr
func (s *MergeRequestsService) Version(ctx context.Context, id string, merge_request_iid string, version_id uint64) (interface{}, *Response, error) {
	diff := new(Diff)
	return s.single(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/versions/%v", id, merge_request_iid, version_id), diff)
}

// Set a time estimate for a merge request
//
// id: Project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#set-a-time-estimate-for-a-merge-request
func (s *MergeRequestsService) Estimate(ctx context.Context, id string, merge_request_iid string, data DurationData) (interface{}, *Response, error) {
	timeStats := new(TimeStats)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/time_estimate", id, merge_request_iid), data, &timeStats)
}

// Reset the time estimate for a merge request
//
// id: Project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#reset-the-time-estimate-for-a-merge-request
func (s *MergeRequestsService) ResetEstimate(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	timeStats := new(TimeStats)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/reset_time_estimate", id, merge_request_iid), nil, &timeStats)
}

// Add spent time for a merge request
//
// id: Project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#add-spent-time-for-a-merge-request
func (s *MergeRequestsService) Spent(ctx context.Context, id string, merge_request_iid string, data DurationData) (interface{}, *Response, error) {
	timeStats := new(TimeStats)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/add_spent_time", id, merge_request_iid), data, &timeStats)
}

// Reset spent time for a merge request
//
// id: Project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#reset-spent-time-for-a-merge-request
func (s *MergeRequestsService) ResetSpent(ctx context.Context, id string, merge_request_iid string) (interface{}, *Response, error) {
	timeStats := new(TimeStats)
	return s.post(ctx, fmt.Sprintf("projects/%v/merge_requests/%v/reset_spent_time", id, merge_request_iid), nil, &timeStats)
}

// Get time tracking stats
//
// id: Project ID or path
// merge_request_iid: The internal ID of the merge request
//
// GitLab API docs: https://docs.gitlab.com/ee/api/merge_requests.html#get-time-tracking-stats
func (s *MergeRequestsService) TimeStats(ctx context.Context, id string, merge_request_iid string) (*TimeStats, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/merge_requests/%v/time_stats", id, merge_request_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	timeStats := new(TimeStats)
	resp, err := s.client.Request(ctx, req, &timeStats)
	if err != nil {
		return nil, resp, err
	}

	return timeStats, resp, nil
}

func (s *MergeRequestsService) list(ctx context.Context, url string, opts *MergeRequestOptions, datas interface{}) (interface{}, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &datas)
	if err != nil {
		return nil, resp, err
	}

	return datas, resp, nil
}

func (s *MergeRequestsService) single(ctx context.Context, url string, data interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &data)
	if err != nil {
		return nil, resp, err
	}

	return data, resp, nil
}

func (s *MergeRequestsService) post(ctx context.Context, url string, data interface{}, result interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("POST", url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &result)
	if err != nil {
		return nil, resp, err
	}

	return result, resp, nil
}

func (s *MergeRequestsService) put(ctx context.Context, url string, data interface{}, result interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("PUT", url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &result)
	if err != nil {
		return nil, resp, err
	}

	return result, resp, nil
}

func (s *MergeRequestsService) delete(ctx context.Context, url string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
