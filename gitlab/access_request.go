package gitlab

import (
	"context"
	"fmt"
	"time"
)

type AccessRequestService service

type AccessRequest struct {
	ID          *int64     `json:"id,omitempty"`
	Username    *string    `json:"username,omitempty"`
	Name        *string    `json:"name,omitempty"`
	State       *string    `json:"state,omitempty"`
	CreatedAt   *time.Time `json:"created_at,omitempty"`
	RequestedAt *time.Time `json:"requested_at,omitempty"`
	AccessLevel *int8      `json:"access_level,omitempty"`
}

// Retreive all Access request for a group or a project
//
// target: must be groups or projects
// id: project/group ID or path
// GitLab API docs: https://docs.gitlab.com/ee/api/access_requests.html#list-access-requests-for-a-group-or-project
func (s *AccessRequestService) List(ctx context.Context, target string, id string, opts *PaginationOptions) ([]*AccessRequest, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("%v/%v/access_requests", target, id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var accessRequests []*AccessRequest
	resp, err := s.client.Request(ctx, req, &accessRequests)
	if err != nil {
		return nil, resp, err
	}

	return accessRequests, resp, nil
}

// Request access to a group or project
//
// target: must be groups or projects
// id: project/group ID or path
// GitLab API docs: https://docs.gitlab.com/ee/api/access_requests.html#request-access-to-a-group-or-project
func (s *AccessRequestService) Request(ctx context.Context, target string, id string) (*AccessRequest, *Response, error) {
	req, err := s.client.NewRequest("POST", fmt.Sprintf("%v/%v/access_requests", target, id), nil)
	if err != nil {
		return nil, nil, err
	}

	accessRequest := new(AccessRequest)
	resp, err := s.client.Request(ctx, req, accessRequest)
	if err != nil {
		return nil, resp, err
	}

	return accessRequest, resp, nil
}

type AccessLevelOption struct {
	AccessLevel *int8 `json:"access_level,omitempty"`
}

// Approve an access request to a group or project
//
// target: must be groups or projects
// id: project/group ID or path
// GitLab API docs: https://docs.gitlab.com/ee/api/access_requests.html#approve-an-access-request
func (s *AccessRequestService) Approve(ctx context.Context, target string, id string, user_id string, opts *AccessLevelOption) (*AccessRequest, *Response, error) {
	u, err := addUrlOptions(fmt.Sprintf("%v/%v/access_requests/%v/approve", target, id, user_id), opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("PUT", u, nil)
	if err != nil {
		return nil, nil, err
	}

	accessRequest := new(AccessRequest)
	resp, err := s.client.Request(ctx, req, accessRequest)
	if err != nil {
		return nil, resp, err
	}

	return accessRequest, resp, nil
}

// Deny an access request to a group or project
//
// target: must be groups or projects
// id: project/group ID or path
// GitLab API docs: https://docs.gitlab.com/ee/api/access_requests.html#deny-an-access-request
func (s *AccessRequestService) Deny(ctx context.Context, target string, id string, user_id string) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("%v/%v/access_requests/%v", target, id, user_id), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
