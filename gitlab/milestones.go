package gitlab

import "time"

type MilestonesService service

type Milestone struct {
	Id          *uint64    `json:"id,omitempty"`
	IId         *uint64    `json:"iid,omitempty"`
	ProjectId   *uint64    `json:"project_id,omitempty"`
	Title       *string    `json:"title,omitempty"`
	Description *string    `json:"description,omitempty"`
	State       *string    `json:"state,omitempty"`
	CreatedAt   *time.Time `json:"created_at,omitempty"`
	UpdatedAt   *time.Time `json:"updated_at,omitempty"`
	DueDate     *time.Time `json:"due_date,omitempty"`
	StartDate   *time.Time `json:"start_date,omitempty"`
	WebUrl      *string    `json:"web_url,omitempty"`
}
