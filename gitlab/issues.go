package gitlab

import (
	"context"
	"fmt"
	"time"
)

type IssuesService service

type Issue struct {
	Id                   *uint64               `json:"id,omitempty"`
	Iid                  *uint64               `json:"iid,omitempty"`
	Title                *string               `json:"title,omitempty"`
	Description          *string               `json:"description,omitempty"`
	State                *string               `json:"state,omitempty"`
	Author               *Author               `json:"author,omitempty"`
	Milestone            *Milestone            `json:"milestone,omitempty"`
	ProjectId            *uint64               `json:"project_id,omitempty"`
	Assignees            *[]Author             `json:"assignees,omitempty"`
	Assignee             *Author               `json:"assignee,omitempty"`
	CreatedAt            *time.Time            `json:"created_at,omitempty"`
	UpdatedAt            *time.Time            `json:"updated_at,omitempty"`
	ClosedAt             *time.Time            `json:"closed_at,omitempty"`
	ClosedBy             *Author               `json:"closed_by,omitempty"`
	MovedToId            *uint                 `json:"moved_to_id,omitempty"`
	Labels               *[]string             `json:"labels,omitempty"`
	Upvotes              *uint                 `json:"upvotes,omitempty"`
	Downvotes            *uint                 `json:"downvotes,omitempty"`
	MergeRequestsCount   *uint                 `json:"merge_requests_count,omitempty"`
	UserNotesCount       *uint                 `json:"user_notes_count,omitempty"`
	DueDate              *JSONDate             `json:"due_date,omitempty"`
	WebUrl               *string               `json:"web_url,omitempty"`
	References           *References           `json:"references,omitempty"`
	TimeStats            *TimeStats            `json:"time_stats,omitempty"`
	HasTasks             *bool                 `json:"has_tasks,omitempty"`
	TaskStatus           *string               `json:"task_status,omitempty"`
	Confidential         *bool                 `json:"confidential,omitempty"`
	DiscussionLocked     *bool                 `json:"discussion_locked,omitempty"`
	Links                *IssueLinks           `json:"_links,omitempty"`
	TaskCompletionStatus *TaskCompletionStatus `json:"task_completion_status,omitempty"`
	Weight               *uint                 `json:"weight,omitempty"`
}

type IssueData struct {
	Iid                                *uint64    `json:"iid,omitempty"`
	Title                              *string    `json:"title,omitempty"`
	Description                        *string    `json:"description,omitempty"`
	Confidential                       *bool      `json:"confidential,omitempty"`
	AssigneeIds                        *[]uint    `json:"assignees,omitempty"`
	MilestoneId                        *uint      `json:"milestone,omitempty"`
	Labels                             *string    `json:"labels,omitempty"`
	CreatedAt                          *time.Time `json:"created_at,omitempty"`
	DueDate                            *JSONDate  `json:"due_date,omitempty"`
	MergeRequestToResolveDiscussionsOf *uint      `json:"merge_request_to_resolve_discussions_of,omitempty"`
	DiscussionToResolve                *string    `json:"discussion_to_resolve,omitempty"`
	Weight                             *uint      `json:"weight,omitempty"`
	EpicId                             *uint      `json:"epic_id,omitempty"`
	EpicIid                            *uint      `json:"epic_iid,omitempty"`
}

type IssueLinks struct {
	Self       *string `json:"self,omitempty"`
	Notes      *string `json:"notes,omitempty"`
	AwardEmoji *string `json:"award_emoji,omitempty"`
	Project    *string `json:"project,omitempty"`
}

type IssueOptions struct {
	State             *string    `json:"state,omitempty"`
	Labels            *string    `json:"labels,omitempty"`
	WithLabelsDetails *bool      `json:"with_labels_details,omitempty"`
	Milestone         *string    `json:"milestone,omitempty"`
	Scope             *string    `json:"scope,omitempty"`
	AuthorId          *uint      `json:"author_id,omitempty"`
	AuthorUsername    *string    `json:"author_username,omitempty"`
	AssigneeId        *uint      `json:"assignee_id,omitempty"`
	AssigneeUsername  *[]string  `json:"assignee_username,omitempty"`
	MyReactionEmoji   *string    `json:"my_reaction_emoji,omitempty"`
	Weight            *uint      `json:"weight,omitempty"`
	Iids              *[]uint    `json:"iids,omitempty"`
	OrderBy           *string    `json:"order_by,omitempty"`
	Sort              *string    `json:"sort,omitempty"`
	Search            *string    `json:"search,omitempty"`
	In                *string    `json:"in,omitempty"`
	CreatedAfter      *time.Time `json:"created_after,omitempty"`
	CreatedBefore     *time.Time `json:"created_before,omitempty"`
	UpdatedAfter      *time.Time `json:"updated_after,omitempty"`
	UpdatedBefore     *time.Time `json:"updated_before,omitempty"`
	Confidential      *bool      `json:"confidential,omitempty"`
	Not               *[]string  `json:"not,omitempty"`
	NonArchived       *bool      `json:"non_archived,omitempty"`
	PaginationOptions
}

// List issues
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#list-issues
func (s *IssuesService) List(ctx context.Context, opts *IssueOptions) ([]*Issue, *Response, error) {
	return s.list(ctx, "issues", opts)
}

// List group issues
//
// id: group ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#list-group-issues
func (s *IssuesService) GroupList(ctx context.Context, id string, opts *IssueOptions) ([]*Issue, *Response, error) {
	return s.list(ctx, fmt.Sprintf("groups/%v/issues", id), opts)
}

// List project issues
//
// id: project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#list-project-issues
func (s *IssuesService) ProjectList(ctx context.Context, id string, opts *IssueOptions) ([]*Issue, *Response, error) {
	return s.list(ctx, fmt.Sprintf("projects/%v/issues", id), opts)
}

// Single issue
//
// id: project ID or path
// isue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#single-issue
func (s *IssuesService) Single(ctx context.Context, id string, issueid uint) (*Issue, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/issues/%v", id, issueid), nil)
	if err != nil {
		return nil, nil, err
	}

	issue := new(Issue)
	resp, err := s.client.Request(ctx, req, &issue)
	if err != nil {
		return nil, resp, err
	}

	return issue, resp, nil
}

// New issue
//
// id: Project ID or path
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#new-issue
func (s *IssuesService) Create(ctx context.Context, id string, data IssueData) (interface{}, *Response, error) {
	issue := new(Issue)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues", id), &data, &issue)
}

// Edit issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/projects.html#edit-project-hook
func (s *IssuesService) Edit(ctx context.Context, id uint, issue_iid uint, data IssueData) (interface{}, *Response, error) {
	issue := new(Issue)
	return s.put(ctx, fmt.Sprintf("projects/%v/issues/%v", id, issue_iid), &data, &issue)
}

// Delete an issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#delete-an-issue
func (s *IssuesService) Delete(ctx context.Context, id uint, issue_iid uint) (*Response, error) {
	req, err := s.client.NewRequest("DELETE", fmt.Sprintf("projects/%v/issues/%v", id, issue_iid), nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Request(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

type MoveIssueData struct {
	ToProjectId *uint `json:"to_project_id,omitempty"`
}

// Move an issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#move-an-issue
func (s *IssuesService) Move(ctx context.Context, id string, issue_iid uint, data MoveIssueData) (interface{}, *Response, error) {
	issue := new(Issue)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues/%v/move", id, issue_iid), &data, &issue)
}

// Subscribe to an issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#subscribe-to-an-issue
func (s *IssuesService) Subscribe(ctx context.Context, id string, issue_iid uint) (interface{}, *Response, error) {
	issue := new(Issue)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues/%v/subscribe", id, issue_iid), nil, &issue)
}

// Unsubscribe from an issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#unsubscribe-from-an-issue
func (s *IssuesService) Unsubscribe(ctx context.Context, id string, issue_iid uint) (interface{}, *Response, error) {
	issue := new(Issue)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues/%v/unsubscribe", id, issue_iid), nil, &issue)
}

type IssueTodo struct {
	BaseTodo
	Project *Project `json:"project,omitempty"`
	Target  *Issue   `json:"target,omitempty"`
}

// Create a todo
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#create-a-todo
func (s *IssuesService) Todo(ctx context.Context, id string, issue_iid uint) (interface{}, *Response, error) {
	issueTodo := new(IssueTodo)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues/%v/todo", id, issue_iid), nil, &issueTodo)
}

type DurationData struct {
	Duration *string `json:"duration,omitempty"`
}

// Set a time estimate for an issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#set-a-time-estimate-for-an-issue
func (s *IssuesService) Estimate(ctx context.Context, id string, issue_iid uint, data DurationData) (interface{}, *Response, error) {
	timeStats := new(TimeStats)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues/%v/time_estimate", id, issue_iid), data, &timeStats)
}

// Reset the time estimate for an issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#reset-the-time-estimate-for-an-issue
func (s *IssuesService) ResetEstimate(ctx context.Context, id string, issue_iid uint) (interface{}, *Response, error) {
	timeStats := new(TimeStats)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues/%v/reset_time_estimate", id, issue_iid), nil, &timeStats)
}

// Add spent time for an issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#add-spent-time-for-an-issue
func (s *IssuesService) Spent(ctx context.Context, id string, issue_iid uint, data DurationData) (interface{}, *Response, error) {
	timeStats := new(TimeStats)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues/%v/add_spent_time", id, issue_iid), data, &timeStats)
}

// Reset spent time for an issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#reset-spent-time-for-an-issue
func (s *IssuesService) ResetSpent(ctx context.Context, id string, issue_iid uint) (interface{}, *Response, error) {
	timeStats := new(TimeStats)
	return s.post(ctx, fmt.Sprintf("projects/%v/issues/%v/reset_spent_time", id, issue_iid), nil, &timeStats)
}

// Get time tracking stats
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#get-time-tracking-stats
func (s *IssuesService) TimeStats(ctx context.Context, id string, issue_iid uint) (*TimeStats, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/issues/%v/time_stats", id, issue_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	timeStats := new(TimeStats)
	resp, err := s.client.Request(ctx, req, &timeStats)
	if err != nil {
		return nil, resp, err
	}

	return timeStats, resp, nil
}

// List merge requests related to issue
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#list-merge-requests-related-to-issue
func (s *IssuesService) RelatedMergeRequests(ctx context.Context, id string, issue_iid uint) ([]*MergeRequest, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/issues/%v/related_merge_requests", id, issue_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	var mergeRequests []*MergeRequest
	resp, err := s.client.Request(ctx, req, &mergeRequests)
	if err != nil {
		return nil, resp, err
	}

	return mergeRequests, resp, nil
}

// List merge requests that will close issue on merge
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#list-merge-requests-that-will-close-issue-on-merge
func (s *IssuesService) MergeRequestsClosedBy(ctx context.Context, id string, issue_iid uint) ([]*MergeRequest, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/issues/%v/closed_by", id, issue_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	var mergeRequests []*MergeRequest
	resp, err := s.client.Request(ctx, req, &mergeRequests)
	if err != nil {
		return nil, resp, err
	}

	return mergeRequests, resp, nil
}

// Participants on issues
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#participants-on-issues
func (s *IssuesService) Participants(ctx context.Context, id string, issue_iid uint) ([]*Author, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/issues/%v/participants", id, issue_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	var authors []*Author
	resp, err := s.client.Request(ctx, req, &authors)
	if err != nil {
		return nil, resp, err
	}

	return authors, resp, nil
}

type UserAgent struct {
	UserAgent        *string `json:"user_agent,omitempty"`
	IpAddress        *string `json:"ip_address,omitempty"`
	AkismetSubmitted *bool   `json:"akismet_submitted,omitempty"`
}

// Get user agent details
//
// id: Project ID or path
// issue_id: Issue IID
//
// GitLab API docs: https://docs.gitlab.com/ee/api/issues.html#get-user-agent-details
func (s *IssuesService) UserAgentDetail(ctx context.Context, id string, issue_iid uint) (*UserAgent, *Response, error) {
	req, err := s.client.NewRequest("GET", fmt.Sprintf("projects/%v/issues/%v/user_agent_detail", id, issue_iid), nil)
	if err != nil {
		return nil, nil, err
	}

	userAgent := new(UserAgent)
	resp, err := s.client.Request(ctx, req, &userAgent)
	if err != nil {
		return nil, resp, err
	}

	return userAgent, resp, nil
}

func (s *IssuesService) list(ctx context.Context, url string, opts *IssueOptions) ([]*Issue, *Response, error) {
	u, err := addUrlOptions(url, opts)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var issues []*Issue
	resp, err := s.client.Request(ctx, req, &issues)
	if err != nil {
		return nil, resp, err
	}

	return issues, resp, nil
}

func (s *IssuesService) post(ctx context.Context, url string, data interface{}, hook interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("POST", url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &hook)
	if err != nil {
		return nil, resp, err
	}

	return hook, resp, nil
}

func (s *IssuesService) put(ctx context.Context, url string, data interface{}, hook interface{}) (interface{}, *Response, error) {
	req, err := s.client.NewRequest("PUT", url, data)
	if err != nil {
		return nil, nil, err
	}

	resp, err := s.client.Request(ctx, req, &hook)
	if err != nil {
		return nil, resp, err
	}

	return hook, resp, nil
}
