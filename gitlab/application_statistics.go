package gitlab

import "context"

type ApplicationStatisticsService service

type ApplicationStatistics struct {
	Forks         *uint16 `json:"forks,omitempty"`
	Issues        *uint16 `json:"issues,omitempty"`
	MergeRequests *uint16 `json:"merge_requests,omitempty"`
	Notes         *uint16 `json:"notes,omitempty"`
	Snippets      *uint16 `json:"snippets,omitempty"`
	SshKeys       *uint16 `json:"ssh_keys,omitempty"`
	Milestones    *uint16 `json:"milestones,omitempty"`
	Users         *uint16 `json:"users,omitempty"`
	Groups        *uint16 `json:"groups,omitempty"`
	Projects      *uint16 `json:"projects,omitempty"`
	ActiveUsers   *uint16 `json:"active_users,omitempty"`
}

// Get application statistics
//
// GitLab API docs: https://docs.gitlab.com/ee/api/statistics.html
func (s *ApplicationStatisticsService) Get(ctx context.Context) (*ApplicationStatistics, *Response, error) {
	req, err := s.client.NewRequest("GET", "application/statistics", nil)
	if err != nil {
		return nil, nil, err
	}

	applicationStatistics := new(ApplicationStatistics)
	resp, err := s.client.Request(ctx, req, &applicationStatistics)
	if err != nil {
		return nil, resp, err
	}

	return applicationStatistics, resp, nil
}
