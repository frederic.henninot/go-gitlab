package gitlab

import "time"

type UsersService service

type Identity struct {
	Provider  *string `json:"provider,omitempty"`
	ExternUid *string `json:"extern_uid,omitempty"`
}

type User struct {
	Author
	Email            *string     `json:"email,omitempty"`
	CreatedAt        *time.Time  `json:"created_at,omitempty"`
	IsAdmin          *bool       `json:"is_admin,omitempty"`
	Bio              *string     `json:"bio,omitempty"`
	Location         *string     `json:"location,omitempty"`
	Skype            *string     `json:"skype,omitempty"`
	Linkedin         *string     `json:"linkedin,omitempty"`
	Twitter          *string     `json:"twitter,omitempty"`
	WebsiteUrl       *string     `json:"website_url,omitempty"`
	Organisation     *string     `json:"organization,omitempty"`
	JobTitle         *string     `json:"job_title,omitempty"`
	LastSignInAt     *time.Time  `json:"last_sign_in_at,omitempty"`
	ConfirmedAt      *time.Time  `json:"confirmed_at,omitempty"`
	ThemeId          *uint64     `json:"theme_id,omitempty"`
	LastActivityOn   *time.Time  `json:"last_activity_on,omitempty"`
	ColorSchemeId    *uint64     `json:"color_scheme_id,omitempty"`
	ProjectsLimit    *uint8      `json:"projects_limit,omitempty"`
	CurrentSignInAt  *time.Time  `json:"current_sign_in_at,omitempty"`
	Identities       *[]Identity `json:"identities,omitempty"`
	CanCreateGroup   *bool       `json:"can_create_group,omitempty"`
	CanCreateProject *bool       `json:"can_create_project,omitempty"`
	TwoFactorEnabled *bool       `json:"two_factor_enabled,omitempty"`
	External         *bool       `json:"external,omitempty"`
	PrivateProfile   *bool       `json:"private_profile,omitempty"`
	CurrentSignInIp  *string     `json:"current_sign_in_ip,omitempty"`
	LastSignInIp     *string     `json:"last_sign_in_ip,omitempty"`
}
