package gitlab

import "context"

type ImportService service

type ImportGithub struct {
	Id       *uint64 `json:"id,omitempty"`
	Name     *uint   `json:"name,omitempty"`
	FullPath *string `json:"full_path,omitempty"`
	FullName *string `json:"full_name,omitempty"`
}

type ImportGithubData struct {
	PersonalAccessToken *string `json:"personal_access_token,omitempty"`
	RepoId              *uint   `json:"repo_id,omitempty"`
	NewName             *string `json:"new_name,omitempty"`
	TargetNamespace     *string `json:"target_namespace,omitempty"`
}

// Import repository from GitHub
//
// GitLab API docs: https://docs.gitlab.com/ee/api/import.html#import-repository-from-github
func (s *ImportService) Github(ctx context.Context, data *ImportGithubData) (*ImportGithub, *Response, error) {
	req, err := s.client.NewRequest("POST", "import/github", data)
	if err != nil {
		return nil, nil, err
	}

	importGithub := new(ImportGithub)
	resp, err := s.client.Request(ctx, req, &importGithub)
	if err != nil {
		return nil, resp, err
	}

	return importGithub, resp, nil
}
